Lapland Database
================

Lapland (Laponia) - baza danych do zarządzania fabryką zabawek.

Projekt zaliczeniowy z przedmiotu Bazy Danych.

### Wykorzystane technologie:

* Baza danych:
	* PostgreSQL.

* Aplikacje klienckie:
	* Python3 (program),
	* PyQt4 (GUI),
	* Psycopg (API).

* Pomocnicze:
	* Git (organizacja),
	* MySQL Workbench (diagram tabel).

### Autorzy projektu (alfabetycznie):

		Piotr Kruk        <ppiotr.kruk@uj.edu.pl>
		Konrad Talik      <konrad.talik@uj.edu.pl>
		Michał Wielgosz   <michal.wielgosz@uj.edu.pl>

---

1. Założenia projektu.
----------------------

### 1.1. Cel.

Baza danych ma służyć jako gotowy produkt dla dowolnej fabryki (najlepiej zabawek i tym podobnych) lub dla dowolnej innej organizacji, zajmującej się przeglądaniem odebranych zamówień, akceptacją ich, zlecaniem produkcji przedmiotów danego zamówienia oraz obsługą wysyłki.

### 1.2. Główne założenia.

Celem bazy i dołączonego do niej oprogramowania jest zaoferowanie możliwości kontroli zamówień przez wyspecjalizowanych do tego urzędników. Urzędnicy mogą akceptować lub odrzucać zamówienia z poziomu programu klienckiego (graficznego). Baza danych ma także umożliwiać kontrolę stanu zamówienia przez klienta, także za pomocą odpowiedniego programu graficznego. Dodatkowymi możliwościami bazy danych jest zarządzanie pracownikami, klientami, zamówieniami i innymi aspektami fabryki na wielu płaszczyznach opisanych poniżej (w podrozdziale "Możliwości").


### 1.3. Możliwości.

Naturalnym następstwem powyższych celów jest zarządzanie z poziomu bazy danych takimi zagadnieniami, jak:

* organizacja pracowników:
	* dodawanie i usuwanie pracowników w bazie danych,
	* zarządzanie atrybutami pracowników (pensja, kadra, stanowisko),
	* obsługa urlopów,
	* klasyfikacja przepisów BHP i integracja ich z urlopami,
	* hierarchia kierowników i podwładnych.

* zarządzanie kadrami firmy:
	* tworzenie, usuwanie i modyfikacja kadr w bazie danych,
	* możliwość ograniczania kadr do wyznaczonych zbiorów profesji (*np. stworzenie kadry urzędnika kontroli zamówień, którą można przydzielić tylko osobom o różnych profesjach z zakresu księgowości*),
	* przynależność kadr do profesji nie jest przymusem! (*np. możemy informatykowi przydzielić kadrę woźnego wbrew ustaleniom tabel*),
	* tworzenie, usuwanie i modyfikacja nowych profesji.

* kadra urzędnika kontroli, jako kluczowa kadra w bazie:
	* możliwość aktualizacji statusu (zatwierdzenia lub odrzucenia) zamówienia z poziomu klienta graficznego,
	* możliwość wyświetlania historii zamówień klienta przez urzędnika z poziomu klienta graficznego.

* zarządzanie agencjami firmy:

	*Agencje, jako zewnętrzne instytucje firmy, dostarczają firmie dodatkowych informacji o klientach (ankiety, statystyki, inne raporty lub dowolne informacje) w postaci listów.*

	* tworzenie, usuwanie i modyfikacja agencji w bazie danych,
	* przydzielanie agencji do konkretnych regionów na świecie,
	* dodawanie i usuwanie pracowników agencji - agentów,
	* przypisywanie klientom konkretnego agenta.

* zarządzanie strukturą sal i biur w fabryce:
	* przydzielanie sal produkcji, biur i magazynów do konkretnych wydziałów,
	* podział biur na gabinety i archiwa,
	* przydzielanie gabinetów konkretnym pracownikom,
	* rozróżnianie szafek w biurach i ich typów (archiwalna, osobista, kartoteka klienta),
	* przydzielanie pracowników, maszynistów i administratorów do konkretnych sal produkcji,
	* przydzielanie gabinetów (biur) urzędnikom oraz przydzielanie im szafek na osobiste dokumenty - możliwość znajdowania się szafki w innym biurze niż gabinet.

* obsługa wysyłek zamówień:
	* dodawanie, modyfikacja i usuwanie pojazdów i ich kierowników,
	* dodawanie kurierów (różnych od kierowników) i przydzielanie ich do konkretnych regionów (kontrolowanych przez agencje).

### 1.4. Ograniczenia przyjęte przy projektowaniu.

Cały projekt zrealizowany został dla fikcyjnego klienta - *Fabryki Zabawek Świętego Mikołaja*. W związku z wymaganiami narzuconymi przez docelowego klienta nazewnictwo encji i relacji uległo ograniczeniom:

***Pochyłą i pogrubioną czcionką*** oznaczono elementy bazy danych takie jak encje, konkretne tabele lub relacje.

* ***Nacje*** (gnomy, elfy itd.) pełnią rolę konkretnych profesji, których reprezentanci mogą być przydzielani na stanowiska poszczególnych ***Kadr***.

* ***Dzieci*** pełnią rolę klientów w bazie, natomiast tabele (i encje) takie jak ***Sanie***, ***Renifery*** i ***Mikolaje*** to (odpowiednio) *środki transportu*, *kierowcy* i *kurierzy*.

Techniczne szczegóły i ograniczenia bazy:

* W tabeli ***Kadry*** przetrzymujemy szczegółowe informacje o dowolnych kadrach, natomiast w tabeli ***Pracownicy*** przechowujemy dane fizycznych zatrudnionych w firmie. Ponadto w bazie wyróżniono odrębne szczegóły dla kluczowych kadr w oddzielnych tabelach: ***Agenci***, ***UrzednicyKontroli***, ***Maszynisci***, ***Tragarze***.

* W bazie możliwe jest magazynowanie informacji o listach w tabeli ***Listy***, ponadto stworzono nową tabelę ***ListyOdDzieci***, służącą do przechowywania szczegółów o listach od dzieci, które mogą być istotne dla *urzędnika kontroli*. Podczas weryfikacji listu mógłby brać je pod uwagę i wpływałyby one na końcową decyzję - zaakceptowanie lub odrzucenie "zamówienia".

* W związku z ograniczeniami logistycznymi relacja ***Mikołaje***<->***Regiony*** jest wiele-do-jednego. Logistyka klienta bazy danych (Fabryki zabawek) nakazuje, by jeden mikołaj obsługiwał jeden region. Jest to związane z prostymi przyczynami jak skomplikowanie infrastruktury miast i inne utrudnienia powodujące problemy w sytuacji, gdyby dwóch lub więcej Mikołajów miało rozdawać prezenty w jednym regionie.

2. Diagram tabel bazy danych (EER).
--------------

Diagram dołączono do dokumentacji w postaci załącznika.


3. Schemat bazy danych (diagram relacji).
-----------------------------------------

Diagram dołączono do dokumentacji w postaci załącznika.


4. Użytkownicy bazy oraz więzy integralności.
---------------------------------------------

1. W tabeli ***UrzednicyKontroli*** istnieje kolumna ***Login*** zawierająca wartość tekstową równoznaczną nazwie użytkownika w bazie danego urzędnika. Jest to wykorzystywane w procedurach składowanych jako element zabezpieczający przed wykonywaniem procedur na wierszach w tabelach, do których dany użytkownik nie powinien mieć dostępu - np. urzędnik może wykonywać procedurę aktualizującą ***ObwieszczeniaKontroli***, ale tylko na tych obwieszczeniach, do których jest przypisany. Zgodnie z definicją, są to statyczne więzy integralności.


2. W bazie utworzono ROLE i konkretnych użytkowników tych ról mogących logować się i wykonywać tylko przydzielone im procedury składowane lub inne operacje na tabelach:

	Role:
	
	* `gnom`

			CREATE ROLE gnom;
			GRANT EXECUTE ON FUNCTION Id_gnoma(TEXT) TO gnom; 
			GRANT EXECUTE ON FUNCTION Widok_gnoma(INT) TO gnom;
			GRANT SELECT ON Obwieszczenia_Kontroli TO gnom;
			GRANT EXECUTE ON FUNCTION Wystaw_opinie_dziecku(INT, TEXT, BOOLEAN) TO gnom;
			GRANT USAGE ON SCHEMA schemat TO gnom;
			GRANT SELECT ON schemat.Dzieci TO gnom;


		* Gnom to podstawowa rola w bazie danych, mająca dostęp do potrzebnych procedur (wykorzystywanych w programie klienckim).
		* Rola ta posiada konkretnych użytkowników, którzy mogą się logować i wykonywać zapytania oraz procedury.

	* `mikolaj`

			CREATE ROLE mikolaj;
			GRANT USAGE ON SCHEMA schemat TO mikolaj;
			GRANT SELECT ON schemat.Regiony TO mikolaj;
			GRANT SELECT ON schemat.Dzieci TO mikolaj;
			GRANT SELECT ON schemat.Mikolaje TO mikolaj;

		* Mikołaj jest zwykłą rolą bazy, mającą dostęp do szczegółowych informacji o regionach, w których rozdaje (lub nie rozdaje) prezenty oraz informacji o dzieciach, które tam mieszkają.
		* Rola ta posiada konkretnych użytkowników, którzy mogą się logować i wykonywać zapytania.

	* `dziecko`

			CREATE ROLE dziecko LOGIN;
			GRANT EXECUTE ON FUNCTION Widok_dziecka(TEXT, TEXT, TEXT) TO dziecko;

		* Jest to rola publiczna, nie mająca wyszczególnionych użytkowników. Roli tej przydzielono **tylko** wywoływanie procedury-widoku `Widok_dziecka(TEXT, TEXT, TEXT)`, wyświetlającej *status zamówienia* wysłanego listu. Elementem zabezpieczającym przed nieautoryzowanym wywoływaniem widoku są tutaj wartości argumentów, które klient musi znać, by wyświetlić status zamówienia - imię, nazwisko i kraj pochodzenia. Trzeci argument jest powiązany z kolumną ***Panstwo*** z tabeli ***Regiony***. Aby zwiększyć poziom zabezpieczeń wystarczy zmienić powiązanie np. na kolumnę ***Id*** w tabeli ***Dzieci***, jednak wymaga to większych modyfikacji.

3. Ważnym więzem integralności jest usuwanie kaskadowe. Zastosowano je dla tabel przechowujących *szczegóły* innych tabel ogólnych (sytuacja opisana poniżej) oraz dla tabel takich jak:

	* Z tabeli ***Szafki*** usuwamy wiersz, gdy z tabeli ***Archiwa*** zniknie archiwum, w którym mieściła się szafka.

	* Z tabeli ***Dzieci*** usuwamy wiersz, gdy z tabeli ***Regiony*** zniknie region, w którym mieszkało dziecko.

	* Z tabeli ***ObwieszczeniaKontroli*** usuwamy wiersz, gdy z tabeli ***ListyOdDzieci*** zniknie list powiązany z obwieszczeniem.


5. Utworzone indeksy.
---------------------

Poniżej wypisano kolumny zawierające indeksy (które określają encje w tychże tabelach) oraz referencje do innych tabel:

* Kolumny ***NumerSaliProdukcji*** i ***NumerSaliMagazynu*** w tabeli ***MagazynSaliProdukcji***.

	* Kolumny indeksują relację *wiele do wielu*, która przyporządkowuje każdej ***SaliProdukcji*** dowolną ilość ***Magazynów***.

	* Powyższe kolumny ***NumerSaliProdukcji*** i ***NumerSaliMagazynu*** są indeksowane (odpowiednio) przez kolumny ***NumerSali*** z tabeli ***SaleProdukcji*** i ***NumerSali*** z tabeli ***Magazyny***.


* Kolumny ***NumerSzafki*** i ***NumerBiura*** w tabeli ***Szafki***.

	* Kolumny indeksują relację zawierania się szafki o danym numerze w danym biurze.

	* Powyższy ***NumerBiura*** jest indeksowany przez kolumnę 
***NumerBiura*** z tabeli ***Archiwa*** (tylko tam rozpatrujemy szafki).


* Kolumna ***NumerSali*** w tabeli ***Sale*** to numer fizycznej sali w Fabryce.
	
	* Kolumna ***NumerSali*** w tabelach *szczegółowych* ***Biura***, ***Magazyny*** i ***SaleProdukcji*** jest nią indeksowana.


* Kolumna ***Id*** w tabeli ***Pracownicy***.

	* Kolumna ***IdPracownika*** w tabelach *szczegółowych* ***Agenci***, ***PracownicySali***, ***Maszynisci***, ***UrzednicyKontroli*** i ***Tragarze*** jest nią indeksowana.


* Kolumna ***Id*** w tabeli ***Listy***.

	* Kolumna ***IdListu*** w tabelach *szczegółowych* ***RaportyObserwacji*** i ***ListyOdDzieci*** jest nią indeksowana.


* W powyższych referencjach zastosowano usuwanie kaskadowe, tam gdzie było to merytorycznie wymagane (tabele *szczegółowe*).


* *Pozostałe klucze ***Id*** w innych tabelach nie posiadają relacji związanych z indeksowaniem, oprócz prostych referencji*.



6. Widoki.
----------

1. Widok pokazujący wszystkich pracowników **bezrobotnych** (bez stanowiska).
	* Definicją pracownika bezrobotnego jest w tym przypadku brak jego numeru ***Id*** w tabeli szczegółowej kadry (***Agenci***, ***UrzednicyKontroli***, itp.).
	* Osoby będące na urlopie nie są zaliczane do bezrobotnych.

	*Przykładowe wywołanie:*

		SELECT * FROM Bezrobotni;
		 id  |    imie    |  nazwisko  | idkadry 
		-----+------------+------------+---------
		  59 | Abigail    | Wong       |       1
		  61 | Bruce      | Trinity    |       5


2. Widok pokazujący **obwieszczenia kontroli** wystawione przez ***UrzędnikówKontroli***.
	* Widok ma w sposób przejrzysty pokazywać wystawione obwieszczenia.
	* Widok zawiera dane z tabeli ***ObwieszczeniaKontroli***, wzbogacone o informacje z tabeli ***Dzieci***.
	* Widok jest ważnym elementem działania programu klienckiego - urzędnik ma dzięki niemu możliwość łatwego podglądu obecnego statusu zaakceptowania danego listu oraz wszelkich ważniejszych związanych z tym informacji.

	*Przykładowe wywołanie:*

		SELECT * FROM Obwieszczenia_Kontroli;
		 nrobwieszczenia | wystawiajacy | idlistudziecka | idraportu | ...
 		-----------------+--------------+----------------+-----------+ ...
		               4 |           83 |              4 |         4 | ...
		               5 |           84 |              5 |         5 | ...
		               9 |           88 |             10 |         9 | ...


		... |     komentarz     | zaakceptowano | datawystawienia | Imie   | Nazwisko 
		... +-------------------+---------------+-----------------+--------+--------------
		... | Sprzataj pokoj    | f             | 2012-12-20      | Abram  | Imbramowicz
		... |                   | t             | 2012-12-20      | Jan    | Nowak
		... | Nie bij brata     | f             | 2012-12-21      | Hans   | Zimmerman

3. Widok pokazujący informacje dla ***UrzędnikaKontroli***.

	* Widok ma w sposób przejrzysty pokazywać dane potrzebne urzędnikowi podczas rozpatrywania listu i wystawiania obwieszczenia.
	* Widok zawiera dane z tabeli ***ObwieszczeniaKontroli***, ***Dzieci***, ***Pracownicy***, ***ListyOdDzieci***, ***RaportyObserwacji***, ***Agenci***.
	* Widok jest ważnym elementem działania programu klienckiego - urzędnik ma dzięki niemu możliwość łatwej identyfikacji dzieci, które są mu przypisane (do weryfikacji).

		* argument: `IdGnoma INT`.

	*Przykładowe wywołanie:*

		SELECT * FROM Widok_Gnoma(89);
		 id_dziecka | imie_dziecka | nazwisko_dziecka | wiek | ...
		------------+--------------+------------------+------+ ...
		         10 | Helmut       | Sturm            |   11 | ...
		         24 | Ip           | Wan              |   12 | ...

		... | wiek | id_listu | kryptonim_agenta_wystawiajacego_raport | id_raportu 
		... +------+----------+----------------------------------------+------------
		... |   11 |       11 | a9                                     |         10
		... |   12 |       25 | b3                                     |         15


4. Widok pokazujący informacje dla ***Dziecka***.

	* Widok ma w sposób przejrzysty pokazywać dane potrzebne dziecku podczas sprawdzania statusu weryfikacji swojego listu.
	* Widok zawiera dane z tabel ***ObwieszczeniaKontroli***, ***Dzieci***, ***Pracownicy***, ***Listyoddzieci***, ***Regiony***.
	* Widok jest ważnym elementem działania programu klienckiego - dziecko ma dzięki niemu możliwość sprawdzenia, czy wysłany przez nie list został zweryfikowany. Dodatkowym atrybutem jest informacja mówiąca, czy w danym regionie dziecka prezenty zostały już rozdane.


		* argumenty: `Imie TEXT`, `Nazwisko TEXT`, `Kraj TEXT`.

	*Przykładowe wywołanie:*

		SELECT * FROM Widok_Dziecka('Siergiej','Wasilowicz','Russia');

		 imie_kontrolera | nazwisko_kontrolera | data_wystawienia_opinii | ...
		-----------------+---------------------+-------------------------+ ...
		 Ruby            | Trinity             | 2012-12-20              | ...

		... | zaakceptowano_list | komentarz | idlistu | ...
		... +--------------------+-----------+---------+ ...
		... | t                  |           |       1 | ...

		... | imie_dziecka | nazwisko_dziecka | wiek | ...
		... +--------------+------------------+------+ ...
		... | Siergiej     | Wasilowicz       |    5 | ...

		... | rozdano_prezenty | panstwo 
		... +------------------+---------
		... | f                | Russia


5. Widok pokazujący **niegrzeczne dzieci**.
	* Widok ma służyć m.in. w celu informacji o ilości dzieci, które mają otrzymać rózgę.
	* Widok zawiera dane z tabel ***ObwieszczeniaKontroli***, ***Dzieci***, ***Listy***, ***Listyoddzieci***.

	*Przykładowe wywołanie:*

		SELECT * FROM Rozga_dla_niegrzecznych;
		   imie    |  nazwisko   |     komentarz     
		-----------+-------------+-------------------
		 Abram     | Imbramowicz | Sprzataj pokoj
		 Hans      | Zimmerman   | Nie bij brata 

6. Widok pokazujący ważne **statystyki** dotyczące dzieci.

	*Przykładowe wywołanie:*

		SELECT * FROM Statystyka_dzieci;
		 ilosc_dzieci |   Srednia Wieku    | Najmlodsze Dziecko | ...
		--------------+--------------------+--------------------+ ...
		           28 | 9.2500000000000000 |                  5 | ...

		... | Najstarsze Dziecko | Ilosc Chlopcow | Ilosc Dziewczynek 
		... +--------------------+----------------+-------------------
		... |                 17 |            175 |                331


7. Widok pokazujący stan i właściwości wydziałów ***Fabryki***.

	Przykładem zastosowania może być sytuacja, w której kontroler chce wysłać zamówienia do danego działu produkcji. Pobiera on wtedy te dane i na ich podstawie wybiera do którego działu ile zamówień ma zostać wysłanych.

	*Przykładowe wywołanie:*

		SELECT * FROM Widok_fabryki;
		 ID WYDZIALU | Imie Administratora | Nazwisko Administratora | ...
		-------------+---------------------+-------------------------+ ...
		           1 | Zgredek             | Holythree               | ...
		           2 | Grzmot              | Alko                    | ...

		... |       Nazwa Wydzialu        | Ilosc zamowien 
		... +-----------------------------+----------------
		... | DZIAL PLUSZOWO GUMOWY       |              0
		... | DZIAL MECHANICZNY           |              0



8. Widok pokazujący **podsumowanie** roku.

	Zazwyczaj wywołujemy ten widok pod koniec sezonu lub po sezonie, ale możemy go także używać w trakcie sezonu w celach kontrolnych. Widok wyświetla następujące podsumowane dane dla obecnego/ukończonego sezonu:

	* ilość dzieci,
	* ilość listów od dzieci,
	* ilość grzecznych dzieci,
	* ilość niegrzecznych dzieci,
	* wyprodukowana liczba zabawek,
	* ilość zatrudnionych pracowników,
	* ilość pracowników na urlopie,
	* ilość pracowników wolnych (bez urlopu),
	* koszt wypłat dla pracowników.

	*Wywołanie:*

		SELECT * FROM Podsumowanie;

9. Widok pokazujący **10 najwyższych** pensji w firmie.

	*Przykładowe wywołanie:*

		SELECT * FROM TopZarobkowy;
		 id  | idkadry |   imie   | nazwisko | idszefa |    pensja    
		-----+---------+----------+----------+---------+--------------
		   1 |       7 | Karol    | Nowak    |       1 | 30.000,00 zł
		  88 |       3 | Bongo    | Petersen |       1 |  2.498,00 zł
		  60 |       3 | Bruce    | Ypman    |       1 |  2.495,00 zł
		 186 |       1 | Tracy    | Xen      |       1 |  2.487,00 zł
		 256 |       1 | Zmirlacz | Maerwen  |       1 |  2.479,00 zł
		 213 |       1 | Gwarek   | Bongo    |       1 |  2.475,00 zł
		 133 |       1 | Grzmot   | Winter   |       1 |  2.473,00 zł
		  83 |       3 | Legolas  | Trinity  |       1 |  2.468,00 zł
		 115 |       2 | Tatiana  | Tor      |       1 |  2.467,00 zł
		 142 |       2 | Rodwen   | Tail     |       1 |  2.466,00 zł
		(10 wierszy)

10. Widok CTE, pokazujący **hierarchię** pracowników w firmie. Wyszczególniony jest poziom określający ilość zwierzchników danego pracownika.

	*Przykładowe wywołanie:*

		SELECT * FROM Poziom_w_hierarchii;
		    imie    |  nazwisko  | id  | idszefa | poziom 
		------------+------------+-----+---------+--------
		 Karol      | Nowak      |   1 |       1 |      0
		 Gwarek     | Wee        |   2 |       1 |      1
		 Tracy      | Winter     |   3 |       1 |      1
		 Zgredek    | Holythree  |   4 |       1 |      2

11. Oprócz tego baza oferuje inne dodatkowe, mniej skomplikowane widoki tj.:

	* widok pokazujący *szeregowych* - pracowników nie mających podwładnych, korzystający z widoku CTE,
	* widok-odnośnik, pokazujący dane **dyrektora** w hierarchii pracowników,
	* widok pokazujący dane **kierowników**, czyli bezpośrednich podwładnych dyrektora,
	* widok pokazujący **liczebność** głównych działów fabryki,
	* widok pokazujący dane pracowników konkretnej kadry.



7. Procedury składowane.
----------------------

1. Procedura służąca wystawieniu opinii ***Dziecku*** przez urzędnika kontroli.

		Wystaw_opinie_dziecku

2. Procedura przyznająca **urlop** pracownikowi.

		Chorobowe

3. Procedura zwalniająca pracowników z danego działu w ***SalachProdukcji***.

		Zwolnij_dzial

4. Procedura wysyłająca zamówienie do ***Fabryki***.

		Zamowienie

5. Procedura pozwalająca na szybkie umieszczenie w bazie nowego ***ListuOdDziecka***. List jest tworzony najpierw w tabeli ***Listy***. Jednak zanim to się stanie najpierw procedura sprawdza, czy dziecko istnieje już w bazie - jeśli nie, dziecko jest dodawane do bazy.

		List_od_dziecka
	

8. Wyzwalacze.
--------------

1. Wyzwalacz aktualizujący datę wydania opinii w tabeli ***ObwieszczeniaKontroli***.

		CREATE  TRIGGER Data_obwieszczenia
		BEFORE UPDATE ON schemat.obwieszczeniakontroli
		FOR EACH ROW EXECUTE PROCEDURE data_obwieszczenia_procedura();

2. Wyzwalacz zabezpieczający zamówienia składane do ***Fabryki*** przed ujemną wartością ilości przedmiotów do produkcji.

		CREATE TRIGGER Bezpieczne_zamowienia
		BEFORE UPDATE ON schemat.Fabryka
		FOR EACH ROW EXECUTE PROCEDURE safe_transaction();

3. Wyzwalacz na tabeli ***Agencje***, aktualizujacy ilość aktywnych agentów i ilość przydzielonych agentów w razie operacji insert/delete.

		CREATE TRIGGER Aktualizuj_agentow
		BEFORE INSERT OR DELETE ON schemat.agenci
		FOR EACH ROW EXECUTE PROCEDURE agenci_procedura();

4. Wyzwalacz na tabeli ***Pracownicy*** przydzielający pracownikowi minimalną stawkę pensji w momencie dodawania pracownika do tabeli bez podania wartości płacy. Stawka przydzielana jest w zależności od przydzielonej pracownikowi kadry (informacja o stawce dla kadry jest w tabeli ***Kadry***). Wyzwalacz ten dodaje też standardowego zwierzchnika (dyrektora) i wartość `False` w kolumnie ***UrlopZdrowotny***.

		CREATE TRIGGER Inicjuj_pracownika
		AFTER INSERT ON schemat.Pracownicy
		FOR EACH ROW EXECUTE PROCEDURE inicjuj_pracownika_procedura();

5. Wyzwalacz uniemożliwiający aktualizację w tabeli ***Regiony*** kolumny ***PrezentyRozdane*** jeśli nie ma 24 lub 25 grudnia. Wyzwalacz rzuca wyjątkiem.

		CREATE TRIGGER zabezpiecz_swieta
		BEFORE UPDATE ON schemat.Regiony
		FOR EACH ROW EXECUTE PROCEDURE zabezpiecz_regiony();

6. Wyzwalacz aktualizujący kolumnę ***LiczbaDzieci*** w tabeli ***Regiony*** z każdym nowo dodanym dzieckiem do tabeli ***Dzieci***.

		CREATE TRIGGER Ilosc_dzieci
		BEFORE INSERT OR DELETE ON schemat.Dzieci
		FOR EACH ROW EXECUTE PROCEDURE licz_dzieci();

9. Skrypt tworzący bazę danych.
-------------------------------

Skrypt SQL tworzący bazę danych dostępny jest w pliku src/sql/szkielet-bazy.sql.

10. Typowe zapytania.
---------------------

### 10.1. Typowymi zapytaniami będą zazwyczaj widoki opisane w dziale *6. Widoki*. 

### 10.2. Przykłady użycia procedur składowanych.

Urzędnik wystawiający opinię dziecku:

		SELECT Wystaw_opinie_dziecku(1,'Ucz sie!',false);

Kierownik przydzielający urlop zdrowotny pracownikowi o numerze ***Id*** = 111:

		SELECT Chorobowe(111);

Administrator zwalniający wszystkich pracowników sali produkcyjnej o numerze 11:

		SELECT Zwolnij_dzial(11);

Zlecenie produkcji 10 zabawek pluszowo-gumowych:

		SELECT Zamowienie(1,10);

Błędne zlecenie ujemnej wartości zabawek - kontrolowane przez wyzwalacz:

		SELECT Zamowienie(1,-5);

### 10.3. Przebieg sezonu świątecznego.

Poniżej przedstawiamy przykładowy przebieg uruchomienia i organizacji sezonu świątecznego:

*Poniższe operacje można wykonywać z poziomu bazy lub poprzez program kliencki*

1. Rusza nowy sezon świąteczny - fabryka przyjmuje nowych pracowników.

	* Administratorzy dodają nowych pracowników:

			INSERT INTO schemat.Pracownicy VALUES
				(DEFAULT, 3, 3, 'Zgredek', 'Nowak', 'Kraków');

	* Ewentualnie tworzona jest hierarchia zwierzchnictwa, gdy jest taka potrzeba:

			UPDATE schemat.Pracownicy SET IdSzefa = 2 WHERE Id = 89;
			UPDATE schemat.Pracownicy SET IdSzega = 89 WHERE Id = 55;
			...

	* Przychodzą pierwsze listy. Listonosze wprowadzają je do bazy:

			SELECT * FROM List_od_dziecka('Poland','Krzysztof','Wielicki',14,'M','2012-11-25',44);

	* Listonosz może też bez problemu dodawać listy, dla których nie zna ustalonych danych:

			SELECT * FROM List_od_dziecka('Poland','Jacek','Kowalik');

	* Powyżej użyte trzy argumenty procedury są wymagane. Fabryka z góry określa dzieciom, by podawały wszystkie trzy informacje (plus wiek) na kopercie listu. Zwiększa to unikalność podczas wstawiania, jakie stosuje procedura. Jeśli jednak trafi się list źle podpisany - można sobie z tym prosto poradzić indeksując list na poczcie i podając indeks w bazie w ten sposób:

			SELECT * FROM List_od_dziecka('Poland','Staszek','NazwiskoNieznane055');

	* Gdy nadejdzie ustalony dla nich ostateczny termin, ***Agenci*** wysyłają ***RaportyObserwacji*** z całego świata do Fabryki. Po otrzymaniu raportów i wpisaniu ich do bazy, kierownicy przydzielają urzędnikom kontroli zlecenia na kolejne obwieszczenia:

			INSERT INTO schemat.ObwieszczeniaKontroli
			(Wystawiajacy, IdListuDziecka, IdRaportu)
			VALUES
			(DEFAULT, 80, 1, 31);

			-- lub:

			INSERT INTO schemat.ObwieszczeniaKontroli VALUES (DEFAULT, 81, 2, 32);


	* Następnym etapem jest kluczowy moment w sezonie - weryfikacja listów nadesłanych od dzieci. ***UrzędnicyKontroli***, zasiadając w swoich biurach, uruchamiają program kliencki, logują się i czytają listy (fizyczne) napisane od dzieci. Do podjęcia decyzji potrzebne jest przeczytanie ***RaportuObserwacji***, nadesłanego przez przypisanego ***Agenta*** - numer konkretnego raportu znajduje się w bazie danych i jest dostępny z poziomu klienta. Po zapoznaniu się z raportem, urzędnik może teraz podjąć decyzję, wystawiając obwieszczenie. Na tym etapie program kliencki używa trzech kluczowych operacji z bazy - dwóch widoków i procedury:

			SELECT * FROM Widok_gnoma(80);
			SELECT * FROM Widok_dziecka('Siergiej','Wasilowicz','Russia');
			SELECT * FROM Wystaw_opinie_dziecku(1, 'Popracuj nad ocenami w szkole!', FALSE);

	* Po zaakceptowaniu ***ListuOdDziecka*** trzeba jeszcze złożyć zamówienie w odpowiednim dziale fabryki:

			-- Zamawiamy jedną zabawkę:
			SELECT Zamowienie(1,1);
			-- Zamawiamy dwa produkty cukiernicze:
			SELECT Zamowienie(5,2);

	* Podczas zbliżających się Świąt, dzieci mogą na bieżąco sprawdzać status swojego *zamówienia* w programie klienckim:

			SELECT * FROM Widok_dziecka('Siergiej','Wasilowicz','Russia');

	* Fabryka produkuje zabawki. Odpowiednie wydziały odbierają zamówienia:

			-- Dział Pluszowo-Gumowy:
			-- -- Sprawdzenie zamówienia (NrWydzialu):
			SELECT Ilosc_nowych_zamowien(1);
			 ilosc_nowych_zamowien 
			-----------------------
			                     2
			(1 wiersz)

			-- -- Przyjęcie zamówień - w tej liczbie, jaką wyświetlono!
			-- -- (NrWydzialu, IloscPrzyjetych)
			SELECT Przyjeto(1,2);
			 przyjeto 
			----------
 
			(1 wiersz)

			-- Dział spożywczy:
			SELECT Ilosc_nowych_zamowien(2);
			SELECT Przyjeto(2,25);

	* Po wyprodukowaniu prezentów, uzbieraniu odpowiedniej ilości i przetransportowaniu ich do ***Sań*** przez ***Tragarzy***, ***Mikołaje*** mogą ruszać w trasę:

			SELECT * FROM schemat.Mikolaje WHERE Pseudonim = 'Santa Staszek';
			 idpracownika | idsani |   pseudonim   
			--------------+--------+---------------
			          296 |      4 | Santa Staszek

			SELECT DISTINCT Panstwo FROM schemat.Regiony WHERE IdMikolaja = 296;
			 panstwo 
			---------
			 Germany
			(1 wiersz)

Powyższy opis to ogólny szkielet przebiegu sezonu. Wszelkie losowe zdarzenia (urlop zdrowotny, zwalnianie pracowników, przemieszczanie agentów) obsługiwane są przez procedury opisane w poprzednim dziale.

Pod koniec sezonu zawsze warto jeszcze wyświetlić statystyki:

		SELECT * FROM Statystyka_dzieci;
		 ilosc_dzieci |   Srednia Wieku    | Najmlodsze Dziecko | ...
		--------------+--------------------+--------------------+ ...
		           28 | 9.2500000000000000 |                  5 | ...

		... | Najstarsze Dziecko | Ilosc Chlopcow | Ilosc Dziewczynek 
		... +--------------------+----------------+-------------------
		... |                 17 |            175 |                331



11. Instrukcje użytkowania.
---------------------------

### 11.1. Instalacja bazy.

Do poprawnego działania skryptów potrzebujemy w systemie następujących pakietów:

* psycopg,

	* Arch Linux: python-psycopg2
	* Gentoo: dev-python/psycopg

* postgresql.

### 11.2. Uruchomienie bazy danych.

Po poprawnej instalacji i skonfigurowaniu PostgreSQL należy przejść do katalogu src/py/database/ i uruchomić plik **admin.py**.

W skrypcie administratorskim należy zalogować się jako administrator (domyślne **postgres**) i uruchomić "reset bazy" poleceniem "**1**".

### 11.3. Korzystanie z programu klienckiego.

Do poprawnego działania programu klienckiego potrzebujemy pakietu pyqt (Użyto wersji 4.9.6-1). 

### 11.4. Tworzenie kopii zapasowych.

#### Kopie automatyczne.

Aby włączyć automatyczne tworzenie kopii zapasowych będziemy potrzebować pliku .pgpass w naszym katalogu domowym, w którym znajduje się hasło do bazy danych.
Zawartość pliku ma postać:

     host:port:baza:użytkownik:hasło

Przykładowy wpis:

     localhost:*:*:postgres:tajne_hasło

Plik musi mieć chmod 600 lub niższy.

Po utworzeniu takiego pliku należy uruchomić skrypt crontab.py z katalogu src/cron.

#### Kopia ręczna.

Aby ręcznie utworzyć kopię zapasową, należy uruchomić konsolę administratora **admin.py** (src/py/database), zalogować się jako administrator bazy i wybrać opcję nr 5 - "wykonaj zrzut bazy".
Drugim sposobem jest wykonanie pliku src/cron/zrzut_bazy.py z powłoki.