-- wynik z workbencha
 
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';


-- -----------------------------------------------------
-- Table `lapland`.`Listy`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Listy` (
  `IdListu` INT NOT NULL ,
  `DataOtrzymania` DATE NOT NULL ,
  `OdDziecka` TINYINT(1) NULL ,
  PRIMARY KEY (`IdListu`) )
ENGINE = InnoDB
COMMENT = 'Listy przychodzace do Fabryki moga byc:\n* RaportamiObserwacj' /* comment truncated */;


-- -----------------------------------------------------
-- Table `lapland`.`RaportyObserwacji`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`RaportyObserwacji` (
  `IdListu` INT NOT NULL ,
  `IdAgenta` INT NOT NULL ,
  `DataUkonczenia` DATE NULL ,
  PRIMARY KEY (`IdListu`) ,
  INDEX `fk_RaportyObserwacji_Listy1_idx` (`IdListu` ASC) ,
  CONSTRAINT `fk_RaportyObserwacji_Listy1`
    FOREIGN KEY (`IdListu` )
    REFERENCES `lapland`.`Listy` (`IdListu` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Raporty o Dzieciach, spisywane przez Agentow, wysylane do Fa' /* comment truncated */;


-- -----------------------------------------------------
-- Table `lapland`.`Nacje`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Nacje` (
  `NazwaNacji` VARCHAR(45) NOT NULL ,
  `SrednieIQ` INT NOT NULL ,
  `SredniaPredkoscChodu` INT NOT NULL ,
  `SredniWzrost` INT NOT NULL ,
  PRIMARY KEY (`NazwaNacji`) )
ENGINE = InnoDB
COMMENT = 'Rasy Pracownikow\n\n// SredniaPredkoscChodu - m/s\n// SredniWzr' /* comment truncated */;


-- -----------------------------------------------------
-- Table `lapland`.`Kadry`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Kadry` (
  `Id` INT NOT NULL ,
  `IdNacji` INT NOT NULL ,
  `NazwaKadry` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `fk_Kadry_Nacje_idx` (`IdNacji` ASC) ,
  CONSTRAINT `fk_Kadry_Nacje`
    FOREIGN KEY (`IdNacji` )
    REFERENCES `lapland`.`Nacje` (`NazwaNacji` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Kategorie dla Pracownikow\n(Tragarze, UrzednicyKontroli, Agen' /* comment truncated */;


-- -----------------------------------------------------
-- Table `lapland`.`Pracownicy`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Pracownicy` (
  `Id` INT NOT NULL ,
  `IdKadry` INT NOT NULL ,
  `Imie` VARCHAR(45) NOT NULL ,
  `Nazwisko` VARCHAR(45) NOT NULL ,
  `Adres` VARCHAR(45) NOT NULL ,
  `UrlopZdrowotny` TINYINT(1) NULL ,
  `KoniecUrlopu` DATE NULL ,
  `Stanowisko` INT NULL ,
  `Pensja` FLOAT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `fk_Pracownicy_Kadry1_idx` (`IdKadry` ASC) ,
  CONSTRAINT `fk_Pracownicy_Kadry1`
    FOREIGN KEY (`IdKadry` )
    REFERENCES `lapland`.`Kadry` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`Fabryka`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Fabryka` (
  `NumerWydzialu` INT NOT NULL ,
  `Administrator` INT NOT NULL ,
  `Produkcyjny` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`NumerWydzialu`) ,
  INDEX `fk_Fabryka_Pracownicy1_idx` (`Administrator` ASC) ,
  CONSTRAINT `fk_Fabryka_Pracownicy1`
    FOREIGN KEY (`Administrator` )
    REFERENCES `lapland`.`Pracownicy` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Fabryka, zawierajaca konkretne Wydzialy:\nprodukcyjne lub biu' /* comment truncated */;


-- -----------------------------------------------------
-- Table `lapland`.`Sale`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Sale` (
  `NumerSali` INT NOT NULL ,
  `NumerWydzialu` INT NOT NULL ,
  PRIMARY KEY (`NumerSali`) ,
  INDEX `fk_Sale_Fabryka1_idx` (`NumerWydzialu` ASC) ,
  CONSTRAINT `fk_Sale_Fabryka1`
    FOREIGN KEY (`NumerWydzialu` )
    REFERENCES `lapland`.`Fabryka` (`NumerWydzialu` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`Biura`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Biura` (
  `NumerSali` INT NOT NULL ,
  PRIMARY KEY (`NumerSali`) ,
  CONSTRAINT `fk_Biura_Sale1`
    FOREIGN KEY (`NumerSali` )
    REFERENCES `lapland`.`Sale` (`NumerSali` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Biura i gabinety';


-- -----------------------------------------------------
-- Table `lapland`.`Archiwa`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Archiwa` (
  `NumerBiura` INT NOT NULL ,
  `ArchiwumKartotek` TINYINT(1) NOT NULL DEFAULT false ,
  PRIMARY KEY (`NumerBiura`) ,
  CONSTRAINT `fk_Archiwa_Biura1`
    FOREIGN KEY (`NumerBiura` )
    REFERENCES `lapland`.`Biura` (`NumerSali` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Archiwa zawieraja Szafki';


-- -----------------------------------------------------
-- Table `lapland`.`Szafki`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Szafki` (
  `NumerSzafki` INT NOT NULL ,
  `NumerBiura` INT NOT NULL ,
  `RodzajSzafki` MEDIUMINT(9) NOT NULL ,
  PRIMARY KEY (`NumerSzafki`, `NumerBiura`) ,
  INDEX `fk_Szafki_Archiwa1_idx` (`NumerBiura` ASC) ,
  CONSTRAINT `fk_Szafki_Archiwa1`
    FOREIGN KEY (`NumerBiura` )
    REFERENCES `lapland`.`Archiwa` (`NumerBiura` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Szafki pelne Dokumentow/Folderow (obiektow fizycznych)\n\nRodz' /* comment truncated */;


-- -----------------------------------------------------
-- Table `lapland`.`UrzednicyKontroli`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`UrzednicyKontroli` (
  `IdPracownika` INT NOT NULL ,
  `NumerSzafki` INT NOT NULL ,
  `NumerBiuraSzafki` INT NOT NULL ,
  `NumerBiuraGabinetu` INT NOT NULL ,
  PRIMARY KEY (`IdPracownika`) ,
  INDEX `fk_UrzednicyKontroli_Szafki1_idx` (`NumerSzafki` ASC, `NumerBiuraSzafki` ASC) ,
  INDEX `fk_UrzednicyKontroli_Biura1_idx` (`NumerBiuraGabinetu` ASC) ,
  CONSTRAINT `fk_UrzednicyKontroli_Pracownicy1`
    FOREIGN KEY (`IdPracownika` )
    REFERENCES `lapland`.`Pracownicy` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UrzednicyKontroli_Szafki1`
    FOREIGN KEY (`NumerSzafki` , `NumerBiuraSzafki` )
    REFERENCES `lapland`.`Szafki` (`NumerSzafki` , `NumerBiura` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_UrzednicyKontroli_Biura1`
    FOREIGN KEY (`NumerBiuraGabinetu` )
    REFERENCES `lapland`.`Biura` (`NumerSali` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`Sanie`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Sanie` (
  `Id` INT NOT NULL ,
  `Pojemnosc` INT NULL ,
  `Rejestracja` VARCHAR(45) NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`Mikolaje`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Mikolaje` (
  `Id` INT NOT NULL ,
  `IdSani` VARCHAR(45) NOT NULL ,
  `Pseudonim` VARCHAR(45) NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `fk_Mikolaje_Sanie1_idx` (`IdSani` ASC) ,
  CONSTRAINT `fk_Mikolaje_Sanie1`
    FOREIGN KEY (`IdSani` )
    REFERENCES `lapland`.`Sanie` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`Regiony`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Regiony` (
  `NumerRegionu` INT NOT NULL ,
  `IdAgencji` INT NOT NULL ,
  `Id` INT NOT NULL ,
  `Panstwo` VARCHAR(45) NULL ,
  `PrezentyRozdane` TINYINT(1) NULL ,
  `LiczbaDzieci` INT NULL ,
  PRIMARY KEY (`NumerRegionu`) ,
  INDEX `fk_Regiony_Mikolaje1_idx` (`Id` ASC) ,
  CONSTRAINT `fk_Regiony_Mikolaje1`
    FOREIGN KEY (`Id` )
    REFERENCES `lapland`.`Mikolaje` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Regiony to konkretne strefy na swiecie - konkretne skrawki r' /* comment truncated */;


-- -----------------------------------------------------
-- Table `lapland`.`Dzieci`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Dzieci` (
  `IdDziecka` INT NOT NULL ,
  `RegionPochodzenia` INT NOT NULL ,
  `Imie` VARCHAR(45) NOT NULL ,
  `Nazwisko` VARCHAR(45) NULL ,
  `NumerKartoteki` INT NULL ,
  PRIMARY KEY (`IdDziecka`) ,
  INDEX `fk_Dzieci_Regiony1_idx` (`RegionPochodzenia` ASC) ,
  CONSTRAINT `fk_Dzieci_Regiony1`
    FOREIGN KEY (`RegionPochodzenia` )
    REFERENCES `lapland`.`Regiony` (`NumerRegionu` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Dzieci wysylaja listy z prosba o konkretne prezenty, na ktor' /* comment truncated */;


-- -----------------------------------------------------
-- Table `lapland`.`ListyOdDzieci`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`ListyOdDzieci` (
  `IdListu` INT NOT NULL ,
  `Dzieci_IdDziecka` INT NOT NULL ,
  PRIMARY KEY (`IdListu`) ,
  INDEX `fk_ListyOdDzieci_Dzieci1_idx` (`Dzieci_IdDziecka` ASC) ,
  CONSTRAINT `fk_ListyOdDzieci_Listy1`
    FOREIGN KEY (`IdListu` )
    REFERENCES `lapland`.`Listy` (`IdListu` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ListyOdDzieci_Dzieci1`
    FOREIGN KEY (`Dzieci_IdDziecka` )
    REFERENCES `lapland`.`Dzieci` (`IdDziecka` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`Renifery`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Renifery` (
  `Id` INT NOT NULL ,
  `IdSani` INT NOT NULL ,
  `Pseudonim` VARCHAR(45) NULL ,
  `Zakupiony` TINYINT(1) NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `fk_Renifery_Sanie1_idx` (`IdSani` ASC) ,
  CONSTRAINT `fk_Renifery_Sanie1`
    FOREIGN KEY (`IdSani` )
    REFERENCES `lapland`.`Sanie` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`ObwieszczenieKontroli`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`ObwieszczenieKontroli` (
  `NrObwieszczenia` INT NOT NULL ,
  `Wystawiajacy` INT NOT NULL ,
  `IdListuDziecka` INT NOT NULL ,
  `IdRaportu` INT NOT NULL ,
  `Zaakceptowano` TINYINT(1) NULL ,
  `DataWystawienia` DATE NULL ,
  PRIMARY KEY (`NrObwieszczenia`) ,
  INDEX `fk_ObwieszczenieKontroli_ListyOdDzieci1_idx` (`IdListuDziecka` ASC) ,
  INDEX `fk_ObwieszczenieKontroli_RaportyObserwacji1_idx` (`IdRaportu` ASC) ,
  INDEX `fk_ObwieszczenieKontroli_UrzednicyKontroli1_idx` (`Wystawiajacy` ASC) ,
  CONSTRAINT `fk_ObwieszczenieKontroli_ListyOdDzieci1`
    FOREIGN KEY (`IdListuDziecka` )
    REFERENCES `lapland`.`ListyOdDzieci` (`IdListu` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ObwieszczenieKontroli_RaportyObserwacji1`
    FOREIGN KEY (`IdRaportu` )
    REFERENCES `lapland`.`RaportyObserwacji` (`IdListu` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ObwieszczenieKontroli_UrzednicyKontroli1`
    FOREIGN KEY (`Wystawiajacy` )
    REFERENCES `lapland`.`UrzednicyKontroli` (`IdPracownika` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Obwieszczenie przygotowuja UrzednicyKontroli, przegladajac R' /* comment truncated */;


-- -----------------------------------------------------
-- Table `lapland`.`Tragarze`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Tragarze` (
  `IdPracownika` INT NOT NULL ,
  PRIMARY KEY (`IdPracownika`) ,
  CONSTRAINT `fk_Tragarze_Pracownicy1`
    FOREIGN KEY (`IdPracownika` )
    REFERENCES `lapland`.`Pracownicy` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Tragarze glownie nosza Zabawki w Fabryce';


-- -----------------------------------------------------
-- Table `lapland`.`SaleProdukcji`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`SaleProdukcji` (
  `NumerSali` INT NOT NULL ,
  `PrzedmiotProdukcji` VARCHAR(45) NULL ,
  `BHPId` INT NULL ,
  PRIMARY KEY (`NumerSali`) ,
  CONSTRAINT `fk_SaleProdukcyjne_Sale1`
    FOREIGN KEY (`NumerSali` )
    REFERENCES `lapland`.`Sale` (`NumerSali` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`Maszynisci`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Maszynisci` (
  `IdPracownika` INT NOT NULL ,
  `NumerSali` INT NOT NULL ,
  PRIMARY KEY (`IdPracownika`) ,
  INDEX `fk_Maszynisci_SaleProdukcyjne1_idx` (`NumerSali` ASC) ,
  CONSTRAINT `fk_Maszynisci_Pracownicy1`
    FOREIGN KEY (`IdPracownika` )
    REFERENCES `lapland`.`Pracownicy` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Maszynisci_SaleProdukcyjne1`
    FOREIGN KEY (`NumerSali` )
    REFERENCES `lapland`.`SaleProdukcji` (`NumerSali` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`Magazyny`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`Magazyny` (
  `NumerSali` INT NOT NULL ,
  `Pelny` TINYINT(1) NULL ,
  PRIMARY KEY (`NumerSali`) ,
  CONSTRAINT `fk_Magazyny_Sale1`
    FOREIGN KEY (`NumerSali` )
    REFERENCES `lapland`.`Sale` (`NumerSali` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lapland`.`MagazynSaliProdukcji`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `lapland`.`MagazynSaliProdukcji` (
  `NumerSaliProdukcji` INT NOT NULL ,
  `NumerSaliMagazynu` INT NOT NULL ,
  PRIMARY KEY (`NumerSaliProdukcji`, `NumerSaliMagazynu`) ,
  INDEX `fk_SaleProdukcyjne_has_Magazyny_Magazyny1_idx` (`NumerSaliMagazynu` ASC) ,
  INDEX `fk_SaleProdukcyjne_has_Magazyny_SaleProdukcyjne1_idx` (`NumerSaliProdukcji` ASC) ,
  CONSTRAINT `fk_SaleProdukcyjne_has_Magazyny_SaleProdukcyjne1`
    FOREIGN KEY (`NumerSaliProdukcji` )
    REFERENCES `lapland`.`SaleProdukcji` (`NumerSali` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SaleProdukcyjne_has_Magazyny_Magazyny1`
    FOREIGN KEY (`NumerSaliMagazynu` )
    REFERENCES `lapland`.`Magazyny` (`NumerSali` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
