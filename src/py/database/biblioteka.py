#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Biblioteka przydatnych funkcji nie związanych z bazą danych.
"""

import random
import ustawienia
import datetime
import baza
import time
import getpass
import signal


class CoLosowac:  # klasa służąca za enumerację
    def __init__(self):
        self.wiek = 0
        self.urlop = 1
        self.koniec_urlopu = 2
        self.stanowisko = 3
        self.pensja = 4


def losuj(co_losowac):
    """ Funkcja losująca różne rzeczy. """

    if co_losowac == 0:  # losujemy wiek dziecka
        return random.randint(5, 15)

    elif co_losowac == 1:  # losujemy czy pracownik na urlopie
        if 90 > random.randrange(100) + 1:  # prawdopodobieństwo choroby
            return False  # pracownik zdrowy
        else:
            return True  # pracownik chory

    elif co_losowac == 2:  # losujemy kiedy kończy się urlop
        data = datetime.date.today()  # dzisiejsza data
        do = data.toordinal()  # konwersja do inta
        data = ustawienia.NAJWCZESNIEJSZA_DATA
        data = datetime.datetime.strptime(data, "%Y-%m-%d")
        od = data.toordinal()

        data = random.randint(od, do)  # losujemy dzień
        data = datetime.date.fromordinal(data)  # przerabiamy na datę
        data = data.isoformat()  # prezentujemy w normalnej formie

        return data

    elif co_losowac == 3:  # losujemy stanowisko pracownika
    ## potrzeba liczbę wszystkich stanowisk w kadrze
        pass

    elif co_losowac == 4:  # losujemy pensję pracownika
    ## potrzeba minimalnych i maksymalnych pensji stanowisk/kadr
        return random.randint(1200, 2500)


def czas(start):
    """ Funkcja zwracająca czas działania programu. """

    return round(time.time() - start, 3)  # zaokrąglenie do 3 miejsc


def wyswietl_wynik(tresc):
    """ Funkcja dodająca białe znaki do wyniku. """

    print('    ' + str(tresc))


def pobierz_haslo():
    """ Funkcja pobierająca w sposób cichy hasło. """

    haslo = getpass.getpass('Podaj hasło: ')
    haslo = haslo.strip()

    return haslo


def pobierz_dane():
    """ Funkcja pobierająca dane do logowania. """

    print('Podaj login:', end=' ')
    nick = input().strip()
    haslo = pobierz_haslo()

    return (nick, haslo)


def aktualny_czas():
    """ Funkcja zwracająca aktualny czas. """

    return time.time()


def zablokuj_zabijanie():
    """ Funkcja blokująca użycie ctrl c. """

    def obsluga_sygnalu(sygnal, ramka):
        baza.koniec()

    signal.signal(signal.SIGINT, obsluga_sygnalu)


def logi(tresc):
    """ Funkcja wypisująca logi na ekran. """

    start = baza.start
    tresc = str(czas(start)) + ' ' + str(tresc)
    print(tresc)


def do_pliku(tresc, plik=None):
    """ Funkcja zrzucająca zapytania do pliku. """

    if plik is None:
        plik = 'zrzut.sql'

    plik = open(plik, 'a')  # otwarcie tylko do dopisywania
    plik.write(str(tresc) + '\n')
    plik.close()  # zapisanie pliku


def jeden_wynik(lista):
    """ Zwraca jeden wynik z wyniku zwróconego przez wykonaj_zapytanie. Przydatne do zapytań zwracających tylko jeden wiersz. """

    if len(lista) == 0:
        b.logi("Pusty wynik.")
        return ''
    else:
        return lista[0][0]
