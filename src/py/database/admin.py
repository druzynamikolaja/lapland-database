#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Biblioteka funkcji dla admina.
"""

import baza
import ustawienia
import biblioteka as b


def resetuj_baze():
    """ Funkcja resetująca bazę danych. """

    try:
        baza.nowa_baza()
    except:
        b.logi('Niepowodzenie.')


def dane_poczatkowe():
    """ Funkcja wprowadzająca dane początkowe do bazy. """

    try:
        for i in range(ustawienia.ILOSC_PRACOWNIKOW):
            pracownik = baza.tworz_pracownika()
            baza.wykonaj_zapytanie(pracownik)
            b.do_pliku(pracownik)
        b.logi('Wygenerowano pracowników.')

        for i in range(ustawienia.ILOSC_DZIECI):
            dziecko = baza.tworz_dziecko()
            b.wyswietl_wynik(dziecko)  # na razie tylko wyświetlanie
            b.do_pliku(dziecko)

        baza.tworz_maszynistow()

        b.logi('Wygenerowano dane początkowe.')
    except:
        b.logi('Niepowodzenie.')


def wlasne_zapytanie(zapytanie):
    """ Funkcja do wykonywania własnych, niestandardowych zapytań. """

    try:
        wynik = baza.wykonaj_zapytanie(zapytanie)
        print('Wykonano zapytanie.')

        for w in wynik:
            b.wyswietl_wynik(w)
    except:
        b.logi('Niepowodzenie.')


def wyswietl_wszystko():
    """ Funkcja służąca do wyświetlenia wszystkiego w bazie by sprawdzić czy wszystko jest ok. """

    try:
        # lista wszystkich tabel związanych z naszą bazą
        lista_tabel = baza.wykonaj_zapytanie('SELECT table_schema, table_name FROM information_schema.tables WHERE table_schema = \'schemat\' OR table_schema = \'przykladowe\' OR table_schema = \'organizacja\';')

        for i in range(len(lista_tabel)):
            tabela = lista_tabel[i][0] + '.' + lista_tabel[i][1]  # z krotek robimy użyteczne stringi do sql
            wiersze = baza.wykonaj_zapytanie('SELECT * FROM ' + tabela)
            for w in wiersze:
                b.wyswietl_wynik(w)
    except:
        b.logi('Niepowodzenie.')


def zrzut_bazy():
    """ Funkcja wykonująca kopię zapasową bazy danych. """

    try:
        exec(open('../../cron/zrzut_bazy.py').read())
    except:
        b.logi('Niepowodzenie.')


def menu():
    """ Funkcja wyświetlająca menu. """

    print('Podaj co zrobić:')
    print('0 - to menu')
    print('1 - reset bazy')
    print('2 - wyświetl obecną zawartość bazy')
    print('3 - generuj dane')
    print('4 - własne zapytanie')
    print('5 - wykonaj zrzut bazy')
    print('inne - wyjście')


def main():  # by nie wywoływać admina gdy załadowany jako moduł
    """ Funkcja wywołująca admina. """

    b.zablokuj_zabijanie()  # blokuje nieumyślne zabicie programu by nie uszkodzić bazy
    try:
        baza.zaloguj()
        if baza.sesja.admin is not True:  # nie zalogowano jako admin
            exit()  # nie ma sensu kontynuować
    except:
        exit()

    menu()  # wywołanie menu
    while True:
        print('>', end=' ')  # prompt
        operacja = input().strip()

        if operacja == '0':
            menu()

        elif operacja == '1':
            resetuj_baze()

        elif operacja == '2':
            wyswietl_wszystko()

        elif operacja == '3':
            dane_poczatkowe()

        elif operacja == '4':
            print('Podaj zapytanie:', end=' ')
            zapytanie = input().strip()
            wlasne_zapytanie(zapytanie)

        elif operacja == '5':
            zrzut_bazy()

        else:
            baza.koniec()
            exit()


if __name__ == "__main__":
    main()
