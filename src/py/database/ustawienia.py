#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Ustawienia bazy danych.
"""


NAZWA_BAZY = 'lapland'
UZYTKOWNIK = 'postgres'
HOST = 'localhost'
NOWA_BAZA = '../../sql/reset-bazy.sql'  # ta ścieżka musi tak pozostać gdyż odpowiada położeniu skryptu
SZKIELET_BAZY = '../../sql/szkielet-bazy.sql'
DANE_PRZYKLADOWE = '../../sql/dane-przykladowe.sql'
DANE_POCZATKOWE = '../../sql/dane-poczatkowe.sql'
UZYTKOWNICY = '../../sql/uzytkownicy.sql'
WIDOKI = '../../sql/widoki.sql'
WYZWALACZE = '../../sql/wyzwalacze.sql'
FUNKCJE = '../../sql/procedury.sql'
KOPIE_ZAPASOWE = '~'
NAJWCZESNIEJSZA_DATA = '2010-01-01'  # najwcześniejsza data jaka może wystąpić w bazie
ILOSC_PRACOWNIKOW = 300  # ilość pracowników do wygenerowania
ILOSC_DZIECI = 1000  # ilość dzieci do wygenerowania
