#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Biblioteka funkcji zaimplementowanych do obsługi bazy danych.
Do poprawnego działania potrzeba zainstalować psycopg w systemie.
"""

import psycopg2
import ustawienia
import biblioteka as b
import os


start = b.aktualny_czas()  # czas uruchomienia programu
sesja = None  # zmienna globalna będąca sesją


class Poziomy:
    """ Dozwolone poziomy autoryzacji. """

    def __init__(self, poziom=None):
        self.poziom = 0  # brak autoryzacji

        if poziom == 'zu':
            self.poziom = 1  # zwykły użytkownik
        elif poziom == 'admin':
            self.poziom = 2  # admin


class Autoryzacja:
    """ Typ będący sesją. """

    def __init__(self, poziom, osoba=None, polaczenie=None):
        self.brak = True
        self.zu = False
        self.admin = False
        self.osoba = osoba  # login osoby
        self.polaczenie = polaczenie

        if isinstance(poziom, Poziomy):  # sprawdzenie czy dozwolona wartość
            if poziom.poziom == 0:  # jeśli brak autoryzacji
                pass
            elif poziom.poziom == 1:  # jeśli zu
                self.brak = False
                self.zu = True  # daj uprawnienia zu
            elif poziom.poziom == 2:  # jeśli admin
                self.brak = False
                self.zu = True  # daj uprawnienia zu
                self.admin = True  # daj uprawnienia admina
        else:  # jeśli coś kombinowano
            raise  # rzuć wyjątek

    def kim_jestem(self):
        return self.osoba

    def rozlacz(self):
        if self.polaczenie is not None:
            self.polaczenie.close()  # poprawne zamknięcie połączenia
            b.logi('Rozłączono.')

    def zakoncz(self):
        self.rozlacz()
        self.__init__(Poziomy())
        b.logi('Zakończono sesję.')


def polacz_z_baza(uzytkownik, haslo):
    """ Funkcja służąca do połączenia się z bazą danych. """

    try:
        polaczenie = psycopg2.connect(database=ustawienia.NAZWA_BAZY, user=uzytkownik, password=haslo, host=ustawienia.HOST)
        polaczenie.autocommit = True  # automatyczne komitowanie do postgresa, domyślnie wyłączone
        b.logi('Połączono z bazą.')
        return polaczenie

    except psycopg2.OperationalError as blad:  # łapanie błędów z psycopg
        blad = str(blad)
        if 'Connection refused' in blad:
            b.logi('Aleś Ty mądry.. Najpierw odpal Postgresa.')
            exit()
        elif 'baza' in blad:
            b.logi('Baza nie istnieje.')
            exit()
        elif 'rola' in blad or 'użytkownik' in blad:
            b.logi('Błąd autoryzacji dla uzytkownika ' + uzytkownik + '!')
            return False  # zwrócenie fałszu zamiast wyjścia z programu
        else:
            raise  # jakiś inny błąd


def zaloguj(uzytkownik=None, haslo=None):
    """ Funkcja służąca do logowania się użytkowników. """

    if uzytkownik is None or haslo is None:
        dane = b.pobierz_dane()
        uzytkownik = dane[0]
        haslo = dane[1]

    elif not (isinstance(uzytkownik, str) or isinstance(haslo, str)):  # nie zgadza się przekazany typ
        raise

    polaczenie = polacz_z_baza(uzytkownik, haslo)
    if polaczenie is not False:
        if uzytkownik == 'postgres':
            poziom = Poziomy('admin')
        else:
            poziom = Poziomy('zu')

        global sesja
        sesja = Autoryzacja(poziom, uzytkownik, polaczenie)
        b.logi('Ustanowiono sesję.')
    else:
        raise  # rzucamy wyjątek by przerwać ale nie ubić

    return sesja


def nowa_baza():
    """ Funkcja resetująca bazę danych. """

    global sesja
    if sesja is None:
        b.logi('Nie dokonałeś autoryzacji.')
    elif sesja.admin is True:
        sesja.rozlacz()  # rozłączenie z bazą by poprawnie ją usunąć

        # polecenie wykonane z powłoki gdyż nie da się w psycopg stworzyć bazy
        polecenie = 'psql -U ' + sesja.kim_jestem() + ' -h ' + ustawienia.HOST + ' -f ' + ustawienia.NOWA_BAZA
        os.system(polecenie)
        b.logi("Utworzono nową bazę.")

        # polecenia wykonywane bez udziału pythona w celu szybszego działania
        # i z tego powodu doświadczamy problemu wielokrotnego podawania hasła

        # poszczególne pliki dorzucimy do strumienia
        polecenie = 'cat ' + ustawienia.SZKIELET_BAZY + ' ' + ustawienia.WIDOKI + ' ' + ustawienia.WYZWALACZE + ' ' + ustawienia.DANE_PRZYKLADOWE + ' ' + ustawienia.DANE_POCZATKOWE + ' ' + ustawienia.FUNKCJE + ' ' + ustawienia.UZYTKOWNICY + '|'
        polecenie += 'psql -U ' + sesja.kim_jestem() + ' -h ' + ustawienia.HOST + ' -d ' + ustawienia.NAZWA_BAZY + ' -f -'
        os.system(polecenie)
        b.logi('Wykonano skrypty tworzące bazę.')

		# niestety musimy 3 razy podać hasło (chyba że uzupełnimy bazę z poziomu pythona (jak było pierwotnie))
        sesja.zakoncz()  # odnowienie sesji
        zaloguj()
    else:
        b.logi('Nie masz uprawnień. Jak dostałeś się do tej metody?')


def wykonaj_zapytanie(zapytanie):
    """ Funkcja służąca do wysłania zapytania do bazy danych. """

    # obcięcie zbędnych znaków
    zapytanie = zapytanie.strip()
    if zapytanie[0] == '"':
        zapytanie = zapytanie[1:]
    if zapytanie[-1] == '"':
        zapytanie = zapytanie[:-1]

    global sesja
    kursor = sesja.polaczenie.cursor()
    #kursor.withhold =  ## nie jestem pewny czy to się przyda

    b.logi(zapytanie)
    kursor.execute(zapytanie)
    #print(kursor.query)  # przyda się do wyłapywania błędnych zapytań

    if 'select' in zapytanie.lower()[:6]:  # jeśli zapytanie jest SELECTem
        return kursor.fetchall()  # zwraca listę krotek
    elif 'insert' in zapytanie.lower()[:6] or 'update' in zapytanie.lower()[:6]:  # jeśli INSERT lub UPDATE
        return kursor.rowcount  # zwraca liczbę "tkniętych" wierszy


def ile_wierszy(tabela):
    """ Funkcja zwracająca ilość wierszy w tabeli. """

    global sesja
    if sesja is None:
        b.logi('Nie dokonałeś autoryzacji.')
    elif sesja.zu is True:
        zapytanie = 'SELECT COUNT(*) FROM ' + tabela

        wynik = wykonaj_zapytanie(zapytanie)
        b.logi('Zliczono wiersze.')
        return b.jeden_wynik(wynik)
    else:
        b.logi('Nie masz uprawnień. Jak dostałeś się do tej metody?')


def koniec():
    """ Zakończenie programu. """

    global sesja
    sesja.zakoncz()  # wyłączenie sesji

    b.logi('Wychodzę.')
#    exit()
