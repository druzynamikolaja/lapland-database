#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Funkcje generujące dane dla bazy.
"""

import biblioteka as b
import random
import baza

def tworz_pracownika(imie=None, nazwisko=None, adres=None, kadra=None, urlop=None, koniec_urlopu=None, stanowisko=None, pensja=None):
    """ Funkcja tworząca pracownika. """

    if kadra is None:
        ilosc_kadr = b.ile_wierszy('schemat.Kadry')
        kadra = random.randrange(ilosc_kadr) + 1  # losujemy kadrę dla pracownika

    ilosc_mozliwosci = b.ile_wierszy('przykladowe.Pracownicy')  # ilość dostępnych możliwości

    if imie is None:
        imie = random.randrange(ilosc_mozliwosci) + 1  # indeks imienia
        zapytanie = 'SELECT Imie FROM przykladowe.Pracownicy WHERE Id = ' + str(imie)
        imie = b.jeden_wynik(baza.wykonaj_zapytanie(zapytanie))  # imię pracownika

    if nazwisko is None:
        nazwisko = random.randrange(ilosc_mozliwosci) + 1  # analogicznie
        zapytanie = 'SELECT Nazwisko FROM przykladowe.Pracownicy WHERE Id = ' + str(nazwisko)
        nazwisko = b.jeden_wynik(baza.wykonaj_zapytanie(zapytanie))

    if adres is None:
        adres = random.randrange(ilosc_mozliwosci) + 1  # analogicznie
        zapytanie = 'SELECT Adres FROM przykladowe.Pracownicy WHERE Id = ' + str(adres)
        adres = b.jeden_wynik(baza.wykonaj_zapytanie(zapytanie))

    if urlop is None:
        urlop = b.losuj(b.CoLosowac().urlop)

    if urlop is True:
        if koniec_urlopu is None:
            koniec_urlopu = b.losuj(b.CoLosowac().koniec_urlopu)
        urlop = 'true'
    else:
        koniec_urlopu = 'NULL'
        urlop = 'false'

    if stanowisko is None:
        ## tutaj nie za bardzo wiem co z tymi stanowiskami
#        ilosc_stanowisk = ile_wierszy('')  ## do poprawy
        ilosc_stanowisk = 1
        stanowisko = random.randrange(ilosc_stanowisk) + 1

    if pensja is None:
        ## to będzie jakoś inaczej zrobione w przyszłości
        pensja = b.losuj(b.CoLosowac().pensja)

    # pierwszy argument DEFAULT gdyż baza sama dba o indeksy
    rezultat = '(DEFAULT, \'' + str(kadra) + '\', \'' + imie + '\', \'' + nazwisko + '\', \'' + adres + '\', \'' + str(urlop) + '\', '
    if koniec_urlopu == 'NULL':
        rezultat = rezultat + 'NULL, \'' + str(stanowisko) + '\', \'' + str(pensja) + '\')'
    else:
        rezultat = rezultat + '\'' + koniec_urlopu + '\', \'' + str(stanowisko) + '\', \'' + str(pensja) + '\')'
    zapytanie = 'INSERT INTO schemat.Pracownicy VALUES ' + rezultat + ';'
    b.logi('Utworzono pracownika.')
    return zapytanie


def tworz_dziecko(imie=None, nazwisko=None, plec=None, region=None, wiek=None):
    """ Funkcja tworząca dziecko. """

    if region is None:
        pass

    ilosc_mozliwosci = b.ile_wierszy('przykladowe.Dzieci')  # ilość dostępnych możliwości

    if (imie is None and plec is None):
        imie = random.randrange(ilosc_mozliwosci) + 1  # indeks imienia
        zapytanie = 'SELECT Plec FROM przykladowe.Dzieci WHERE Id = ' + str(imie)  # płeć zależna od imienia
        plec = b.jeden_wynik(baza.wykonaj_zapytanie(zapytanie))
        zapytanie = 'SELECT Imie FROM przykladowe.Dzieci WHERE Id = ' + str(imie)
        imie = b.jeden_wynik(baza.wykonaj_zapytanie(zapytanie))  # imię dziecka

    if nazwisko is None:
        nazwisko = random.randrange(ilosc_mozliwosci) + 1  # analogicznie
        zapytanie = 'SELECT Nazwisko FROM przykladowe.Dzieci WHERE Id = ' + str(nazwisko)
        nazwisko = b.jeden_wynik(baza.wykonaj_zapytanie(zapytanie))

    if wiek is None:
        wiek = b.losuj(b.CoLosowac().wiek)

    # pierwszy argument DEFAULT gdyż baza sama dba o indeksy
    rezultat = '(DEFAULT, \'' + str(region) + '\', \'' + imie + '\', \'' + nazwisko + '\', DEFAULT, \'' + str(wiek) + '\', \'' + plec + '\')'
    zapytanie = 'INSERT INTO schemat.Dzieci VALUES ' + rezultat + ';'
    b.logi('Utworzono dziecko.')
    return zapytanie


def tworz_maszynistow():
    """ Funkcja tworząca maszynistów. """

    ilosc_mozliwosci = baza.ile_wierszy('przykladowe.Maszyny')  # ilość dostępnych możliwości
    ilosc_maszynistow = baza.ile_wierszy('Maszynisci')  # ilość maszynistów

    for i in range(ilosc_maszynistow):
        maszyna = random.randrange(ilosc_mozliwosci) + 1  # indeks maszyny
        zapytanie = 'SELECT Maszyna FROM przykladowe.Maszyny WHERE Id = ' + str(maszyna)
        maszyna = b.jeden_wynik(baza.wykonaj_zapytanie(zapytanie))  # nazwa maszyny

        zapytanie = 'SELECT Id FROM Maszynisci WHERE i = ' + str(i + 1)
        pracownik = b.jeden_wynik(baza.wykonaj_zapytanie(zapytanie))

        sala = 0  # numer sali

        rezultat = '(' + str(pracownik) + ', ' + str(sala) + ', \'' + maszyna + '\')'
        zapytanie = 'INSERT INTO schemat.Maszynisci VALUES ' + rezultat + ';'
        b.wyswietl_wynik(zapytanie)
        b.logi('Utworzono maszynistę.')
#    return zapytanie
