#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Program kliencki dla zwykłego użytkownika bazy danych Lapland Database.
"""

import sys
from PyQt4 import QtGui, QtCore

sys.path.append('database')  # przejrzyste ładowanie modułów z lib
import baza


def wypelnijTabelke(qtable, lista):
    wiersze = len(lista)
    kolumny = len(lista[0])
    qtable.clear()
    qtable.setColumnCount(kolumny)
    qtable.setRowCount(wiersze)
    for wiersz in range(wiersze):
        for kolumna in range(kolumny):
            komorka = QtGui.QTableWidgetItem(str(lista[wiersz][kolumna]))
            qtable.setItem(wiersz, kolumna, komorka)


def wypelnijTabelkeOdwrocona(qtable, lista):
    wiersze = len(lista)
    kolumny = len(lista[0])
    qtable.clear()
    qtable.setRowCount(kolumny)
    qtable.setColumnCount(wiersze)
    for kolumna in range(kolumny):
        for wiersz in range(wiersze):
            komorka = QtGui.QTableWidgetItem(str(lista[wiersz][kolumna]))
            qtable.setItem(kolumny - 1 - kolumna, wiersz, komorka)


class EkranKlienta(QtGui.QWidget):
    """ Klasa będąca szablonem wszystkich ekranów w programie klienckim. """

    def __init__(self, pingwin=False, danyPoprzednik=0):
        super(EkranKlienta, self).__init__()
        self.pingwin = pingwin
        self.poprzednik = danyPoprzednik
        # Czy zamknięcie okienka powoduje wylogowanie z bazy.
        self.wylogowuje = False
        self.tworzEkranKlienta()

    def tworzEkranKlienta(self):
        """ Szkielet podstawowego okienka klienta. """
        QtGui.QToolTip.setFont(QtGui.QFont('SansSerif', 10))

        # Geometria dla każdego ekranu.
        self.resize(800, 600)
        self.centruj()
        if self.pos().y() > 64:
            self.move(self.pos().x(), 64)
        self.setWindowIcon(QtGui.QIcon('app/gui/ikona-programu.png'))

        # Tło do wyboru - z pingwinem lub bez (domyślnie).
        self.label = QtGui.QLabel(self)
        self.label.setPixmap(QtGui.QPixmap('app/gui/tlo-programu.png'))
        if self.pingwin:
            """ Taki trick. """
            self.label.setGeometry(0, 0, 800, 600)
        self.label.setScaledContents(1)

        # PushButton powrotu.
        backButton = QtGui.QPushButton("Cofnij", self)
        if self.poprzednik == 0:
            backButton.setText("Wyjście")
            backButton.clicked.connect(self.close)
        else:
            backButton.clicked.connect(self.goBack)

        if self.wylogowuje:
            backButton.setText("Wyloguj")

        backButton.resize(backButton.sizeHint())
        backButton.move(16, 560)

    def goBack(self):
        self.poprzednik.show()
        self.poprzednik.centruj()
        self.hide()
        if self.wylogowuje:
            baza.koniec()

    def closeEvent(self, event):
        """ Event wywoływany przy próbie zamknięcia okienka. """
        odp = QtGui.QMessageBox.question(self, "Wychodzę", "Czy na pewno chcesz wyjść?",
                                         QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                         QtGui.QMessageBox.No)

        if odp == QtGui.QMessageBox.Yes:
            if baza.sesja is not None:
                baza.koniec()
            event.accept()
        else:
            event.ignore()

    def centruj(self):
        """ Funkcje pomocnicze. """
        ramka = self.frameGeometry()
        foo = QtGui.QDesktopWidget().availableGeometry().center()
        ramka.moveCenter(foo)
        self.move(ramka.topLeft())


class PanelGnoma(EkranKlienta):
    """ Główne stanwisko pracy gnoma - GnomePanel. """

    def __init__(self, danyLogin, danyPoprzednik=0):
        super(PanelGnoma, self).__init__(False, danyPoprzednik)
        self.pingwin = False
        self.wylogowuje = True
        self.login = danyLogin
        self.tworzPanelGnoma()

    def tworzPanelGnoma(self):
        self.setWindowTitle("Lapland - Panel Gnoma")
        self.tytul = QtGui.QLabel(self)
        self.tytul.setText('<font color="black"><b><h1>Panel Gnoma</h1></b></font>')
        self.tytul.move(32, 32)
        self.wynik = baza.wykonaj_zapytanie("select * from widok_gnoma(id_gnoma('" + self.login + "'));")

        # Podgląd wybranego obwieszczenia kontroli.
        self.tytulPodgladu = QtGui.QLabel(self)
        self.tytulPodgladu.setText('<font color="black"><b><h2>Status dziecka</h2></b></font>')
        self.tytulPodgladu.move(500, 32)

        # Tabelka obwieszczenia.
        self.tabelkaObwieszczenia = QtGui.QTableWidget(self)
        self.tabelkaObwieszczenia.move(500, 64)
        self.tabelkaObwieszczenia.resize(260, 300)
        self.tabelkaObwieszczenia.setRowCount(9)
        self.tabelkaObwieszczenia.setColumnCount(1)
        self.tabelkaObwieszczenia.setColumnWidth(0, 200)
        self.aktualizujEtykietyObwieszczenia()
        # Zablokowanie edycji tabelki.
        self.tabelkaObwieszczenia.setEditTriggers(self.tabelkaObwieszczenia.NoEditTriggers)

        # Obrazek
        self.obrazek = QtGui.QLabel(self)
        self.obrazek.setPixmap(QtGui.QPixmap('app/gui/prezent.jpg'))
        self.obrazek.move(300, 64)

        # Sekcja wyszukiwania
        self.tytulWyszukiwania = QtGui.QLabel(self)
        self.tytulWyszukiwania.setText('<font color="black"><b><h2>Wyszukaj</h2></b></font>')
        self.tytulWyszukiwania.move(48, 80)

        self.tytulSzukaniaWszystkich = QtGui.QLabel(self)
        self.tytulSzukaniaWszystkich.setText('<font color="black"><b>Wśród wszystkich dzieci:</b></font>')
        self.tytulSzukaniaWszystkich.move(64, 110)

        self.przyciskWyszukajWeWszystkich = QtGui.QPushButton('Podaj nazwisko i imię', self)
        self.przyciskWyszukajWeWszystkich.move(64, 130)
        self.przyciskWyszukajWeWszystkich.clicked.connect(self.wyszukajPoWszystkich)

        self.tytulSzukaniaPrzypisanych = QtGui.QLabel(self)
        self.tytulSzukaniaPrzypisanych.setText('<font color="black"><b>Wśród przypisanych:</b></font>')
        self.tytulSzukaniaPrzypisanych.move(64, 200)

        self.przyciskWyszukaj = QtGui.QPushButton('Podaj nazwisko', self)
        self.przyciskWyszukaj.move(64, 220)
        self.przyciskWyszukaj.clicked.connect(self.wyszukajPoNazwisku)

        # Nasze władcze przyciski ]:->
        self.przyciskZatwierdz = QtGui.QPushButton('ZATWIERDŹ', self)
        self.przyciskZatwierdz.move(300, 340)
        self.przyciskZatwierdz.clicked.connect(self.zatwierdz)

        self.przyciskOdrzuc = QtGui.QPushButton('ODRZUĆ', self)
        self.przyciskOdrzuc.move(400, 340)
        self.przyciskOdrzuc.clicked.connect(self.odrzuc)

        # Tytuł dla tabelki
        self.tytulTabelki = QtGui.QLabel(self)
        self.tytulTabelki.setText('<font color="black">Tabela dzieci przypisanych tobie:</font>')
        self.tytulTabelki.move(64, 380)

        # Tabelka z informacjami o dzieciach.
        self.tabelka = QtGui.QTableWidget(self)
        wypelnijTabelke(self.tabelka, self.wynik)
        self.tabelka.move(84, 400)
        self.tabelka.setColumnWidth(0, 50)
        self.tabelka.resize(673, 150)
        self.tabelka.setHorizontalHeaderLabels(('ID Raportu', 'Agent', 'ID Listu', 'Wiek', 'Nazwisko', 'Imię', 'ID'))
        # Zablokowanie edycji tabelki.
        self.tabelka.setEditTriggers(self.tabelka.NoEditTriggers)

        # Ustawiamy, by zmiana zaznaczenia w tabeli aktualizowała podgląd obwieszczenia:
        self.tabelka.itemSelectionChanged.connect(self.aktualizujDynamiczniePodglad)

    def zatwierdz(self):
        zaznaczone = self.tabelka.selectedItems()
        wiersz = zaznaczone[0].row()
        imie = self.tabelka.item(wiersz, 1).text()
        nazwisko = self.tabelka.item(wiersz, 2).text()
        idListu = self.tabelka.item(wiersz, 4).text()
        komentarz, podanoKomentarz = QtGui.QInputDialog.getText(self, 'ZATWIERDŹ', 'Napisz opcjonalny komentarz dla dziecka:\n\n(Uwaga! Zatwierdzasz właśnie list PRZYPISANY TOBIE napisany przez dziecko o nazwisku ' + nazwisko + ' ' + imie + '!)')
        if not podanoKomentarz:
            komentarz = ''
        baza.wykonaj_zapytanie(" select * from wystaw_opinie_dziecku(" + idListu + ",'" + komentarz + "', true);")
        self.aktualizujDynamiczniePodglad()

    def odrzuc(self):
        zaznaczone = self.tabelka.selectedItems()
        wiersz = zaznaczone[0].row()
        imie = self.tabelka.item(wiersz, 1).text()
        nazwisko = self.tabelka.item(wiersz, 2).text()
        idListu = self.tabelka.item(wiersz, 4).text()
        komentarz, podanoKomentarz = QtGui.QInputDialog.getText(self, 'ODRZUĆ', 'Napisz obowiązkowo komentarz dla dziecka:\n\n(Uwaga! Zatwierdzasz odrzucasz list PRZYPISANY TOBIE napisany przez dziecko o nazwisku ' + nazwisko + ' ' + imie + '!)')
        if not podanoKomentarz:
            QtGui.QMessageBox.critical(self, 'Błąd', 'Nie podano komentarza.\n\nNie można odrzucić listu bez podania komentarza! Niegrzeczne dziecko musi dostać pouczenie! Sama rózga nie wystarczy!', QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)
        else:
            baza.wykonaj_zapytanie(" select * from wystaw_opinie_dziecku(" + idListu + ",'" + komentarz + "', false);")
            self.aktualizujDynamiczniePodglad()

    def aktualizujEtykietyObwieszczenia(self):
        self.tabelkaObwieszczenia.setVerticalHeaderLabels(('Nazwisko', 'Imię', 'Wystawiono', 'Zaakcepotowano', 'Komentarz', 'Id raportu', 'Id listu dziecka', 'Wystawiający', 'Nr obwieszczenia'))
        self.tabelkaObwieszczenia.setHorizontalHeaderLabels((' '))

    def aktualizujDynamiczniePodglad(self):
        zaznaczone = self.tabelka.selectedItems()
        wiersz = zaznaczone[0].row()
        idListu = self.tabelka.item(wiersz, 4).text()
        odpowiedzObwieszczen = baza.wykonaj_zapytanie('select * from obwieszczenia_kontroli where idlistudziecka = ' + idListu + ';')
        wypelnijTabelkeOdwrocona(self.tabelkaObwieszczenia, odpowiedzObwieszczen)
        idDziecka = self.tabelka.item(wiersz, 0).text()
        plec = baza.wykonaj_zapytanie('select Plec from schemat.dzieci where id = ' + idDziecka + ';')
        if plec[0][0] == 'M':
            self.obrazek.setPixmap(QtGui.QPixmap('app/gui/chlopczyk.jpg'))
        elif plec[0][0] == 'K':
            self.obrazek.setPixmap(QtGui.QPixmap('app/gui/dziewczynka.jpg'))
        else:
            self.obrazek.setPixmap(QtGui.QPixmap('app/gui/prezent.jpg'))
        self.aktualizujEtykietyObwieszczenia()

    def wyszukajPoWszystkich(self):
        nazwisko, podanoNazwisko = QtGui.QInputDialog.getText(self, 'Nazwisko', 'Podaj nazwisko dziecka:\n\n(Pilnuj wielkich liter!)')

        if podanoNazwisko:
            imie, podanoImie = QtGui.QInputDialog.getText(self, 'Imię', 'Podaj imię dziecka:\n\n(Pilnuj wielkich liter!)')

            if podanoImie:
                znalezione = baza.wykonaj_zapytanie('select * from obwieszczenia_kontroli where "Nazwisko Dziecka" = \'' + nazwisko + '\' and "Imie Dziecka" = \'' + imie + '\';')

                if len(znalezione) > 0:
                    wypelnijTabelkeOdwrocona(self.tabelkaObwieszczenia, znalezione)
                    self.obrazek.setPixmap(QtGui.QPixmap('app/gui/prezent.jpg'))
                    self.aktualizujEtykietyObwieszczenia()

                else:
                    QtGui.QMessageBox.critical(self, 'Błąd', 'Nie ma obwieszczenia kontroli dla takiego dziecka!\nMożliwe przyczyny:\n* To dziecko nie wysłało (jeszcze) listu do Fabryki,\n* Przydzielony dziecku urzędnik nie wystawił jeszcze obwieszczenia dla listu,\n* źle wpisane imię lub/i nazwisko.', QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)

    def wyszukajPoNazwisku(self):
        nazwisko, podanoNazwisko = QtGui.QInputDialog.getText(self, 'Nazwisko', 'Podaj nazwisko dziecka:')

        if podanoNazwisko:
                znalezione = self.tabelka.findItems(nazwisko, QtCore.Qt.MatchContains)

                if len(znalezione) > 0:
                    self.tabelka.setCurrentItem(znalezione[0])

                else:
                    QtGui.QMessageBox.critical(self, 'Błąd', 'Nie znaleziono żadnych dzieci przypisanych tobie.', QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)


class EkranLogowania(EkranKlienta):
    """ Klasa abstrakcyjna dla celów przyszłościowych. """

    def __init__(self, danyPoprzednik=0):
        super(EkranLogowania, self).__init__(False, danyPoprzednik)
        self.pingwin = False


class EkranLogowaniaGnoma(EkranLogowania):

    def __init__(self, danyPoprzednik=0):
        super(EkranLogowaniaGnoma, self).__init__(danyPoprzednik)
        self.tworzEkranLogowaniaGnoma()

    def tworzEkranLogowaniaGnoma(self):
        self.setWindowTitle("Lapland - TWOJE PREZENTY - Logowanie - Gnom")
        self.komunikatLogowania = QtGui.QLabel(self)
        self.komunikatLogowania.setText('<font color="black"><b>Podaj proszę swój login i hasło.</b></font>')
        self.komunikatLogowania.move(32, 258)

        self.etykietaLogin = QtGui.QLabel(self)
        self.etykietaLogin.setText('<font color="black">Login:</font>')
        self.etykietaLogin.move(32, 290)
        self.poleLogin = QtGui.QLineEdit(self)
        self.poleLogin.move(96, 286)

        self.etykietaHaslo = QtGui.QLabel(self)
        self.etykietaHaslo.setText('<font color="black">Hasło:</font>')
        self.etykietaHaslo.move(32, 322)
        self.poleHaslo = QtGui.QLineEdit(self)
        self.poleHaslo.setEchoMode(QtGui.QLineEdit.Password)
        self.poleHaslo.move(96, 314)

        self.zalogujButton = QtGui.QPushButton("Zaloguj", self)
        self.zalogujButton.setToolTip("Zaloguj się jako <b>gnom</b>.")
        self.zalogujButton.clicked.connect(self.zaloguj)
        self.zalogujButton.resize(self.zalogujButton.sizeHint())
        self.zalogujButton.move(256, 312)

    def zaloguj(self, event):
        login = self.poleLogin.text()
        haslo = self.poleHaslo.text()
        baza.zaloguj(login, haslo)
        self.gnomePanel = PanelGnoma(login, self)
        self.gnomePanel.show()
        self.hide()


class EkranLogowaniaDziecka(EkranLogowania):

    def __init__(self, danyPoprzednik=0):
        super(EkranLogowaniaDziecka, self).__init__(danyPoprzednik)
        self.tworzEkranDziecka()

    def tworzEkranDziecka(self):
        self.setWindowTitle("Lapland - TWOJE PREZENTY - Sprawdź status")
        self.label.setPixmap(QtGui.QPixmap('app/gui/tlo-z-prezentem.jpg'))
        self.label.setGeometry(0, 0, 1000, 600)
        self.komunikatLogowania = QtGui.QLabel(self)
        self.komunikatLogowania.setText('<font color="white"><b>Podaj proszę swoje imię, nazwisko i swój kraj pochodzenia,<br><f>abyśmy mogli w pełni stwierdzić Twoją tożsamość.</b></font>')
        self.komunikatLogowania.move(32, 96)

        self.etykietaLogin = QtGui.QLabel(self)
        self.etykietaLogin.setText('<font color="black"><b>Imię:</b></font>')
        self.etykietaLogin.move(32, 460)
        self.poleLogin = QtGui.QLineEdit(self)
        self.poleLogin.move(128, 456)

        self.etykietaHaslo = QtGui.QLabel(self)
        self.etykietaHaslo.setText('<font color="black"><b>Nazwisko:</b></font>')
        self.etykietaHaslo.move(32, 492)
        self.poleHaslo = QtGui.QLineEdit(self)
        self.poleHaslo.move(128, 488)

        self.etykietaKraj = QtGui.QLabel(self)
        self.etykietaKraj.setText('<font color="black"><b>Kraj:</b></font>')
        self.etykietaKraj.move(32, 524)
        self.poleKraj = QtGui.QLineEdit(self)
        self.poleKraj.move(128, 520)

        self.zalogujButton = QtGui.QPushButton("Sprawdź status listu!", self)
        self.zalogujButton.setToolTip("Tutaj możesz sprawdzić, czy twój list został zaakceptowany.")
        self.zalogujButton.clicked.connect(self.sprawdzList)
        self.zalogujButton.resize(self.zalogujButton.sizeHint())
        self.zalogujButton.move(600, 460)

        #self.tabelka = QtGui.QTableWidget(self)
        #self.tabelka.move(84, 32)
        #self.tabelka.setColumnWidth(0, 50)
        #self.tabelka.resize(673, 150)
        #self.tabelka.setHorizontalHeaderLabels(('ID Raportu', 'Agent', 'ID Listu', 'Wiek', 'Nazwisko', 'Imię', 'ID'))
        # Zablokowanie edycji tabelki.
        #self.tabelka.setEditTriggers(self.tabelka.NoEditTriggers)

    def sprawdzList(self, event):
        imie = self.poleLogin.text()
        nazwisko = self.poleHaslo.text()
        kraj = self.poleKraj.text()
        baza.zaloguj('dziecko', 'lapland')
        odpowiedz = baza.wykonaj_zapytanie('select * from widok_dziecka(\'' + imie + '\', \'' + nazwisko + '\', \'' + kraj + '\') fetch first row only;')
        baza.koniec()
        if len(odpowiedz) < 1:
            QtGui.QMessageBox.information(self, 'Status', 'Niestety, ale informacja nie jest w tym momencie dostępna dla takich danych!<br><br>Sprawdź swoje dane!<br><br>Jeśli są poprawne, to znaczy, że Twój list nie dotarł jeszcze do Mikołaja lub Mikołaj nie znalazł jeszcze czasu na przeczytanie Twojego listu - ma ich tak dużo! Cierpliwości!', QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)
            return
        zaakceptowano = odpowiedz[0][3]
        komentarz = odpowiedz[0][4]
        rozdano = odpowiedz[0][9]
        zaakceptowano_tekst = ''
        komentarz_tekst = ''
        if rozdano:
            QtGui.QMessageBox.information(self, 'Status', 'Prezenty zostały już rozdane! Sprawdź swój kominek!', QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)
            pass
        if zaakceptowano:
            zaakceptowano_tekst = "Twój list został pomyślne zaakceptowany!\nGratulujemy! "

            if komentarz != '' and komentarz != ' ' and komentarz != 0:
                komentarz_tekst = "Mikołaj zostawił dla Ciebie wiadomość: " + komentarz + " - przekazał gnom " + odpowiedz[0][0] + " w imieniu Mikołaja."
        else:
            zaakceptowano_tekst = "Niestety, ale Twój list nie został zaakceptowany przez Mikołaja :( "
            komentarz_tekst = "Spróbuj w przyszłym roku! Postaraj się i bądź grzeczny!\nMikołaj zostawił dla Ciebie poradę: " + komentarz + " - przekazał gnom " + odpowiedz[0][0] + " w imieniu Mikołaja."

        QtGui.QMessageBox.information(self, 'Status', zaakceptowano_tekst + komentarz_tekst, QtGui.QMessageBox.Ok, QtGui.QMessageBox.Ok)


class EkranGlowny(EkranKlienta):
    """ Ekran główny - logowanie. """

    def __init__(self):
        super(EkranGlowny, self).__init__(True)
        self.pingwin = True
        self.tworzEkranGlowny()

    def tworzEkranGlowny(self):
        self.setWindowTitle("Lapland - TWOJE PREZENTY - Logowanie")
        self.dzieckoButton = QtGui.QPushButton("DZIECKO", self)
        self.dzieckoButton.setToolTip("Zaloguj się jako <b>dziecko</b>.")
        self.dzieckoButton.clicked.connect(self.dziecko)
        self.dzieckoButton.resize(self.dzieckoButton.sizeHint())
        self.dzieckoButton.move(185, 258)
        self.gnomButton = QtGui.QPushButton("GNOM", self)
        self.gnomButton.setToolTip("Zaloguj się jako <b>gnom</b>.")
        self.gnomButton.clicked.connect(self.gnom)
        self.gnomButton.resize(self.gnomButton.sizeHint())
        self.gnomButton.move(32, 258)
        self.tytul = QtGui.QLabel(self)
        self.tytul.setText('<font color="black"><b><h1>Lapland</h1></b><h2>TWOJE PREZENTY</h2></font>')
        self.tytul.move(32, 32)
        self.kimJestes = QtGui.QLabel(self)
        self.kimJestes.setText('<font color="black">Kim jesteś? - zaloguj się:</font>')
        self.kimJestes.move(73, 216)
        """ Ekrany logowania """
        self.ekranLogowaniaDziecka = EkranLogowaniaDziecka()
        self.ekranLogowaniaGnoma = EkranLogowaniaGnoma()

    def ustaw_okno_dziecka(self, okno):
        self.ekranLogowaniaDziecka = okno

    def ustaw_okno_gnoma(self, okno):
        self.ekranLogowaniaGnoma = okno

    def dziecko(self):
        self.ekranLogowaniaDziecka.show()
        self.ekranLogowaniaDziecka.centruj()
        self.hide()

    def gnom(self):
        self.ekranLogowaniaGnoma.show()
        self.ekranLogowaniaGnoma.centruj()
        self.hide()


def main():
    app = QtGui.QApplication(sys.argv)

    """ Tworzymy wszystkie ekrany na początku, lecz wyświetlamy pojedyńczo."""
    ekranGlowny = EkranGlowny()
    ekranGlowny.show()

    logowanieDziecka = EkranLogowaniaDziecka(ekranGlowny)
    ekranGlowny.ustaw_okno_dziecka(logowanieDziecka)

    logowanieGnoma = EkranLogowaniaGnoma(ekranGlowny)
    ekranGlowny.ustaw_okno_gnoma(logowanieGnoma)

    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
