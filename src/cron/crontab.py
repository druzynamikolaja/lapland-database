#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Skrypt dodający tworzenie kopii bazy do crontaba.
"""

import os

if not os.path.exists(os.path.expanduser('~') + '/.pgpass'):
    print('Nie wykryto pgpass. Czy na pewno chcesz kontynuować?')
    decyzja = input().strip()
    if not (decyzja == 'y' or decyzja == 't'):
        exit()

plik = os.path.abspath('zrzut_bazy.py')

crontab = os.popen('crontab -l')
for i in crontab:
    if plik in i:
        print('W crontabie jest już wpis.')
        exit()

polecenie = 'crontab -l |{ cat; echo "5 3 * * * "' + plik + '; }| crontab -'
os.system(polecenie)
