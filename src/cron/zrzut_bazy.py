#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Skrypt wykonujący kopie zapasowe bazy.
"""

import os
import sys
sys.path.append(os.path.dirname(sys.argv[0]) + '/../py/database')  # karkołomna ścieżka do wewnętrznych modułów
import ustawienia
import datetime

plik = ustawienia.KOPIE_ZAPASOWE + '/' + ustawienia.NAZWA_BAZY + '-' + datetime.datetime.now().strftime('%y%m%d%H%M') + '.sql'
polecenie = 'pg_dump -U ' + ustawienia.UZYTKOWNIK + ' -h ' + ustawienia.HOST + ' ' + ustawienia.NAZWA_BAZY + ' -f ' + plik
os.system(polecenie)

# i jeszcze kompresja
polecenie = 'xz ' + plik
os.system(polecenie)
