-- http://www.postgresql.org/docs/9.2/static/sql-createtrigger.html



/**
 * @author	Konrad Talik
 *			Wyzwalacz przydzielający zastepstwo na miejsce zwolnionego pracownika.
 *			Zastępstwo jest realizowane tylko dla maszynistów i pracowników sal.
 */

/**

CREATE OR REPLACE FUNCTION zastepstwo()
RETURNS TRIGGER AS $$

DECLARE
kogo_zastapic int;
id_zastepcy int;
id_kadry int;

BEGIN
	IF (NEW.UrlopZdrowotny = TRUE) THEN -- przydzielono urlop
		-- Sprawdzamy które osoby nie mają jeszcze zastępstwa.
		-- Tzn., że musimy sprawdzić, czy są w tabeli Pracownicy z danym KońcemUrlopu
		-- oraz czy są jednocześnie w tabeli 
		-- Maszynisci lub tabeli PracownicySali (bo jeszcze ich nie zastępiono!).

		LOOP -- Sprawdzamy dla wszystkich obecnie nie mających zastępstwa.
			kogo_zastapic = (SELECT Id FROM schemat.Pracownicy
				WHERE UrlopZdrowotny = TRUE
				AND ( 
					Id IN (SELECT IdKartyZakladowej FROM schemat.Maszynisci)
					OR Id IN (SELECT IdKartyZakladowej FROM schemat.PracownicySali)
					)
				FETCH FIRST ROW ONLY);

			-- Nie ma już kogo zastąpić.
			EXIT WHEN kogo_zastapic IS NULL;

			-- Sprawdzamy kadrę pracownika na urlopie.
			id_kadry = (SELECT IdKadry FROM schemat.Pracownicy
				WHERE Id = kogo_zastapic
				FETCH FIRST ROW ONLY);
			
			id_zastepcy = (SELECT Id FROM Bezrobotni
				WHERE IdKadry = id_kadry
				FETCH FIRST ROW ONLY);

			-- Nie mamy kim zastąpić chorego!
			EXIT WHEN id_zastepcy IS NULL;

			-- Zastępstwo dla maszynistów.
			IF id_kadry = 4 THEN
				UPDATE schemat.Maszynisci SET IdKartyZakladowej = id_zastepcy
					WHERE IdKartyZakladowej = kogo_zastapic;
			ELSE
				UPDATE schemat.PracownicySali SET IdKartyZakladowej = id_zastepcy
					WHERE IdKartyZakladowej = kogo_zastapic;
			END IF;

		END LOOP;

	END IF; -- wpp. urlop się skończył i nic nie robimy
	RETURN NEW;
END

$$ LANGUAGE plpgsql;


CREATE TRIGGER Przydziel_zastepstwo
BEFORE UPDATE OF UrlopZdrowotny, KoniecUrlopu ON schemat.Pracownicy
FOR EACH ROW EXECUTE PROCEDURE zastepstwo();

*/


/*
 TRIGGER KTORY WRAZ Z UPDATE OBWIESZCZENIA_KONTROLI AKTUALIZUJE DATE WYSTAWIENIA ALE TYLKO PODCZAS ZMIANY
	KOMENTARZA LUB DECYZJI O AKCEPTACJI!
 @author: Piotr Kruk
 */



CREATE OR REPLACE FUNCTION data_obwieszczenia_procedura()
RETURNS TRIGGER AS
$$
BEGIN
		IF (NEW.zaakceptowano <> OLD.zaakceptowano OR NEW.komentarz <> OLD.komentarz) THEN 
			NEW.datawystawienia = CURRENT_DATE;
		END IF;
		RETURN NEW;
END
$$
LANGUAGE plpgsql;


CREATE  TRIGGER Data_obwieszczenia
BEFORE UPDATE ON schemat.obwieszczeniakontroli
FOR EACH ROW EXECUTE PROCEDURE data_obwieszczenia_procedura();



--- przyklad usuniecia : 
-- DROP TRIGGER dateobwieszczenia ON obwieszczeniakontroli;

/*
@author Piotr Kruk
trigger zabezpieczajacy zamowienia, nie moze byc ujemna liczba zamowien!

*/



CREATE OR REPLACE FUNCTION safe_transaction()
RETURNS TRIGGER AS
$$
BEGIN
	IF (NEW.ilosczamowien < 0) THEN
		NEW.ilosczamowien = OLD.ilosczamowien;
	ELSIF (NEW.ilosczamowien <> OLD.ilosczamowien) THEN
		NEW.ilosczamowien = NEW.ilosczamowien + OLD.ilosczamowien;
	END IF;
	RETURN NEW;
END
$$
LANGUAGE PLPGSQL;


CREATE TRIGGER Bezpieczne_zamowienia
BEFORE UPDATE ON schemat.Fabryka
FOR EACH ROW EXECUTE PROCEDURE safe_transaction();



-- Przyklad usuniecia 
--drop trigger bezpieczne_zamowienia on schemat.fabryka;


/*
@author : Piotr Kruk
trigger aktualizujacy w tabeli agencje kolumne ilosc aktywnych agentow 
domyslnie ustawiam wszystkich dodanych agentow na aktywnych, nieaktywny gdy ma urlop zdrowotny
prawda ? 
*/



CREATE OR REPLACE FUNCTION agenci_procedura() RETURNS TRIGGER AS $agenci$
DECLARE 
a boolean;
b int;
BEGIN
	IF (TG_OP = 'INSERT') THEN
		a = TRUE;
		b = NEW.idpracownika;
		IF EXISTS (SELECT * FROM schemat.pracownicy WHERE id = b AND urlopzdrowotny = FALSE) THEN
				UPDATE schemat.agencje SET iloscprzydzielonychagentow = iloscprzydzielonychagentow + 1,
				iloscaktywnychagentow = iloscaktywnychagentow +1
				WHERE id = NEW.idagencji; -- Gdy nie posiada urlopu
				RETURN NEW;
			RETURN NEW;
		ELSIF  EXISTS (SELECT * from schemat.pracownicy where id = b AND urlopzdrowotny = true) THEN
			UPDATE schemat.agencje set iloscprzydzielonychagentow = iloscprzydzielonychagentow + 1
			WHERE id = NEW.idagencji;-- gdy jest urlop
			RETURN NEW; --\\\
		END IF; --- *
	ELSIF (TG_OP = 'DELETE' ) THEN
		b = OLD.idpracownika;
		IF EXISTS (SELECT * FROM schemat.pracownicy WHERE id = b AND urlopzdrowotny = TRUE ) THEN
				UPDATE schemat.agencje set iloscprzydzielonychagentow = iloscprzydzielonychagentow -1
				WHERE id = OLD.idagencji;
				UPDATE  schemat.agencje set iloscprzydzielonychagentow = 0
				WHERE id = old.idagencji
				AND iloscprzydzielonychagentow < 0;
			RETURN OLD;
		ELSIF   EXISTS (SELECT * FROM schemat.pracownicy WHERE id = b AND urlopzdrowotny = FALSE) THEN
				UPDATE schemat.agencje SET iloscprzydzielonychagentow = iloscprzydzielonychagentow -1,
				iloscaktywnychagentow = iloscaktywnychagentow -1
				WHERE id = OLD.idagencji; 

				UPDATE schemat.agencje SET iloscprzydzielonychagentow = 0
				WHERE id = OLD.idagencji
				AND  iloscprzydzielonychagentow < 0;
				UPDATE schemat.agencje SET iloscaktywnychagentow = 0
				WHERE id = OLD.idagencji
				AND iloscaktywnychagentow < 0; 
			RETURN OLD;
		END IF;
	END IF; --- :)
END
$agenci$ LANGUAGE PLPGSQL;


CREATE TRIGGER Aktualizuj_agentow
BEFORE INSERT OR DELETE ON schemat.agenci
FOR EACH ROW EXECUTE PROCEDURE agenci_procedura();



/*
USUWANIE : 

drop trigger agenci on schemat.agenci;
drop function agenci();

*/



/**
 * @author	Konrad Talik
 *			Wyzwalacz przydzielający pracownikowi minimalną stawkę płacy,
 *			standardowego zwierzchnika oraz UrlopZdrowotny = FALSE
 *			po operacji insert w tabeli Pracownicy.
 */



CREATE OR REPLACE FUNCTION inicjuj_pracownika_procedura() RETURNS TRIGGER AS 
$inicjuj$

BEGIN
	IF NEW.Pensja IS NULL THEN
		UPDATE schemat.Pracownicy SET Pensja = 
			(SELECT MinimalnaStawka FROM schemat.Kadry WHERE Id = NEW.IdKadry)
		WHERE Id = NEW.Id;
	END IF;
	IF NEW.IdSzefa IS NULL THEN
		UPDATE schemat.Pracownicy SET IdSzefa =
			(SELECT Id from Dyrektor FETCH FIRST ROW ONLY)
		WHERE Id = NEW.Id;
	END IF;
	NEW.UrlopZdrowotny = FALSE;
	RETURN NEW;
END

$inicjuj$
LANGUAGE PLPGSQL;


CREATE TRIGGER Inicjuj_pracownika
AFTER INSERT ON schemat.Pracownicy
FOR EACH ROW EXECUTE PROCEDURE inicjuj_pracownika_procedura();



/*
@author Piotr Kruk
Trigger uniemozliwia Rozdanie prezentow w danych regionach jeśli nie ma 24 lub 25 grudnia.
Trigger wyświetla okno błędu. 
*/



CREATE OR REPLACE FUNCTION zabezpiecz_regiony() RETURNS TRIGGER AS
$zabezpiecz$
DECLARE
miesiac int;
dzien int;
BEGIN
	miesiac = (select extract (MONTH FROM CURRENT_DATE) limit 1);
	dzien = (select extract (DAY FROM CURRENT_DATE) limit 1);
	IF (NEW.prezentyrozdane = TRUE AND miesiac <> 12 and (dzien <> 24 AND dzien <> 25) ) THEN
		NEW.prezentyrozdane = false;
		RAISE EXCEPTION 'nie ma jeszcze swiat, nie mozna rozdawac prezentow!';
		return NEW;
	END IF;
	return new;
END

$zabezpiecz$
LANGUAGE PLPGSQL;


CREATE TRIGGER zabezpiecz_swieta
BEFORE UPDATE ON schemat.Regiony
FOR EACH ROW EXECUTE PROCEDURE zabezpiecz_regiony();



/**
--- Przyklad uzycia : 
-- select * from schemat.regiony;
-- update schemat.regiony set prezentyrozdane = true;
--- Usuniecie triggeru : 
-- drop trigger zabezpiecz_swieta on schemat.regiony;
*/

/*
@author Piotr Kruk 
Z kazdym nowym dziecku w regionach jest aktualizowana ilosc dzieci na dany region.

*/



CREATE OR REPLACE FUNCTION licz_dzieci() RETURNS TRIGGER AS
$dzieci$
BEGIN
	IF (TG_OP = 'INSERT') THEN
		UPDATE schemat.regiony SET liczbadzieci = liczbadzieci+1 where numerregionu = NEW.regionpochodzenia;
	ELSIF (TG_OP = 'DELETE') THEN
		UPDATE schemat.regiony SET liczbadzieci = liczbadzieci-1 where numerregionu = NEW.regionpochodzenia;

	END IF;
	RETURN NEW;
END
$dzieci$
LANGUAGE PLPGSQL;


CREATE TRIGGER Ilosc_dzieci
BEFORE INSERT OR DELETE  ON schemat.Dzieci
FOR EACH ROW EXECUTE PROCEDURE licz_dzieci();




/**
--- Przyklad uzycia
-- insert into schemat.dzieci values (29,1,'xxx','xxx',1,10,'M');
--- Przyklad usuniecia
-- drop trigger ilosc_dzieci on schemat.Dzieci;
*/
