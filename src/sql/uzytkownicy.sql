-- Hasło dla superusera (oczywiście standardowo lokalnie nie jest ono wymagane)
-- ALTER USER postgres WITH PASSWORD 'placki';  -- to coś psuje obecne hasło, lepiej ustawić na własną rękę

-- Usuwanie ról.
DROP ROLE IF EXISTS gnom;
DROP ROLE IF EXISTS mikolaj;
DROP ROLE IF EXISTS dziecko;

-- Usuwanie konkretnych userów.
DROP ROLE IF EXISTS santa;
-- Gnomy.
DROP ROLE IF EXISTS ruby;
DROP ROLE IF EXISTS mily;
DROP ROLE IF EXISTS rodwen;
DROP ROLE IF EXISTS legolas;
DROP ROLE IF EXISTS bruce;
DROP ROLE IF EXISTS gimli;
DROP ROLE IF EXISTS bongowong;
DROP ROLE IF EXISTS wyrmir;
DROP ROLE IF EXISTS bongopetersen;
DROP ROLE IF EXISTS rudi;
DROP ROLE IF EXISTS zmirlacz;
DROP ROLE IF EXISTS gimlivanya;
DROP ROLE IF EXISTS ypman;
DROP ROLE IF EXISTS dawidson;

-- Podstawowe role w bazie danych.
CREATE ROLE gnom NOINHERIT;
GRANT EXECUTE ON FUNCTION id_gnoma(text) TO gnom; 
GRANT EXECUTE ON FUNCTION widok_gnoma(int) TO gnom;
GRANT SELECT ON obwieszczenia_kontroli TO gnom;
GRANT EXECUTE ON FUNCTION wystaw_opinie_dziecku(int, text, boolean) TO gnom;
GRANT USAGE ON SCHEMA schemat TO gnom;
GRANT SELECT ON schemat.Dzieci TO gnom;

CREATE ROLE mikolaj NOINHERIT;
GRANT USAGE ON SCHEMA schemat TO mikolaj;
GRANT SELECT ON schemat.Regiony TO mikolaj;
GRANT SELECT ON schemat.Dzieci TO mikolaj;
GRANT SELECT ON schemat.Mikolaje TO mikolaj;

CREATE ROLE dziecko LOGIN;
GRANT EXECUTE ON FUNCTION widok_dziecka(text, text, text) TO dziecko;

-- -- Tworzenie konkretnych użytkowników.
-- Mikołaje
CREATE ROLE santa WITH LOGIN PASSWORD 'hohoho' INHERIT; GRANT mikolaj TO santa;

-- Gnomy
CREATE ROLE ruby WITH LOGIN PASSWORD 'tajnehaslo' INHERIT; GRANT gnom TO ruby;
CREATE ROLE mily WITH LOGIN PASSWORD 'dopracydopracy' INHERIT; GRANT gnom TO mily;
CREATE ROLE rodwen WITH LOGIN PASSWORD 'qwerty123' INHERIT; GRANT gnom TO rodwen;
CREATE ROLE legolas WITH LOGIN PASSWORD 'ilikebows' INHERIT; GRANT gnom TO legolas;
CREATE ROLE bruce WITH LOGIN PASSWORD 'ecurb' INHERIT; GRANT gnom TO bruce;
CREATE ROLE gimli WITH LOGIN PASSWORD 'striketheearth' INHERIT; GRANT gnom TO gimli;
CREATE ROLE bongowong WITH LOGIN PASSWORD 'kongo' INHERIT; GRANT gnom TO bongowong;
CREATE ROLE wyrmir WITH LOGIN PASSWORD 'root' INHERIT; GRANT gnom TO wyrmir;
CREATE ROLE bongopetersen WITH LOGIN PASSWORD 'haslo' INHERIT; GRANT gnom TO bongopetersen;
CREATE ROLE rudi WITH LOGIN PASSWORD 'rudialya' INHERIT; GRANT gnom TO rudi;
CREATE ROLE zmirlacz WITH LOGIN PASSWORD 'grafy' INHERIT; GRANT gnom TO zmirlacz;
CREATE ROLE gimlivanya WITH LOGIN PASSWORD 'abc123def456' INHERIT; GRANT gnom TO gimlivanya;
CREATE ROLE ypman WITH LOGIN PASSWORD 'yp' INHERIT; GRANT gnom TO ypman;
CREATE ROLE dawidson WITH LOGIN PASSWORD 'tatiana69' INHERIT; GRANT gnom TO dawidson;

