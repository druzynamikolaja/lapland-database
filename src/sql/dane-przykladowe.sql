-- przykładowi pracownicy
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Legolas', 'Nowak', 'Laponia, Lapland');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Lily', 'Winter', 'Szwecja, Laply');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Mily', 'Beard', 'Laponia, Lodowiec');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Aragorn', 'Wedrowiec', 'Polska, Kielce');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Wyrmir', 'Szalony', 'Russia, Kijow');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Abigail', 'Maerwen', 'Laponia, Lapland');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Adrienne', 'Alya', 'Laponia, Pingwincity');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Alanna', 'Vanya', 'Laponia, Pingwincity');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Earmaiviel', 'Xen', 'Laponia, Pingwincity');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Rodwen', 'Violet', 'Szwecja, Lodowiec');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Ethiriel', 'Tor', 'Russia, Kijow');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Tilda', 'Aindeldiel', 'Linux, Terminal');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Taylor', 'Yaviel', 'Francja, Paris');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Thy', 'Aranel', 'Poland, Opole');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Tracy', 'Trinity', 'Laponia, Lapland');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Tatiana', 'Eruanna', 'Laponia, Lapland');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Miroslawa', 'Holythree', 'Poland, Warsaw');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Ruby', 'Tail', 'Laponia, Lapland');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Rudi', 'Alko', 'Russia, Moscow');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Zmirlacz', 'Petersen', 'Polska, Cracow');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Gimli', 'Tor', 'England, Moria');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Gwarek', 'Gburek', 'Czechy, Praga');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Zgredek', 'Zgred', 'England, Hogwart');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Grzmot', 'Dawidson', 'Germany, Berlin');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Borom', 'Ypman', 'Japan, Tokio');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Wasyl', 'Wiliczenko', 'Russia, Moscow');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'James', 'Wong', 'England, London');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Bongo', 'Bongo', 'Africa, Kongo');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Yp', 'Man', 'Japan, Tokio');
INSERT INTO przykladowe.Pracownicy VALUES (DEFAULT, 'Bruce', 'Wee', 'Spain, Corrida');

-- przykładowe dzieci
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Siergiej', 'Wasilowicz','M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Wasyl', 'Tamburino','M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Jean', 'Pierre','M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Abram', 'Imbramowicz','M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Jan', 'Nowak','M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Mirmir', 'Zbor','M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Ayoko', 'Siririmi', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'James', 'Borman','M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Hans', 'Zimmerman', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Helmut', 'Sturm', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Sasha', 'White', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Alexia','Molotow', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Boguslawa', 'Dwatri', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Chris', 'Obuszenko', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Julietta', 'Franque', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Esmeralda', 'von Dia', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Maria', 'Hosse', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Kate', 'Kde', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Miriam', 'Weasley', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Helga', 'Wermut', 'K');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Andriej', 'Sacharow', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Mariusz', 'Nowak', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Jon', 'Sailor', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Ip', 'Wan', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Jackie', 'Wan', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Willy', 'Trau', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Johnny', 'Nice', 'M');
INSERT INTO przykladowe.Dzieci VALUES (DEFAULT, 'Angelina', 'Beauty', 'K'); 


-- przykładowe maszyny
INSERT INTO przykladowe.Maszyny VALUES (DEFAULT, 'Sprezarka');
INSERT INTO przykladowe.Maszyny VALUES (DEFAULT, 'Gwiazdiarka');
INSERT INTO przykladowe.Maszyny VALUES (DEFAULT, 'MagicznyKufer');
