-- domyślny schemat
CREATE SCHEMA schemat;

-- schemat do danych przykładowych
CREATE SCHEMA przykladowe;


CREATE TABLE schemat.Listy (
  Id SERIAL PRIMARY KEY,
  DataOtrzymania date NOT NULL,
  OdDziecka bool
);

CREATE TABLE schemat.RaportyObserwacji (
  IdListu int PRIMARY KEY REFERENCES schemat.Listy(Id) ON DELETE CASCADE,
  IdAgenta int NOT NULL, -- od agentow lub  pracownikow 
  DataUkonczenia date
);

CREATE TABLE schemat.Nacje (
  Id SERIAL PRIMARY KEY,
  NazwaNacji varchar(45) NOT NULL,
  SrednieIQ int NOT NULL,
  SredniaPredkoscChodu int NOT NULL,
  SredniWzrost int NOT NULL
);

CREATE TABLE schemat.Kadry (
  Id SERIAL PRIMARY KEY,
  IdNacji int REFERENCES schemat.Nacje(Id),
  NazwaKadry varchar(45) NOT NULL,
  MinimalnaStawka money NOT NULL
);

CREATE TABLE schemat.Pracownicy (
  Id SERIAL PRIMARY KEY,
  IdKadry int REFERENCES schemat.Kadry(Id),
  IdNacji int REFERENCES schemat.Nacje(Id),
  Imie varchar(45) NOT NULL,
  Nazwisko varchar(45) NOT NULL,
  Adres varchar(45) NOT NULL,
  UrlopZdrowotny bool,
  KoniecUrlopu date,
  IdSzefa int REFERENCES schemat.Pracownicy(Id),
  Pensja money
);

CREATE TABLE schemat.Sanie (
  Id SERIAL PRIMARY KEY,
  Pojemnosc int NOT NULL,
  Rejestracja varchar(45)
);

CREATE TABLE schemat.Mikolaje (
  IdPracownika int PRIMARY KEY REFERENCES schemat.Pracownicy(Id) ON DELETE CASCADE,
  IdSani int REFERENCES schemat.Sanie(Id),
  Pseudonim varchar(45)
);

CREATE TABLE schemat.Regiony (
  NumerRegionu SERIAL PRIMARY KEY,
  IdMikolaja int REFERENCES schemat.Mikolaje(IdPracownika),
  Panstwo varchar(45),
  PrezentyRozdane bool,
  LiczbaDzieci int
);
---------------------------------BRAKUJACY APARAT AGENTURY -----------------

CREATE TABLE schemat.Agencje (
	Id SERIAL PRIMARY KEY,
	Adres varchar(45),
	GlownyRegion int REFERENCES schemat.Regiony(NumerRegionu),
	IloscPrzydzielonychAgentow int,
	IloscAktywnychAgentow int
);


CREATE TABLE schemat.Agenci (
	IdPracownika int PRIMARY KEY REFERENCES schemat.Pracownicy(Id) ON DELETE CASCADE,
	IdAgencji int REFERENCES schemat.Agencje(Id),
	Kryptonim varchar(45)
);

--------------------------------------END ------------------------------------
CREATE TABLE schemat.Fabryka (
  NumerWydzialu SERIAL PRIMARY KEY,
  Administrator int REFERENCES schemat.Pracownicy(Id),
  NazwaWydzialu varchar(80),
  IloscZamowien int,
  Zrealizowano int
);

CREATE TABLE schemat.Sale (
  NumerSali SERIAL PRIMARY KEY,
  NumerWydzialu int REFERENCES schemat.Fabryka(NumerWydzialu),
  IloscStanowisk int NOT NULL,
  IloscStanowiskWolnych int NOT NULL
);

CREATE TABLE schemat.Biura (
  NumerSali int PRIMARY KEY REFERENCES schemat.Sale (NumerSali) ON DELETE CASCADE,
  NumerTelefonu int,
  AdresBiura varchar(45) NOT NULL,
  IloscKomputerow int,
  Drukarka BOOL,
  Ksero BOOL
);

CREATE TABLE schemat.Archiwa (
  NumerBiura int PRIMARY KEY REFERENCES schemat.Biura(NumerSali) ON DELETE CASCADE,
  ArchiwumKartotek bool DEFAULT false
);

CREATE TABLE schemat.Szafki (
  NumerSzafki int NOT NULL,
  NumerBiura int REFERENCES schemat.Archiwa(NumerBiura) ON DELETE CASCADE NOT NULL,
  -- usunęliśmy archiwum, więc usuwamy szafki!
  RodzajSzafki smallint NOT NULL,
  PRIMARY KEY (NumerSzafki, NumerBiura)
);

CREATE TABLE schemat.UrzednicyKontroli (
  IdPracownika int PRIMARY KEY REFERENCES schemat.Pracownicy(Id) ON DELETE CASCADE,
  NumerSzafki int NOT NULL,
  NumerBiuraSzafki int NOT NULL,
  NumerBiuraGabinetu int REFERENCES schemat.Biura(NumerSali),
  Wytrzymalosc int,
  Login	varchar(45),
  FOREIGN KEY (NumerSzafki, NumerBiuraSzafki) REFERENCES schemat.Szafki(NumerSzafki, NumerBiura)
);


CREATE TABLE schemat.Dzieci (
  Id SERIAL PRIMARY KEY,
  RegionPochodzenia int REFERENCES schemat.Regiony(NumerRegionu) ON DELETE CASCADE,
  -- nie chcemy tam dawać prezentów? usuwamy dzieci!
  Imie varchar(45) NOT NULL,
  Nazwisko varchar(45),
  NumerKartoteki int,
  Wiek smallint,
  Plec char(1)
);

CREATE TABLE schemat.ListyOdDzieci (
  IdListu int PRIMARY KEY REFERENCES schemat.Listy(Id) ON DELETE CASCADE,
  IdDziecka int REFERENCES schemat.Dzieci(Id)
);

CREATE TABLE schemat.Renifery (
  Id SERIAL PRIMARY KEY,
  IdSani int REFERENCES schemat.Sanie(Id),
  Pseudonim varchar(45),
  Zakupiony bool
);

CREATE TABLE schemat.ObwieszczeniaKontroli (
  NrObwieszczenia SERIAL PRIMARY KEY,
  Wystawiajacy int REFERENCES schemat.UrzednicyKontroli(IdPracownika),
  IdListuDziecka int REFERENCES schemat.ListyOdDzieci(IdListu) ON DELETE CASCADE,
  -- Znikają listy? - Znikają też obwieszczenia.
  IdRaportu int REFERENCES schemat.RaportyObserwacji(IdListu),
  Zaakceptowano bool,
  Komentarz varchar(45),
  DataWystawienia date
);

CREATE TABLE schemat.Bhp (
  Id SERIAL PRIMARY KEY,
  Przepisy varchar(80),
  WartoscUrlopu int -- wartosc urlopu w dniach.
);

CREATE TABLE schemat.SaleProdukcji (
  NumerSali int PRIMARY KEY REFERENCES schemat.Sale(NumerSali) ON DELETE CASCADE,
  PrzedmiotProdukcji varchar(45),
  IdBhp int REFERENCES schemat.Bhp(Id)
);

CREATE TABLE schemat.Magazyny (
  NumerSali int PRIMARY KEY REFERENCES schemat.Sale(NumerSali) ON DELETE CASCADE,
  Pelny bool,
  Pojemnosc int
);

CREATE TABLE schemat.MagazynSaliProdukcji (
  NumerSaliProdukcji int REFERENCES schemat.SaleProdukcji(NumerSali) ON DELETE CASCADE,
  NumerSaliMagazynu int REFERENCES schemat.Magazyny(NumerSali) ON DELETE CASCADE,
  PRIMARY KEY (NumerSaliProdukcji, NumerSaliMagazynu),
  Przeplyw int
);

CREATE TABLE schemat.Tragarze (
  IdPracownika int PRIMARY KEY REFERENCES schemat.Pracownicy(Id) ON DELETE CASCADE,
  Plecak int,
  IdSani int REFERENCES schemat.Sanie(Id),
  NumerSaliMagazynu int REFERENCES schemat.Magazyny(NumerSali),
  NumerSaliProdukcji int REFERENCES schemat.SaleProdukcji(NumerSali)
);

CREATE TABLE schemat.PracownicySali (
  IdKartyZakladowej int PRIMARY KEY REFERENCES schemat.Pracownicy(Id) ON DELETE CASCADE,
  NumerSali int REFERENCES schemat.SaleProdukcji(NumerSali),
  ObslugaTasmy bool,
  RobotyReczne bool,
  Sprzatanie bool
);

CREATE TABLE schemat.Maszynisci (
  IdKartyZakladowej int PRIMARY KEY REFERENCES schemat.Pracownicy(Id) ON DELETE CASCADE,
  NumerSali int REFERENCES schemat.SaleProdukcji(NumerSali),
  NumerMaszyny int,
  NumerStanowiska int,
  Konserwator bool
);

CREATE TABLE przykladowe.Pracownicy (
  Id SERIAL,
  Imie varchar(45) NOT NULL,
  Nazwisko varchar(45) NOT NULL,
  Adres varchar(45) NOT NULL
);

CREATE TABLE przykladowe.Dzieci (
  Id SERIAL,
  Imie varchar(45) NOT NULL,
  Nazwisko varchar(45) NOT NULL,
  Plec char(1) NOT NULL
);

CREATE TABLE przykladowe.Maszyny (
  Id SERIAL,
  Maszyna VARCHAR(45) NOT NULL
);
