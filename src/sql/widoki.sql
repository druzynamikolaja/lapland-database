/*
 * Widoki zwracające same Id pracowników danych kadr.
 */

/* @author m1s13k */
CREATE OR REPLACE VIEW Maszynisci AS SELECT row_number() over() as i, * FROM (SELECT Id FROM schemat.Pracownicy WHERE idkadry = 4) as Przydzial;

/* @author ktalik */
CREATE OR REPLACE VIEW Wywiad AS SELECT row_number() over() as i, * FROM (SELECT Id FROM schemat.Pracownicy WHERE idkadry = 1) as Przydzial;

CREATE OR REPLACE VIEW Fizyczni AS SELECT row_number() over() as i, * FROM (SELECT Id FROM schemat.Pracownicy WHERE idkadry = 2) as Przydzial;

CREATE OR REPLACE VIEW Urzednicy AS SELECT row_number() over() as i, * FROM (SELECT Id FROM schemat.Pracownicy WHERE idkadry = 3) as Przydzial;

CREATE OR REPLACE VIEW Sekretarze AS SELECT row_number() over() as i, * FROM (SELECT Id FROM schemat.Pracownicy WHERE idkadry = 5) as Przydzial;

CREATE OR REPLACE VIEW Administracja AS SELECT row_number() over() as i, * FROM (SELECT Id FROM schemat.Pracownicy WHERE idkadry = 6) as Przydzial;

CREATE OR REPLACE VIEW Mikolaje AS SELECT row_number() over() as i, * FROM (SELECT Id FROM schemat.Pracownicy WHERE idkadry = 7) as Przydzial;



/*
 * Funkcja wyświetlająca Id pracowników zadanej kadry.
 * @author ktalik
 */



CREATE OR REPLACE FUNCTION Widok_kadry(ZadaneIdKadry int)
	RETURNS TABLE (IdPracownika int)
	AS
	$body$
	SELECT p.Id FROM Schemat.Pracownicy p
		WHERE p.IdKadry = $1
	$body$
language sql;



/* @author Konrad 
 * Funkcja zwracająca Id Urzednika - na potrzeby klienta. */



CREATE OR REPLACE FUNCTION id_gnoma(DanyLogin text)
	returns integer
	AS
	$body$
	SELECT IdPracownika FROM schemat.UrzednicyKontroli WHERE Login = DanyLogin;
	$body$
language sql SECURITY DEFINER; -- udzielenie praw do czynnosci wykonywanych przez funkcję dla wykonujacego (gnoma).



/*
	@author Piotr Kruk
	widok zwracajacy bezrobotnych czyli pracownikow, bez zadnej przydzialu,bez pracy

*/



CREATE OR REPLACE  VIEW bezrobotni AS 
SELECT p.id,p.imie,p.nazwisko,p.idkadry

FROM schemat.pracownicy p
WHERE urlopzdrowotny = FALSE  --chorych nie zaliczam do bezrobotnych :)
	AND p.id NOT IN (SELECT idpracownika FROM schemat.agenci)
	AND p.id NOT IN (SELECT idpracownika FROM schemat.mikolaje)
	AND p.id NOT IN (SELECT idpracownika FROM schemat.urzednicykontroli)
	AND p.id NOT IN (SELECT idkartyzakladowej FROM schemat.pracownicysali)
	AND p.id NOT IN (SELECT idkartyzakladowej FROM schemat.maszynisci)
	AND p.id NOT IN (SELECT administrator FROM schemat.fabryka)
	AND p.id NOT IN (SELECT idpracownika FROM schemat.tragarze)
	;



--------- PRZYKLAD UZYCIA
--select * from bezrobotni;
-- usuniecie WIDOKU
-- drop view bezrobotni;



/*
@author: Piotr Kruk
widok pokazujacy wszystkich szefow
*/



CREATE OR REPLACE VIEW szefowie AS 
	SELECT ID, idkadry AS "KADRA", Imie, nazwisko, UrlopZdrowotny, KoniecUrlopu, Pensja
	FROM schemat.pracownicy
	WHERE ID IN (select DISTINCT idszefa FROM schemat.pracownicy)
	ORDER BY ID
;



--- Przyklad uzycia
--select * from szefowie;
/*
@author : Piotr Kruk
widok wyswietlajacy top 10 pensji w fabryce!
*/



CREATE OR REPLACE VIEW topzarobkowy AS
	SELECT id,idkadry,imie,nazwisko,idszefa,Pensja
	FROM schemat.pracownicy
	GROUP BY pensja,id
	ORDER BY pensja DESC limit 10
;



--- Przyklad uzycia :
--select * from topzarobkowy;
--- Przyklad uzycia : 
--drop view topzarobkowy;



/*
@author Piotr Kruk
funkcja wyswietlajaca niegrzeczne dzieci
*/



CREATE OR REPLACE VIEW rozga_dla_niegrzecznych AS
	SELECT d.imie,d.nazwisko,o.komentarz
	FROM schemat.dzieci d,schemat.obwieszczeniakontroli o,schemat.listy l,schemat.listyoddzieci ld
	WHERE l.id = ld.idlistu
	AND o.idlistudziecka = ld.idlistu
	AND ld.iddziecka = d.id
	AND o.zaakceptowano = FALSE
;


-- Przyklad uzycia 
--select * from rozga_dla_niegrzecznych;



/*
@author Piotr Kruk
Wyswietla statystyke dla dzieci
*/



CREATE OR REPLACE VIEW statystyka_dzieci AS
	SELECT COUNT(id) AS "ilosc_dzieci", AVG(Wiek) AS "Srednia Wieku", MIN(Wiek) AS "Najmlodsze Dziecko",
	MAX(Wiek) AS "Najstarsze Dziecko", (select COUNT(Plec) FROM schemat.dzieci WHERE Plec = 'M') AS "Ilosc Chlopcow",
	 ((SELECT COUNT(Plec) FROM schemat.dzieci WHERE Plec = 'K')) AS "Ilosc Dziewczynek"
	FROM schemat.Dzieci
;



--- Przyklad uzycia :
-- select * from statystyka_dzieci;



/*
	@author: Piotr Kruk
	widok wyswietlajacy obwieszczeniakontroli cale dane plus dzieci dla ktorych zostalo obwieszczenie zrobione

*/



CREATE OR REPLACE VIEW obwieszczenia_kontroli AS
	SELECT o.nrobwieszczenia,o.wystawiajacy,o.idlistudziecka,o.idraportu, o.komentarz, 
			o.zaakceptowano,o.datawystawienia, d.imie AS "Imie Dziecka", d.nazwisko AS "Nazwisko Dziecka"
	FROM  schemat.obwieszczeniakontroli o, schemat.Dzieci d, schemat.listyoddzieci l
	WHERE o.idlistudziecka = l.idlistu
		AND l.iddziecka = d.id;



--- Przyklad uzycia :
-- select * from obwieszczenia_kontroli;
--- Przyklad usuniecia :
-- drop view obwieszczenia_kontroli;



/*
@author : Piotr Kruk
 statystyka dla kadr, ktora kadra ma przewage w naszym swiecie ! 
*/



CREATE OR REPLACE VIEW Statystyka_kadr AS
	SELECT (SELECT COUNT(IdKadry) FROM schemat.pracownicy WHERE idkadry = 1) AS "Liczebnosc Wywiadu",
	(SELECT COUNT(IdKadry) FROM schemat.pracownicy WHERE idkadry = 2) AS "Liczebnosc Pracownikow Fizycznych",
	(SELECT COUNT(IdKadry) FROM schemat.pracownicy WHERE idkadry = 3 ) AS "Liczebnosc Urzednikow Kontroli",
	(SELECT COUNT(IdKadry) FROM schemat.pracownicy WHERE idkadry = 4) AS "Liczebnosc Maszynistow",
	(SELECT COUNT(IdKadry) FROM schemat.pracownicy WHERE Idkadry = 5) AS "Liczebnosc Sekretarzy",
	(SELECT COUNT(IdKadry) FROM schemat.pracownicy WHERE Idkadry = 6) AS "Liczebnosc Administacji",
	(SELECT COUNT(IdKadry) FROM schemat.pracownicy WHERE IdKadry = 7) AS "Liczebnosc Mikolajow"
;



--- Przyklad Uzycia
-- select * from statystyka_kadr;



/*
 	Widok Podsumowanie roku. Wyswietla :
		ilosc dzieci, ilosc listow od dzieci, ilosc grzeczny i niegrzecznych dzieci, wyprodukowana ilosc zabawek, 
			ilosc zatrudnionych pracownikow, ilosc pracownikow na urlopie i tych zdrowych i koszt pensji dla wszystkich pracownikow
	@author: Piotr Kruk
 
 */



CREATE OR REPLACE VIEW podsumowanie AS
	SELECT count (d.id) AS "Ilosc Dzieci", count (l.idlistu) AS "Ilosc listow od dzieci",
	(SELECT COUNT(zaakceptowano) AS "ilosc grzecznych dzieci" FROM schemat.obwieszczeniakontroli 
		WHERE zaakceptowano = TRUE),
	(SELECT COUNT (zaakceptowano) AS "ilosc niegrzecznych dzieci" FROM schemat.obwieszczeniakontroli
		WHERE zaakceptowano = FALSE),
	(SELECT SUM (ilosczamowien) AS "Wyprodukowana liczba zabawek" FROM schemat.Fabryka
		WHERE ilosczamowien IS NOT NULL),
	(SELECT COUNT (id)  AS "Ilosc Zatrudnionych Pracownikow" FROM schemat.pracownicy ),
	(SELECT COUNT (id) AS "Ilosc Pracownikow na Urlopie" FROM schemat.pracownicy 
		WHERE urlopzdrowotny = TRUE),
	(SELECT COUNT (id) AS "ilosc Pracownikow Wolnych(bez urlopu)" FROM schemat. pracownicy
		WHERE urlopzdrowotny = FALSE),
	(SELECT SUM (Pensja) AS "Koszt Wyplat Dla Pracownikow" FROM schemat.pracownicy)
		FROM schemat.Dzieci d, schemat.ListyodDzieci l
	;



--- Przyklad UZYCIA
-- select * from podsumowanie;
--- Przyklad usuniecia
-- drop view podsumowanie



/*
FUNKCJA WYSWIETLA imie i nazwisko kontrolera, date rozpatrzenia listu, czy zaakceptowano list, Imie i nazwisko DZIECKA, wiek, 
	czy zostały rozdane prezenty i państwo z którego dziecko pochodzi.
@param przyjmuje IMIE NAZWISKO I KRAJ POCHODZENIA DZIECKA
*/

CREATE OR REPLACE function widok_dziecka(parm1 text, parm2 text,parm3 text)
	returns table (Imie_Kontrolera text , Nazwisko_Kontrolera text,Data_Wystawienia_Opinii date,
	Zaakceptowano_List boolean, komentarz text,idListu int, Imie_Dziecka text,Nazwisko_Dziecka text,Wiek smallint,Rozdano_Prezenty boolean,Panstwo text
	)
	AS 
	$body$
	SELECT p.imie, p.nazwisko, o.datawystawienia, o.zaakceptowano, o.komentarz,l.idlistu,
	 d.imie , d.nazwisko , d.wiek,r.prezentyrozdane, r.Panstwo
	FROM Schemat.Obwieszczeniakontroli o, Schemat.dzieci d, Schemat.pracownicy p, 
			Schemat.listyoddzieci l, Schemat.Regiony r
		WHERE d.imie = $1 AND d.nazwisko = $2
			AND (o.wystawiajacy = p.id AND l.idlistu = o.idlistudziecka)
			AND d.regionpochodzenia = r.numerregionu
			AND r.panstwo = $3
			AND o.idlistudziecka = l.idlistu
			AND l.iddziecka = d.id;
			
	$body$
language sql SECURITY DEFINER; -- udzielenie praw do czynnosci wykonywanych przez funkcję dla wykonujacego (dziecko).



-- select * from widok_dziecka('Siergiej','Wasilowicz','Russia'); -- WYWOLANIE FUNKCJI 
-- drop function widok_dziecka(parm1 text,parm2 text,parm3 text); -- USUNIECIE FUNKCJI 



/*
* Funkcja wyswietlajaca DLA GNOMA KONTROLERA KTORY PODA SWOJE ID , Wyswietla, przyporzadkowane mu dzieci,
	ich imie,nazwisko,wiek, idlistu i idraport(to sa obiekty fizyczne dzieki temu wie, po jakie listy siegnac),
	i pseudonim agenta, ktory sledzil dziecko 
* @author Piotr Kruk
*/



CREATE OR REPLACE FUNCTION widok_gnoma(parm3 int)
	returns table (id_dziecka int,imie_dziecka text,nazwisko_dziecka text,wiek smallint,id_listu int,
				kryptonim_agenta_wystawiajacego_raport text, id_raportu int)
	AS
	$BODY$
	SELECT DISTINCT d.id, d.imie, d.nazwisko, d.wiek, ld.idlistu,bond.kryptonim,r.idlistu
	FROM schemat.pracownicy p, schemat.dzieci d, schemat.obwieszczeniakontroli o,
		schemat.listyoddzieci ld, schemat.RaportyObserwacji r, schemat.Agenci bond
	WHERE p.id = $1
	AND o.wystawiajacy = p.id
	AND o.idlistudziecka = ld.idlistu
	AND d.id = ld.iddziecka
	AND o.idraportu = r.idlistu
	AND bond.idpracownika = r.idagenta
	ORDER BY d.id
	$BODY$
language sql SECURITY DEFINER; -- udzielenie praw do czynnosci wykonywanych przez funkcję dla wykonujacego (gnoma).



---- Przyklad uzycia
-- select * from widok_gnoma(80);
--- Usuniecie 
-- drop function widok_gnoma(parm3 int);



/*
@author Piotr Kruk
-- Widok pokazujacy stan fabryki, przyklad zastosowania, gnom kontroler pobiera sobie widok z fabryki
-- wybiera do ktorego dzialu ile ma pojsc zamowien i wysyla to do funkcji zamowienia_fabryki;

*/



CREATE OR REPLACE VIEW widok_fabryki AS 
	SELECT f.numerwydzialu AS "ID WYDZIALU", p.imie AS "Imie Administratora",
		p.nazwisko AS "Nazwisko Administratora",f.nazwawydzialu AS "Nazwa Wydzialu",
		f.ilosczamowien AS "Ilosc zamowien"
	FROM schemat.Fabryka f, schemat.Pracownicy p
	WHERE  p.id = f.administrator
;



--- Przyklad Uzycia 
--select * from widok_fabryki;
--- Usniecie
-- drop view widok_fabryki;



/**
 * @author	Konrad Talik
 *			Widok CTE prezentujący poziom pracownika w hierarchii.
 */



CREATE OR REPLACE VIEW Poziom_w_hierarchii AS (

	WITH RECURSIVE poziom_w_hierarchii_cte AS (

		-- Kotwica
		SELECT Imie, Nazwisko, Id, IdSzefa, 0 as Poziom
		FROM schemat.Pracownicy WHERE Id = IdSzefa

		UNION ALL

		-- Rekursja
		SELECT p.Imie, p.Nazwisko, p.Id, p.IdSzefa, Poziom + 1 as Poziom
		FROM schemat.Pracownicy p
		JOIN poziom_w_hierarchii_cte h 
		ON (p.IdSzefa = h.Id)
		AND p.Id != p.IdSzefa

	)
	SELECT * FROM poziom_w_hierarchii_cte
	ORDER BY Poziom
);



-- Sposób użycia:
--	SELECT * FROM Poziom_w_hierarchii;



/**
 * @author	Konrad Talik
 *			Na podstawie powyższego widoku CTE wyodrębniamy dyrektora.
 *			Dyrektor nam się może zmieniać :)
 */



CREATE OR REPLACE VIEW Dyrektor AS 
	SELECT Imie, Nazwisko, Id FROM Poziom_w_hierarchii
	WHERE Poziom = 0;



-- Sposób użycia:
--	SELECT * FROM Dyrektor;



/**
 * @author	Konrad Talik
 *			Na podstawie powyższego widoku CTE wyodrębniamy kierowników,
 *			czyli bezpośrednich podwładnych dyrektora.
 */
 CREATE OR REPLACE VIEW Kierownicy AS
 	SELECT Imie, Nazwisko, Id FROM Poziom_w_hierarchii
	WHERE Poziom = 1;

-- Sposób użycia:
--	SELECT * FROM Kierownicy;



/**
 * @author	Konrad Talik
 *			Widok CTE prezentujący pracowników nie majacych podwładnych.
 *			Są to "liście" w drzewie hierarchii.
 *			Najniższe warstwy kadry **różnego** poziomu.
 *
 */



CREATE OR REPLACE VIEW Szeregowi AS (

	SELECT Imie, Nazwisko, Id, IdSzefa, Poziom FROM Poziom_w_hierarchii p
		WHERE Poziom != 0 -- Wyrzucamy kotwicę - dyrektorów.
		AND p.Id NOT IN (
			SELECT IdSzefa FROM schemat.Pracownicy )
	ORDER BY Poziom
);



-- Sposób użycia:
--	SELECT * FROM Szeregowi;

/*
@author Piotr Kruk
Podstawowe widoki, sortujace 
*/

CREATE OR REPLACE VIEW SIMPLE_VIEW_REGIONY AS (
	SELECT numerregionu,idmikolaja,panstwo,prezentyrozdane,liczbadzieci
	FROM  schemat.regiony
	ORDER BY  numerregionu
);

CREATE OR REPLACE VIEW SIMPLE_VIEW_PRACOWNICY AS (
	SELECT id,idkadry,idnacji,imie,nazwisko,adres,urlopzdrowotny,koniecurlopu,idszefa,pensja
	FROM schemat.pracownicy
	ORDER BY id
);

CREATE OR REPLACE VIEW SIMPLE_VIEW_DZIECI AS (
	SELECT id,regionpochodzenia,imie,nazwisko,numerkartoteki,wiek,plec
	FROM schemat.dzieci
	ORDER BY  id
);

CREATE OR REPLACE VIEW SIMPLE_VIEW_URZEDNICY_KONTROLI AS ( 
	SELECT idpracownika,numerszafki,numerbiuraszafki,numerbiuragabinetu,wytrzymalosc,login
	FROM schemat.urzednicykontroli
	ORDER BY idpracownika
);

CREATE OR REPLACE VIEW SIMPLE_VIEW_FABRYKA AS  (
	SELECT numerwydzialu,administrator,nazwawydzialu,ilosczamowien,zrealizowano
	FROM schemat.fabryka
	ORDER BY numerwydzialu
);
