
---- nacje
INSERT INTO schemat.Nacje VALUES (1, 'Elf', 80, 130, 190);
INSERT INTO schemat.Nacje VALUES (2, 'Krasnolud', 100, 160, 120);
INSERT INTO schemat.Nacje VALUES (3, 'Gnomy', 130, 140, 100);
INSERT INTO schemat.Nacje VALUES (4, 'Skrzat', 140, 200, 30);
INSERT INTO schemat.Nacje VALUES (5, 'Goblin', 160, 60, 115);
INSERT INTO schemat.Nacje VALUES (6, 'Ogry', 30,100,100);
INSERT INTO schemat.Nacje VALUES (7, 'Ludzie', 180,180,180);
--- KADRY

INSERT INTO schemat.Kadry VALUES (1, 4, 'Wywiad', '5000');
INSERT INTO schemat.Kadry VALUES (2, 1, 'Pracownicy sal', '1200');
INSERT INTO schemat.Kadry VALUES (3, 3, 'Urzędnicy kontroli', '3000');
INSERT INTO schemat.Kadry VALUES (4, 6, 'Maszynisci', '2000');
INSERT INTO schemat.Kadry VALUES (5, 3, 'Sekretarze', '1500');
INSERT INTO schemat.Kadry VALUES (6, 5, 'Administracja', '4000');
INSERT INTO schemat.Kadry VALUES (7, 7, 'Mikolaje', '0');
INSERT INTO schemat.Kadry VALUES (8, 2, 'Tragarze', '1300');

--- Pracownicy
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '7', 7, 'Karol', 'Nowak', 'Poland/Cracow','false', NULL, '1','30000'); -- SZEF WSZYSTKICH SZEFOW BOSS MIKOLAJOW !
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Gwarek', 'Wee', 'Russia, Moscow', 'false', NULL, '1', '1282');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Tracy', 'Winter', 'Russia, Kijow', 'false', NULL, '2', '1523');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Zgredek', 'Holythree', 'Laponia, Pingwincity', 'false', NULL, '1', '1313');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Grzmot', 'Alko', 'Japan, Tokio', 'false', NULL, '1', '2120');--5
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Tracy', 'Xen', 'Spain, Corrida', 'false', NULL, '1', '1949');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Taylor', 'Violet', 'Russia, Kijow', 'false', NULL, '1', '2132');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Adrienne', 'Dawidson', 'Laponia, Lapland', 'false', NULL, '1', '2002');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Abigail', 'Alya', 'England, Hogwart', 'false', NULL, '1', '1807');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Rudi', 'Nowak', 'Poland, Warsaw', 'false', NULL, '1', '2415');--10
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 6, 'Alanna', 'Aindeldiel', 'Africa, Kongo', 'false', NULL, '1', '1333');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 6, 'Abigail', 'Eruanna', 'Laponia, Lodowiec', 'false', NULL, '12', '2175'); -- ZASTĘPCA DYREKTORA
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 6, 'Grzmot', 'Winter', 'Laponia, Lapland', 'false', NULL, '12', '1339');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 6, 'Wasyl', 'Tor', 'Africa, Kongo', 'true', '2012-09-21', '11', '1487');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 6, 'Yp', 'Alko', 'Linux, Terminal', 'false', NULL, '11', '1979');--15
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Wyrmir', 'Man', 'Laponia, Lapland', 'false', NULL, '12', '1675');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Alanna', 'Wiliczenko', 'Szwecja, Lodowiec', 'false', NULL, '1', '1303');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Legolas', 'Wiliczenko', 'England, Moria', 'true', '2011-05-24', '17', '1826');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Gimli', 'Aranel', 'Russia, Moscow', 'false', NULL, '17', '2230');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Wyrmir', 'Wee', 'Russia, Moscow', 'false', NULL, '17', '2123');--20
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Yp', 'Wedrowiec', 'Spain, Corrida', 'false', NULL, '20', '2341');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Wasyl', 'Wee', 'Polska, Cracow', 'true', '2012-02-06', '20', '1910');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Wasyl', 'Dawidson', 'Japan, Tokio', 'false', NULL, '20', '2070');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'James', 'Alko', 'Laponia, Lapland', 'false', NULL, '20', '2439');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Mily', 'Xen', 'Laponia, Lapland', 'false', NULL, '20', '1374');--25
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Gimli', 'Tor', 'Russia, Moscow', 'false', NULL, '20', '2241');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Miroslawa', 'Aindeldiel', 'England, Moria', 'false', NULL, '20', '2166');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Tilda', 'Alko', 'Poland, Opole', 'false', NULL, '20', '1567');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Thy', 'Wedrowiec', 'Linux, Terminal', 'true', '2011-10-29', '20', '1610');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Taylor', 'Alko', 'England, Hogwart', 'false', NULL, '22', '2267');--30
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Zmirlacz', 'Eruanna', 'Germany, Berlin', 'false', NULL, '22', '1929');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Mily', 'Aindeldiel', 'Laponia, Lapland', 'false', NULL, '23', '2221');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Aragorn', 'Bongo', 'Polska, Kielce', 'false', NULL, '20', '13');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Zmirlacz', 'Wiliczenko', 'Japan, Tokio', 'false', NULL, '24', '1497');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Abigail', 'Wee', 'England, Hogwart', 'false', NULL, '24', '1745');--35
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Rodwen', 'Aranel', 'Laponia, Lapland', 'false', NULL, '25', '1681');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Wyrmir', 'Beard', 'Laponia, Lapland', 'false', NULL, '20', '1388');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Bongo', 'Wong', 'Polska, Cracow', 'false', NULL, '20', '1969');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Tracy', 'Bongo', 'Laponia, Pingwincity', 'false', NULL, '20', '2030');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Bongo', 'Gburek', 'Japan, Tokio', 'false', NULL, '23', '2216');--40
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Gwarek', 'Petersen', 'Laponia, Lapland', 'false', NULL, '20', '1481');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Thy', 'Szalony', 'England, London', 'false', NULL, '20','1592');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Miroslawa', 'Wong', 'Spain, Corrida', 'false', NULL, '20', '1483');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Wasyl', 'Eruanna', 'Russia, Kijow', 'false', NULL, '20', '1544');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Tatiana', 'Szalony', 'Czechy, Praga', 'false', NULL, '20', '1740');--45
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Yp', 'Aranel', 'Laponia, Pingwincity', 'false', NULL, '20', '1555');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Gwarek', 'Ypman', 'Laponia, Lapland', 'false', NULL, '20', '1231');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Ethiriel', 'Wedrowiec', 'Czechy, Praga', 'false', NULL, '20', '1824');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Gwarek', 'Xen', 'Czechy, Praga', 'true', '2012-03-27', '20', '1344');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 4, 'Miroslawa', 'Yaviel', 'Laponia, Lapland', 'false', NULL, '1', '1952');--50
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Wyrmir', 'Yaviel', 'Laponia, Lapland', 'false', NULL, '1', '2093');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Mily', 'Alko', 'Linux, Terminal', 'false', NULL, '30', '1472');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Borom', 'Alko', 'Japan, Tokio', 'false', NULL, '30', '1350');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Ruby', 'Winter', 'Laponia, Lodowiec', 'false', NULL, '30', '1786');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Thy', 'Tor', 'Polska, Kielce', 'false', NULL, '30', '2253');--55
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Wasyl', 'Holythree', 'Poland, Warsaw', 'false', NULL, '20', '2158');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Aragorn', 'Holythree', 'England, Moria', 'false', NULL, '22', '1553');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Earmaiviel', 'Holythree', 'Polska, Cracow', 'false', NULL, '1', '2153');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Abigail', 'Wong', 'Laponia, Pingwincity', 'false', NULL, '1', '2091');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Bruce', 'Ypman', 'Laponia, Pingwincity', 'true', '2010-03-02', '1', '2495');--60
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Bruce', 'Trinity', 'Szwecja, Laply', 'false', NULL, '1', '2095');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Thy', 'Yaviel', 'Linux, Terminal', 'false', NULL, '1', '2267');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Wasyl', 'Beard', 'Russia, Moscow', 'false', NULL, '1', '1844');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'James', 'Beard', 'Russia, Moscow', 'false', NULL, '1', '1765');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Gwarek', 'Xen', 'Linux, Terminal', 'false', NULL, '1', '2104');--65
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Tracy', 'Tor', 'Russia, Kijow', 'false', NULL, '1', '1989');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Miroslawa', 'Wedrowiec', 'Laponia, Pingwincity', 'false', NULL, '1', '1742');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Thy', 'Man', 'Czechy, Praga', 'false', NULL, '1', '1725');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Borom', 'Tor', 'Germany, Berlin', 'false', NULL, '1', '1573');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Mily', 'Gburek', 'Russia, Kijow', 'false', NULL, '1', '1510');--70
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Mily', 'Ypman', 'Polska, Kielce', 'false', NULL, '1', '1310');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Yp', 'Gburek', 'Polska, Kielce', 'false', NULL, '1', '2027');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Grzmot', 'Alko', 'Czechy, Praga', 'true', '2010-09-29', '1', '1740');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bruce', 'Bongo', 'Francja, Paris', 'false', NULL, '1', '1617');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Ruby', 'Xen', 'England, Hogwart', 'false', NULL, '1', '1352');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Mily', 'Yaviel', 'Russia, Kijow', 'true', '2010-10-26', '1', '2051');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Tatiana', 'Ypman', 'Czechy, Praga', 'false', NULL, '1', '1240');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Wasyl', 'Maerwen', 'Japan, Tokio', 'true', '2011-04-09', '1', '1996');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Ethiriel', 'Ypman', 'Laponia, Lodowiec', 'false', NULL, '1', '1401');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Ruby', 'Trinity', 'Laponia, Pingwincity', 'false', NULL, '1', '1392');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Mily', 'Vanya', 'Laponia, Pingwincity', 'false', NULL, '1', '1874');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Rodwen', 'Trinity', 'Russia, Kijow', 'false', NULL, '1', '1244');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Legolas', 'Trinity', 'Francja, Paris', 'false', NULL, '40', '2468');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Bruce', 'Vanya', 'Laponia, Lapland', 'false', NULL, '40', '1836');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Gimli', 'Tor', 'England, London', 'false', NULL, '1', '1503');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Bongo', 'Wong', 'Laponia, Lapland', 'true', '2011-11-14', '1', '1581');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Wyrmir', 'Trinity', 'Japan, Tokio', 'false', NULL, '1', '1939');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Bongo', 'Petersen', 'Laponia, Lapland', 'true', '2012-06-01', '1', '2498');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Rudi', 'Alya', 'Russia, Kijow', 'false', NULL, '1', '2108');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Zmirlacz', 'Trinity', 'Russia, Kijow', 'true', '2010-03-14', '1', '1352');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Gimli', 'Vanya', 'England, London', 'false', NULL, '1', '2357');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Lily', 'Ypman', 'Laponia, Lapland', 'false', NULL, '40', '2127');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Tatiana', 'Dawidson', 'Russia, Kijow', 'true', '2012-12-13', '1', '2415');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Wasyl', 'Yaviel', 'England, Moria', 'true', '2010-05-11', '40', '1389');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Aragorn', 'Vanya', 'Laponia, Lapland', 'true', '2010-06-12', '1', '1739');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bongo', 'Bongo', 'Laponia, Lapland', 'false', NULL, '40', '2363');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Mily', 'Petersen', 'Japan, Tokio', 'false', NULL, '1', '1929');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Aragorn', 'Wee', 'Szwecja, Lodowiec', 'false', NULL, '1', '1381');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Gwarek', 'Holythree', 'England, Hogwart', 'true', '2012-01-11', '1', '2290');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Wasyl', 'Tail', 'England, London', 'false', NULL, '1', '2040');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bongo', 'Alya', 'Laponia, Lapland', 'false', NULL, '1', '2337');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Zgredek', 'Yaviel', 'Czechy, Praga', 'false', NULL, '1', '1889');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Abigail', 'Bongo', 'Poland, Warsaw', 'false', NULL, '1', '1642');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Rodwen', 'Eruanna', 'Laponia, Pingwincity', 'false', NULL, '1', '2001');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Zmirlacz', 'Tail', 'Laponia, Lodowiec', 'false', NULL, '1', '2193');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Rudi', 'Ypman', 'Polska, Cracow', 'false', NULL, '1', '1782');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Bongo', 'Man', 'Russia, Kijow', 'true', '2011-07-03', '1', '1216');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Wasyl', 'Winter', 'Laponia, Lodowiec', 'false', NULL, '1', '1687');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Wyrmir', 'Dawidson', 'Laponia, Lodowiec', 'false', NULL, '1', '2111');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Wasyl', 'Dawidson', 'Japan, Tokio', 'false', NULL, '1', '1317');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Yp', 'Holythree', 'Laponia, Lapland', 'false', NULL, '1', '1629');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Zmirlacz', 'Wedrowiec', 'Spain, Corrida', 'false', NULL, '1', '1452');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Borom', 'Zgred', 'Russia, Moscow', 'false', NULL, '1', '2146');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Ruby', 'Dawidson', 'Czechy, Praga', 'false', NULL, '1', '2024');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Tatiana', 'Tor', 'Japan, Tokio', 'false', NULL, '1', '2467');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Ruby', 'Dawidson', 'Russia, Moscow', 'false', NULL, '1', '1365');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Mily', 'Dawidson', 'Czechy, Praga', 'false', NULL, '1', '1834');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Bruce', 'Violet', 'Laponia, Lapland', 'false', NULL, '1', '1452');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Legolas', 'Eruanna', 'Czechy, Praga', 'false', NULL, '1', '1909');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Borom', 'Zgred', 'Laponia, Pingwincity', 'true', '2010-04-17', '1', '1904');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Bongo', 'Wiliczenko', 'Czechy, Praga', 'false', NULL, '1', '1954');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Legolas', 'Wedrowiec', 'Francja, Paris', 'false', NULL, '1', '1433');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Mily', 'Wee', 'England, Hogwart', 'false', NULL, '1', '1486');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Tatiana', 'Vanya', 'Laponia, Lapland', 'true', '2010-05-30', '1', '1204');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Earmaiviel', 'Tor', 'Linux, Terminal', 'false', NULL, '1', '2295');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Alanna', 'Eruanna', 'Poland, Warsaw', 'false', NULL, '1', '1526');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Grzmot', 'Vanya', 'Linux, Terminal', 'false', NULL, '1', '1815');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Gwarek', 'Alya', 'Polska, Kielce', 'false', NULL, '1', '1436');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Gimli', 'Bongo', 'England, Hogwart', 'false', NULL, '1', '2057');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Alanna', 'Holythree', 'Francja, Paris', 'false', NULL, '1', '2364');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Earmaiviel', 'Vanya', 'Laponia, Lapland', 'false', NULL, '1', '1229');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Miroslawa', 'Vanya', 'Japan, Tokio', 'false', NULL, '1', '1475');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Grzmot', 'Winter', 'Russia, Moscow', 'false', NULL, '1', '2473');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Taylor', 'Eruanna', 'Polska, Kielce', 'false', NULL, '1', '1400');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Miroslawa', 'Trinity', 'Laponia, Lapland', 'true', '2010-02-25', '1', '2186');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Borom', 'Tail', 'Laponia, Lapland', 'false', NULL, '1', '1977');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Tracy', 'Vanya', 'Laponia, Lapland', 'false', NULL, '1', '1581');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Rodwen', 'Szalony', 'Russia, Moscow', 'false', NULL, '1', '2072');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'James', 'Man', 'England, Hogwart', 'false', NULL, '1', '1734');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Rodwen', 'Tor', 'Linux, Terminal', 'false', NULL, '1', '1574');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Mily', 'Szalony', 'Poland, Opole', 'false', NULL, '1', '2241');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Rodwen', 'Tail', 'England, London', 'false', NULL, '1', '2466');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Grzmot', 'Tor', 'Laponia, Pingwincity', 'false', NULL, '1', '2461');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Legolas', 'Tor', 'Polska, Cracow', 'false', NULL, '1', '1222');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Aragorn', 'Eruanna', 'England, Moria', 'false', NULL, '1', '2194');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Zmirlacz', 'Bongo', 'Poland, Warsaw', 'false', NULL, '1', '1287');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Gwarek', 'Tor', 'Japan, Tokio', 'false', NULL, '1', '1233');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Rudi', 'Alya', 'Laponia, Pingwincity', 'false', NULL, '1', '2054');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Aragorn', 'Zgred', 'Polska, Cracow', 'false', NULL, '1', '1982');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bruce', 'Dawidson', 'Laponia, Lapland', 'false', NULL, '1', '1921');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Lily', 'Tor', 'Africa, Kongo', 'false', NULL, '1', '1985');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Earmaiviel', 'Wedrowiec', 'Laponia, Pingwincity', 'false', NULL, '1', '1452');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Miroslawa', 'Maerwen', 'Szwecja, Lodowiec', 'false', NULL, '152', '1575');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bongo', 'Man', 'England, Moria', 'false', NULL, '152', '1353');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Grzmot', 'Tor', 'Laponia, Lapland', 'true', '2012-01-22', '1', '2414');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Aragorn', 'Beard', 'England, London', 'false', NULL, '151', '1707');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Wyrmir', 'Maerwen', 'Polska, Kielce', 'false', NULL, '152', '2241');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tracy', 'Szalony', 'Poland, Warsaw', 'false', NULL, '153', '2369');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Ethiriel', 'Man', 'Szwecja, Laply', 'false', NULL, '152', '1361');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Borom', 'Wedrowiec', 'Germany, Berlin', 'false', NULL, '153', '2208');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Grzmot', 'Man', 'England, Hogwart', 'false', NULL, '155', '1379');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bruce', 'Yaviel', 'Laponia, Lapland', 'true', '2011-04-21', '155', '1205');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Wyrmir', 'Trinity', 'Polska, Kielce', 'false', NULL, '155', '1297');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tracy', 'Alko', 'Russia, Moscow', 'false', NULL, '153', '2290');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Earmaiviel', 'Xen', 'Linux, Terminal', 'false', NULL, '153', '2436');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tracy', 'Wiliczenko', 'Laponia, Lapland', 'false', NULL, '153', '1241');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Abigail', 'Dawidson', 'Russia, Kijow', 'false', NULL, '153', '1434');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Adrienne', 'Wee', 'Poland, Opole', 'false', NULL, '152', '2067');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Gwarek', 'Ypman', 'Russia, Moscow', 'false', NULL, '150', '1850');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Borom', 'Wedrowiec', 'Russia, Moscow', 'false', NULL, '160', '1712');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Zmirlacz', 'Beard', 'Linux, Terminal', 'false', NULL, '160', '1614');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Ruby', 'Gburek', 'Laponia, Lapland', 'true', '2010-08-22', '160', '1678');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Rudi', 'Tor', 'Laponia, Lapland', 'false', NULL, '160', '1720');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Grzmot', 'Tor', 'Szwecja, Laply', 'false', NULL, '160', '2027');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Grzmot', 'Ypman', 'Japan, Tokio', 'true', '2010-07-11', '160', '2435');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Zgredek', 'Szalony', 'Africa, Kongo', 'false', NULL, '161', '2135');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Yp', 'Dawidson', 'Laponia, Lodowiec', 'false', NULL, '162', '1329');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Gwarek', 'Wee', 'Linux, Terminal', 'true', '2012-03-24', '163', '1949');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Wyrmir', 'Wong', 'Laponia, Lapland', 'false', NULL, '163', '1750');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Aragorn', 'Tail', 'Laponia, Lodowiec', 'false', NULL, '163', '1924');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Alanna', 'Aindeldiel', 'Laponia, Lapland', 'false', NULL, '164', '2181');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Zmirlacz', 'Wee', 'Russia, Kijow', 'false', NULL, '165', '1739');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tilda', 'Xen', 'Poland, Opole', 'false', NULL, '165', '1792');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tilda', 'Xen', 'Polska, Kielce', 'true', '2011-08-01', '166', '1557');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Legolas', 'Gburek', 'Japan, Tokio', 'false', NULL, '166', '2399');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tracy', 'Xen', 'Russia, Kijow', 'false', NULL, '166', '2487');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bruce', 'Wee', 'Germany, Berlin', 'true', '2012-07-11', '167', '1388');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Gwarek', 'Tor', 'Szwecja, Lodowiec', 'false', NULL, '170', '1870');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Zmirlacz', 'Zgred', 'Szwecja, Lodowiec', 'false', NULL, '170', '1553');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Ruby', 'Violet', 'Poland, Opole', 'false', NULL, '170', '1785');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bongo', 'Tail', 'Szwecja, Laply', 'true', '2011-06-20', '170', '1413');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tilda', 'Alya', 'England, Hogwart', 'false', NULL, '170', '2136');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Aragorn', 'Eruanna', 'Africa, Kongo', 'false', NULL, '170', '2346');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Wasyl', 'Winter', 'Spain, Corrida', 'false', NULL, '170', '1491');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Adrienne', 'Holythree', 'Laponia, Lapland', 'false', NULL, '170', '2134');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Aragorn', 'Vanya', 'Laponia, Lodowiec', 'false', NULL, '172', '1359');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Rudi', 'Alya', 'Spain, Corrida', 'false', NULL, '1', '1478');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Earmaiviel', 'Tor', 'Poland, Warsaw', 'false', NULL, '172', '2057');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tilda', 'Ypman', 'Laponia, Lodowiec', 'false', NULL, '173', '1280');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Rudi', 'Bongo', 'Russia, Moscow', 'false', NULL, '173', '2407');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Miroslawa', 'Szalony', 'Szwecja, Lodowiec', 'false', NULL, '173', '1468');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Adrienne', 'Zgred', 'Laponia, Pingwincity', 'false', NULL, '173', '2432');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Abigail', 'Vanya', 'Laponia, Pingwincity', 'false', NULL, '173', '2326');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tatiana', 'Wedrowiec', 'Laponia, Lapland', 'true', '2011-05-10', '163', '2314');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Earmaiviel', 'Tor', 'Spain, Corrida', 'false', NULL, '165', '1251');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Borom', 'Maerwen', 'Szwecja, Lodowiec', 'false', NULL, '173', '2317');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Thy', 'Maerwen', 'Laponia, Lodowiec', 'false', NULL, '1', '1799');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Wasyl', 'Dawidson', 'Russia, Moscow', 'true', '2011-08-12', '174', '2421');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Adrienne', 'Bongo', 'Laponia, Pingwincity', 'false', NULL, '176', '1411');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Wyrmir', 'Vanya', 'Laponia, Lapland', 'false', NULL, '180', '1354');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tilda', 'Ypman', 'Russia, Kijow', 'true', '2011-03-09', '180', '1330');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Ruby', 'Trinity', 'Japan, Tokio', 'true', '2011-06-01', '180', '1323');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Gwarek', 'Bongo', 'Laponia, Pingwincity', 'false', NULL, '180', '2475');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bruce', 'Yaviel', 'Linux, Terminal', 'false', NULL, '180', '1938');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Legolas', 'Ypman', 'Laponia, Lapland', 'false', NULL, '180', '1239');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tracy', 'Eruanna', 'Laponia, Pingwincity', 'false', NULL, '180', '2461');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Legolas', 'Vanya', 'England, London', 'false', NULL, '180', '2259');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Ruby', 'Wee', 'Linux, Terminal', 'false', NULL, '180', '1618');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Adrienne', 'Yaviel', 'Germany, Berlin', 'false', NULL, '180', '1377');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Yp', 'Violet', 'Russia, Kijow', 'false', NULL, '180', '2395');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Miroslawa', 'Holythree', 'Szwecja, Lodowiec', 'false', NULL, '1', '2315');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Zgredek', 'Aranel', 'England, Hogwart', 'false', NULL, '1', '2389');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Tracy', 'Alko', 'Laponia, Lapland', 'false', NULL, '1', '2029');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Wyrmir', 'Violet', 'Japan, Tokio', 'false', NULL, '182', '1594');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Earmaiviel', 'Trinity', 'Germany, Berlin', 'false', NULL, '182', '1917');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Miroslawa', 'Maerwen', 'Laponia, Lapland', 'false', NULL, '182', '2440');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Bruce', 'Beard', 'Laponia, Lapland', 'true', '2010-06-12', '182', '1506');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Mily', 'Man', 'Laponia, Pingwincity', 'false', NULL, '183', '2040');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Borom', 'Violet', 'Laponia, Lapland', 'false', NULL, '184', '1677');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Borom', 'Szalony', 'Laponia, Pingwincity', 'false', NULL, '185', '1910');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Miroslawa', 'Eruanna', 'Polska, Kielce', 'false', NULL, '185', '2203');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Earmaiviel', 'Yaviel', 'Linux, Terminal', 'false', NULL, '185', '2172');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Ruby', 'Beard', 'Russia, Kijow', 'true', '2012-09-19', '185', '1352');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Wyrmir', 'Aranel', 'Russia, Moscow', 'false', NULL, '185', '2303');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Ethiriel', 'Tail', 'Laponia, Lapland', 'true', '2010-07-06', '185', '2307');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Mily', 'Dawidson', 'Linux, Terminal', 'false', NULL, '185', '2155');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Rodwen', 'Vanya', 'Szwecja, Laply', 'false', NULL, '185', '2255');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Wyrmir', 'Tail', 'Spain, Corrida', 'false', NULL, '185', '2226');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Lily', 'Trinity', 'Laponia, Pingwincity', 'false', NULL, '185', '1201');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Earmaiviel', 'Wong', 'England, London', 'false', NULL, '185', '1461');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Zmirlacz', 'Szalony', 'Laponia, Lapland', 'false', NULL, '185', '1867');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Zmirlacz', 'Violet', 'Russia, Kijow', 'false', NULL, '1', '2067');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Gimli', 'Xen', 'Laponia, Lapland', 'false', NULL, '100', '1203');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Alanna', 'Aindeldiel', 'Russia, Kijow', 'false', NULL, '100', '2372');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Rudi', 'Vanya', 'England, Moria', 'false', NULL, '100', '1949');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Gwarek', 'Tail', 'England, Hogwart', 'false', NULL, '100', '1670');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Rodwen', 'Man', 'Africa, Kongo', 'false', NULL, '100', '1970');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Lily', 'Wedrowiec', 'Laponia, Lodowiec', 'false', NULL, '100', '1978');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Tracy', 'Aindeldiel', 'Poland, Opole', 'false', NULL, '100', '2142');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Thy', 'Holythree', 'Laponia, Lapland', 'false', NULL, '100', '1622');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Grzmot', 'Nowak', 'Francja, Paris', 'true', '2010-09-27', '100', '1293');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'James', 'Bongo', 'Laponia, Lapland', 'false', NULL, '100', '2425');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bongo', 'Wong', 'Laponia, Pingwincity', 'false', NULL, '100', '2345');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Thy', 'Zgred', 'Szwecja, Lodowiec', 'false', NULL, '100', '1595');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Bongo', 'Xen', 'Japan, Tokio', 'true', '2010-05-09', '100', '1805');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Zmirlacz', 'Maerwen', 'Russia, Moscow', 'false', NULL, '100', '2479');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Abigail', 'Yaviel', 'England, Moria', 'false', NULL, '100', '1231');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Wyrmir', 'Nowak', 'Poland, Opole', 'false', NULL, '100', '1479');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Abigail', 'Aranel', 'Poland, Warsaw', 'false', NULL, '100', '1406');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Taylor', 'Winter', 'Francja, Paris', 'false', NULL, '150', '1793');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Ruby', 'Wedrowiec', 'Africa, Kongo', 'false', NULL, '150', '1802');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Tatiana', 'Szalony', 'Francja, Paris', 'false', NULL, '150', '1874');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Tilda', 'Wee', 'Poland, Warsaw', 'false', NULL, '160', '1354');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Yp', 'Szalony', 'Linux, Terminal', 'false', NULL, '1', '1479');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'James', 'Dawidson', 'Polska, Kielce', 'false', NULL, '1', '2364');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Zmirlacz', 'Aranel', 'Polska, Cracow', 'false', NULL, '1', '1914');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'James', 'Szalony', 'Szwecja, Lodowiec', 'false', NULL, '1', '1672');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Zmirlacz', 'Winter', 'Czechy, Praga', 'false', NULL, '1', '1946');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Gwarek', 'Wiliczenko', 'Szwecja, Laply', 'false', NULL, '1', '1706');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Wasyl', 'Wiliczenko', 'Czechy, Praga', 'false', NULL, '1', '2222');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Gwarek', 'Maerwen', 'Poland, Warsaw', 'false', NULL, '1', '2059');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Bruce', 'Petersen', 'Czechy, Praga', 'false', NULL, '1', '1612');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Bruce', 'Alya', 'Czechy, Praga', 'false', NULL, '1', '2403');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Miroslawa', 'Yaviel', 'Africa, Kongo', 'false', NULL, '1', '1305');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Gimli', 'Nowak', 'Linux, Terminal', 'false', NULL, '1', '1293');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Gwarek', 'Petersen', 'Russia, Kijow', 'false', NULL, '1', '1681');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Gwarek', 'Winter', 'England, London', 'false', NULL, '1', '2348');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Ruby', 'Yaviel', 'Szwecja, Lodowiec', 'false', NULL, '1', '1591');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Miroslawa', 'Dawidson', 'Polska, Kielce', 'false', NULL, '1', '1380');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Borom', 'Man', 'Russia, Kijow', 'true', '2010-09-17', '1', '1610');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Adrienne', 'Aindeldiel', 'Francja, Paris', 'false', NULL, '1', '1551');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Grzmot', 'Szalony', 'England, London', 'false', NULL, '1', '2031');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Gwarek', 'Aranel', 'Szwecja, Laply', 'false', NULL, '1', '2434');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Gwarek', 'Tor', 'Russia, Moscow', 'false', NULL, '1', '2212');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Zgredek', 'Aranel', 'Laponia, Pingwincity', 'false', NULL, '1', '1549');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Mily', 'Aindeldiel', 'Czechy, Praga', 'false', NULL, '1', '2108');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Wyrmir', 'Alko', 'Laponia, Lodowiec', 'false', NULL, '1', '1212');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '3', 3, 'Rudi', 'Szalony', 'Laponia, Lapland', 'false', NULL, '1', '1548');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Adrienne', 'Szalony', 'Spain, Corrida', 'true', '2012-10-14', '1', '1607');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '4', 6, 'Tatiana', 'Tail', 'Laponia, Lapland', 'true', '2011-04-10', '1', '1557');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '1', 4, 'Zmirlacz', 'Dawidson', 'Szwecja, Laply', 'false', NULL, '1', '2211');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Wasyl', 'Aindeldiel', 'Czechy, Praga', 'false', NULL, '1', '2233');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '5', 3, 'Aragorn', 'Eruanna', 'Szwecja, Lodowiec', 'false', NULL, '1', '1765');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '2', 1, 'Miroslawa', 'Tail', 'Japan, Tokio', 'false', NULL, '1', '1471');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '7', 7, 'Abigail', 'Xen', 'Russia, Kijow', 'false', NULL, '1', '1414');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '7', 7, 'Ruby', 'Holythree', 'Polska, Cracow', 'false', NULL, '1', '1987');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '7', 7, 'Yp', 'Szalony', 'Russia, Kijow', 'false', NULL, '1', '1461');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '7', 7, 'Yp', 'Alya', 'Laponia, Lapland', 'false', NULL, '1', '1905');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '7', 7, 'Gimli', 'Bongo', 'Russia, Moscow', 'false', NULL, '1', '1627');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '7', 7, 'Taylor', 'Wiliczenko', 'Japan, Tokio', 'false', NULL, '1', '2094');
INSERT INTO schemat.Pracownicy VALUES (DEFAULT, '7', 7, 'Adrienne', 'Wee', 'Laponia, Pingwincity', 'false', NULL, '1', '1917');



---- SANIE

INSERT INTO schemat.sanie VALUES (1, 10000,'MOSCOW');
INSERT INTO schemat.sanie VALUES (2, 15000, 'USA');
INSERT INTO schemat.sanie VALUES (3, 8000, 'GREENDLAND');
INSERT INTO schemat.sanie VALUES (4, 5000, 'FRANCE');
INSERT INTO schemat.sanie VALUES (5, 9000, 'POLAND');
INSERT INTO schemat.sanie VALUES (6, 3000, 'JAPAN');
INSERT INTO schemat.sanie VALUES (7, 9000, 'SPAIN');
INSERT INTO schemat.sanie VALUES (8, 7000, 'GERMANY');
INSERT INTO schemat.sanie VALUES (9, 20000, 'DELUXEVIP');

---- mikołajowie
INSERT INTO schemat.Mikolaje VALUES (1, 9, 'Santa Claus'); -- BOSS 
INSERT INTO schemat.Mikolaje VALUES (293, 1, 'Dziadek Mroz');
INSERT INTO schemat.Mikolaje VALUES (294, 2, 'Santa Hosse');
INSERT INTO schemat.Mikolaje VALUES (295, 3, 'Santa USA');
INSERT INTO schemat.Mikolaje VALUES (296, 4, 'Santa Staszek');
INSERT INTO schemat.Mikolaje VALUES (297, 5, 'Santa Chan');
INSERT INTO schemat.Mikolaje VALUES (298, 6, 'Santa Marian');
INSERT INTO schemat.Mikolaje VALUES (299, 7, 'Santa Iwan');
INSERT INTO schemat.Mikolaje VALUES (300, 8, 'Santa Tux');



---- renifery
INSERT INTO schemat.Renifery VALUES (1, 1, 'Rudolf',true);
INSERT INTO schemat.Renifery VALUES (2, 1, 'Hans',true);
INSERT INTO schemat.Renifery VALUES (3, 2, 'Archi',true);
INSERT INTO schemat.Renifery VALUES (4, 2, 'Wzium', true);
INSERT INTO schemat.Renifery VALUES (5, 3, 'Bazin', false);
INSERT INTO schemat.Renifery VALUES (6, 3, 'Grum', false);
INSERT INTO schemat.Renifery VALUES (7, 4, 'Tux',false);
INSERT INTO schemat.Renifery VALUES (8, 4, 'Ping', false);
INSERT INTO schemat.Renifery VALUES (9, 5, 'Chap', false);
INSERT INTO schemat.Renifery VALUES (10, 6, 'Wrum', true);
INSERT INTO schemat.Renifery VALUES (11, 7, 'Sash',false);
INSERT INTO schemat.Renifery VALUES (12, 8, 'Rumcajs',false);
INSERT INTO schemat.Renifery VALUES (13, 7, 'Pimpus', true);
INSERT INTO schemat.Renifery VALUES (14, 9, 'Mustang',false);
INSERT INTO schemat.Renifery VALUES (15, 9, 'Diablo',false);
INSERT INTO schemat.Renifery VALUES (16, 9, 'Ferrari',false);

--KONIEC Pracownicy

-- FABRYKA!!!

INSERT INTO schemat.Fabryka values (1,4,'DZIAL PLUSZOWO GUMOWY',0,0);
INSERT INTO schemat.Fabryka values (2,5,'DZIAL MECHANICZNY',0,0);
INSERT INTO schemat.Fabryka values (3,6,'DZIAL ELEKTRONICZNO-CYFROWY',0,0);
INSERT INTO schemat.Fabryka values (4,7,'DZIAL MAGICZNY',0,0);
INSERT INTO schemat.Fabryka values (5,8,'DZIAL SPOZYWCZY',0,0); 
INSERT INTO schemat.Fabryka values (6,9,'MAGAZYNY',0,0);
INSERT INTO schemat.Fabryka values (7,10,'BIURA',0,0);


------ SALE ----

INSERT INTO schemat.Sale values (1,1,500,500);
INSERT INTO schemat.Sale values (2,1,500,500);
INSERT INTO schemat.Sale values (3,2,600,600);
INSERT INTO schemat.Sale values (4,2,700,700);
INSERT INTO schemat.Sale values (5,1,300,300);
INSERT INTO schemat.Sale values (6,1,300,300);
INSERT INTO schemat.Sale values (7,3,600,600);
INSERT INTO schemat.Sale values (8,4,100,100);
INSERT INTO schemat.Sale values (9,1,200,200);
INSERT INTO schemat.Sale values (10,3,500,500);
INSERT INTO schemat.Sale values (11,3,2000,2000);
INSERT INTO schemat.Sale values (12,4,5000,5000);
INSERT INTO schemat.Sale values (13,1,200,200);
INSERT INTO schemat.Sale values (14,5,400,400);
INSERT INTO schemat.Sale values (15,2,500,500);
INSERT INTO schemat.Sale values (16,6,7000,7000);
INSERT INTO schemat.Sale values (17,6,1100,1100);
INSERT INTO schemat.Sale values (18,6,2000,2000);
INSERT INTO schemat.Sale values (19,6,2000,2000);
INSERT INTO schemat.Sale values (20,6,5000,5000);
INSERT INTO schemat.Sale values (21,6,5000,5000);
INSERT INTO schemat.Sale values (22,6,1000,5000);
INSERT INTO schemat.Sale values (23,7,50,50);
INSERT INTO schemat.Sale values (24,7,100,100);
INSERT INTO schemat.Sale values (25,7,100,100);
INSERT INTO schemat.Sale values (26,7,1000,1000);

------ BHP ------
INSERT into schemat.BHP values (1, 'W razie uklucia nalozyc bandaz',1);
INSERT into schemat.BHP values (2, 'W wypadku straty konczyny,przyszyc i opatrzyc',70);
INSERT into schemat.BHP values (3, 'W wypadku porazenia, wylaczyc prad i opatrzyc', 30);
INSERT into schemat.BHP values (4, 'W wypadku rany magicznej podac eliksir', 10);
INSERT into schemat.BHP values (5, 'Podac srodek przeczyszczajacy', 2);

 

 -- OZNACZENIE BHP 1 - Lalki,pilki 2 - Rzeczy Mechaniczne i motoryczne 3 - Elektronika 4 - RZECZY RARE SPECJALNE MAGICZNE
 -- 5 RZECZY SPOZYWCZE!!!!
 INSERT INTO schemat.SaleProdukcji VALUES (1,'Lalka',1);
 INSERT INTO schemat.SaleProdukcji VALUES (2,'Pilka',1);
 INSERT INTO schemat.SaleProdukcji VALUES (3,'Samochod',2);
 INSERT INTO schemat.SaleProdukcji VALUES (4,'Ciufcia',2);
 INSERT INTO schemat.SaleProdukcji VALUES (5,'Puzzle',1);
 INSERT INTO schemat.SaleProdukcji VALUES (6,'Kredki',1);
 INSERT INTO schemat.SaleProdukcji VALUES (7,'Gra Komputerowa',3);
 INSERT INTO schemat.SaleProdukcji VALUES (8,'Jednorozec',4);
 INSERT INTO schemat.SaleProdukcji VALUES (9,'Misie',1);
 INSERT INTO schemat.SaleProdukcji VALUES (10,'Gitara',1);
 INSERT INTO schemat.SaleProdukcji VALUES (11,'Keyboard',3);
 INSERT INTO schemat.SaleProdukcji VALUES (12,'Godzilla',4);
 INSERT INTO schemat.SaleProdukcji VALUES (13,'Dom dla lalek',1);
 INSERT INTO schemat.SaleProdukcji VALUES (14,'Wor Slodyczy',5);
 INSERT INTO schemat.SaleProdukcji VALUES (15,'Kuchenka',2);


 


 INSERT INTO schemat.PracownicySali values (11 ,1, false, false, true);
 INSERT INTO schemat.PracownicySali values (12, 2, true, false, false);
 INSERT INTO schemat.PracownicySali values (13, 4, true, false, false);
 INSERT into schemat.PracownicySali values (14, 3, false, true, false);
 INSERT into schemat.PracownicySali values (15, 5, true, false, true);
 

----- MAGAZYNY ----
insert into schemat.magazyny values (16,false,10000);
insert into schemat.magazyny values (17,false,5000);
insert into schemat.magazyny values (18,false,5000);
insert into schemat.magazyny values (19,false,5000);
insert into schemat.magazyny values (20,false,2500);
insert into schemat.magazyny values (21,false,7000);
insert into schemat.magazyny values (22,false,7000);


----- END MAGAZYNY -----------


--- MAGAZYNY SALI PRODUKCJI -----
insert into schemat.magazynsaliprodukcji values (1,16,45);
insert into schemat.magazynsaliprodukcji values (2,16,45);
insert into schemat.magazynsaliprodukcji values (3,16,50);
insert into schemat.magazynsaliprodukcji values (4,16,50);
insert into schemat.magazynsaliprodukcji values (5,16,50);
insert into schemat.magazynsaliprodukcji values (6,16,30);
insert into schemat.magazynsaliprodukcji values (7,17,100);
insert into schemat.magazynsaliprodukcji values (8,18,100);
insert into schemat.magazynsaliprodukcji values (9,17,35);
insert into schemat.magazynsaliprodukcji values (10,20,20);
insert into schemat.magazynsaliprodukcji values (11,21,15);
insert into schemat.magazynsaliprodukcji values (12,22,45);
insert into schemat.magazynsaliprodukcji values (13,20,50);
insert into schemat.magazynsaliprodukcji values (14,21,50);
insert into schemat.magazynsaliprodukcji values (15,22,50);

---- END MAGAZYNY SALI PRODUKCJI -----

INSERT INTO schemat.Tragarze VALUES (20, 32, 1, 16, 1 );
INSERT INTO schemat.Tragarze VALUES (21, 57, 1, 17, 2 );
INSERT INTO schemat.Tragarze VALUES (22, 51, 1, 18, 3 );
INSERT INTO schemat.Tragarze VALUES (23, 78, 2, 18, 4 );
INSERT INTO schemat.Tragarze VALUES (24, 35, 2, 18, 5 ); 
INSERT INTO schemat.Tragarze VALUES (25, 25, 2, 19, 6 );
INSERT INTO schemat.Tragarze VALUES (26, 7, 2, 20, 7 );
INSERT INTO schemat.Tragarze VALUES (27, 4, 3, 20, 8 );
INSERT INTO schemat.Tragarze VALUES (28, 97, 3, 21, 9 );
INSERT INTO schemat.Tragarze VALUES (29, 34, 4, 22, 10 );
INSERT INTO schemat.Tragarze VALUES (30, 80, 4, 16, 11 );
INSERT INTO schemat.Tragarze VALUES (31, 7, 5, 17, 12 );
INSERT INTO schemat.Tragarze VALUES (32, 2, 5, 18, 13 );
INSERT INTO schemat.Tragarze VALUES (33, 15, 6, 19, 14 );
INSERT INTO schemat.Tragarze VALUES (34, 15, 6, 20, 15 );
INSERT INTO schemat.Tragarze VALUES (35, 50, 7, 21, 1 );
INSERT INTO schemat.Tragarze VALUES (36, 68, 7, 22, 2 );
INSERT INTO schemat.Tragarze VALUES (37, 85, 1, 22, 3 );
INSERT INTO schemat.Tragarze VALUES (38, 27, 2, 21, 4 );
INSERT INTO schemat.Tragarze VALUES (39, 89, 8, 21, 5 );
INSERT INTO schemat.Tragarze VALUES (40, 72, 8, 21, 6 );
INSERT INTO schemat.Tragarze VALUES (41, 50, 2, 20, 7 );
INSERT INTO schemat.Tragarze VALUES (42, 35, 1, 20, 8 );
INSERT INTO schemat.Tragarze VALUES (43, 97, 1, 19, 9 );
INSERT INTO schemat.Tragarze VALUES (44, 58, 2, 18, 10 );
INSERT INTO schemat.Tragarze VALUES (45, 54, 9, 17, 11 );
INSERT INTO schemat.Tragarze VALUES (46, 99, 9, 16, 12 );
INSERT INTO schemat.Tragarze VALUES (47, 76, 9, 16, 13 );
INSERT INTO schemat.Tragarze VALUES (48, 88, 9, 17, 14 );
INSERT INTO schemat.Tragarze VALUES (49, 13, 9, 18, 15 );


------ END TRAGARZE ---------------------

------- REGIONY ----------------
INSERT INTO schemat.Regiony values (1,1, 'Russia',false,0);
INSERT INTO schemat.Regiony values (2,1, 'Russia',false,0);
INSERT INTO schemat.Regiony values (3,293, 'Russia',false,0);
INSERT INTO schemat.Regiony values (4,293, 'Russia',false,0);
INSERT INTO schemat.Regiony values (5,294, 'Russia',false,0);
INSERT INTO schemat.Regiony values (6,294, 'Poland',false,0);
INSERT INTO schemat.Regiony values (7,295, 'England',false,0);
INSERT INTO schemat.Regiony values (8,295, 'Czech Republic',false,0);
INSERT INTO schemat.Regiony values (9,295, 'France',false,0);
INSERT INTO schemat.Regiony values (10,296, 'Germany',false,0);
INSERT INTO schemat.Regiony values (11,296, 'Germany',false,0);
INSERT INTO schemat.Regiony values (12,297, 'Spain',false,0);
INSERT INTO schemat.Regiony values (13,297, 'Japan',false,0);
INSERT INTO schemat.Regiony values (14,298, 'Sweden',false,0);
INSERT INTO schemat.Regiony values (15,299, 'USA',false,0);
INSERT INTO schemat.Regiony values (16,300, 'Italy',false,0);


---------- DZIECI -----------------------

                      
INSERT INTO schemat.Dzieci values (DEFAULT, 1, 'Siergiej', 'Wasilowicz', 1, 5,'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 2, 'Wasyl', 'Tamburino', 2, 5,'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 9,'Jean', 'Pierre', 3, 7,'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 3, 'Abram', 'Imbramowicz', 4, 9, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 6, 'Jan', 'Nowak',5,7,'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 6, 'Mirmir', 'Zbor',6,6,'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 13, 'Ayoko', 'Siririmi',7,9, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 7, 'James', 'Borman',8,10 ,'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 10,'Hans', 'Zimmerman',9,7, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT,11,'Helmut', 'Sturm',10, 11, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 16,'Sasha', 'White',11, 8, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 4, 'Alexia','Molotow',12, 10, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 3, 'Boguslawa', 'Dwatri',13, 5, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 8, 'Chris', 'Obuszenko',14, 13, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 9,'Julietta', 'Franque',15, 10, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 12, 'Esmeralda', 'von Dia',16, 9, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 12, 'Maria', 'Hosse',17, 7, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 7,'Kate', 'Kde',18, 11, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 14, 'Miriam', 'Weasley',19, 13, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 11, 'Helga', 'Wermut',20, 10, 'K');
INSERT INTO schemat.Dzieci values (DEFAULT, 4, 'Andriej', 'Sacharow',21, 13, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 6, 'Mariusz', 'Nowak',22, 10, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 7, 'Jon', 'Sailor',23, 11, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 13, 'Ip', 'Wan',24, 12, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 13, 'Jackie', 'Wan',25, 12, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 7, 'Willy', 'Trau',26, 5, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 15,'Johnny', 'Nice',27, 8, 'M');
INSERT INTO schemat.Dzieci values (DEFAULT, 15,'Angelina', 'Beauty',28,9, 'K'); 


---- BIURA ----

 insert into schemat.biura values (23, 300, 'Laplandzka 4', 50, true, false);
 insert into schemat.biura values (24, 301, 'Potulnego Barbarzyncy 3', 100, true, true);
 insert into schemat.biura values (25, 302, 'Pingwinia 1', 100, true, true);
 insert into schemat.biura values (26, 303, 'Borsuczej norki 3', 100, true, false);
 
-----------------------
-------------------- ARCHIWA -------------
insert into schemat.archiwa values (23,true);
insert into schemat.archiwa values (24,true);
insert into schemat.archiwa values (25,true);
insert into schemat.archiwa values (26,true);

-------------------------------------------

------------------ SZAFKI -------------

insert into schemat.szafki values (1,23,0);
insert into schemat.szafki values (2,23,1);
insert into schemat.szafki values (3,23,2);
insert into schemat.szafki values (4,24,0);
insert into schemat.szafki values (5,24,1);
insert into schemat.szafki values (6,24,2);
insert into schemat.szafki values (7,24,0);
insert into schemat.szafki values (8,24,0);
insert into schemat.szafki values (9,25,0);
insert into schemat.szafki values (10,25,1);
insert into schemat.szafki values (11,25,2);
insert into schemat.szafki values (12,26,0);
insert into schemat.szafki values (13,26,1);
insert into schemat.szafki values (14,26,2);

--------------------------- URZEDNICY KONTROLI -----------------
insert into schemat.urzednicykontroli values (80 ,1 ,23, 23, 100, 'ruby');
insert into schemat.urzednicykontroli values (81 ,2 ,23, 23, 100, 'mily');
insert into schemat.urzednicykontroli values (82 ,3 ,23, 23, 100, 'rodwen');
insert into schemat.urzednicykontroli values (83 ,4 ,24, 24, 100, 'legolas');
insert into schemat.urzednicykontroli values (84 ,5 ,24, 24, 100, 'bruce');
insert into schemat.urzednicykontroli values (85 ,6 ,24, 25, 100, 'gimli');
insert into schemat.urzednicykontroli values (86 ,7 ,24, 25, 100, 'bongowong');
insert into schemat.urzednicykontroli values (87 ,8 ,24, 24, 100, 'wyrmir');
insert into schemat.urzednicykontroli values (88 ,9 ,25, 26, 100, 'bongopetersen');
insert into schemat.urzednicykontroli values (89 ,10 ,25, 24,100, 'rudi');
insert into schemat.urzednicykontroli values (90 ,11 ,25, 25,100, 'zmirlacz');
insert into schemat.urzednicykontroli values (91 ,12 ,26, 25,100, 'gimlivanya');
insert into schemat.urzednicykontroli values (92 ,13 ,26, 26,100, 'ypman');
insert into schemat.urzednicykontroli values (93 ,14 ,26, 24, 100, 'dawidson');
------------------------------------------------------------------

------- listy -----


insert into schemat.listy values (DEFAULT, '2012-12-12', true);--1
insert into schemat.listy values (DEFAULT, '2012-12-12', true);
insert into schemat.listy values (DEFAULT, '2012-12-12', true);
insert into schemat.listy values (DEFAULT, '2012-12-12', true);
insert into schemat.listy values (DEFAULT, '2012-12-12', true);
insert into schemat.listy values (DEFAULT, '2012-12-12', true);
insert into schemat.listy values (DEFAULT, '2012-12-12', true);
insert into schemat.listy values (DEFAULT, '2012-12-12', true);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);--8
insert into schemat.listy values (DEFAULT, '2012-12-10', true);
insert into schemat.listy values (DEFAULT, '2012-12-10', true);--10
insert into schemat.listy values (DEFAULT, '2012-12-10', true);
insert into schemat.listy values (DEFAULT, '2012-12-10', true);
insert into schemat.listy values (DEFAULT, '2012-12-10', true);
insert into schemat.listy values (DEFAULT, '2012-12-10', true);
insert into schemat.listy values (DEFAULT, '2012-12-10', true);
insert into schemat.listy values (DEFAULT, '2012-12-11', true);
insert into schemat.listy values (DEFAULT, '2012-12-11', true);
insert into schemat.listy values (DEFAULT, '2012-12-11', true);
insert into schemat.listy values (DEFAULT,  '2012-12-11', true);
insert into schemat.listy values (DEFAULT, '2012-12-11', true);--20
insert into schemat.listy values (DEFAULT, '2012-12-10', true);
insert into schemat.listy values (DEFAULT, '2012-12-19', true);
insert into schemat.listy values (DEFAULT, '2012-12-18', true);
insert into schemat.listy values (DEFAULT, '2012-12-17', true);
insert into schemat.listy values (DEFAULT, '2012-12-10', true);
insert into schemat.listy values (DEFAULT, '2012-10-12', true);
insert into schemat.listy values (DEFAULT, '2012-11-12', true);
insert into schemat.listy values (DEFAULT, '2012-12-9', true);
insert into schemat.listy values (DEFAULT, '2012-12-11', true);
insert into schemat.listy values (DEFAULT, '2012-12-12', false);--30
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);--40
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);--50
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);
insert into schemat.listy values (DEFAULT, '2012-12-10', false);--60


-----------------------------------------------------END LISTY-----------------
insert into schemat.raportyobserwacji values (31,150,'2012-12-20');
insert into schemat.raportyobserwacji values (32,151,'2012-12-20');
insert into schemat.raportyobserwacji values (33,152,'2012-12-20');
insert into schemat.raportyobserwacji values (34,153,'2012-12-20');
insert into schemat.raportyobserwacji values (35,154,'2012-12-20');
insert into schemat.raportyobserwacji values (36,155,'2012-12-20');
insert into schemat.raportyobserwacji values (37,156,'2012-12-20');
insert into schemat.raportyobserwacji values (38,157,'2012-12-20');
insert into schemat.raportyobserwacji values (39,158,'2012-12-20');
insert into schemat.raportyobserwacji values (40,159,'2012-12-20');
insert into schemat.raportyobserwacji values (41,160,'2012-12-20');
insert into schemat.raportyobserwacji values (42,161,'2012-12-20');
insert into schemat.raportyobserwacji values (43,162,'2012-12-20');
insert into schemat.raportyobserwacji values (44,163,'2012-12-20');
insert into schemat.raportyobserwacji values (45,164,'2012-12-20');
insert into schemat.raportyobserwacji values (46,165,'2012-12-20');
insert into schemat.raportyobserwacji values (47,166,'2012-12-20');
insert into schemat.raportyobserwacji values (48,167,'2012-12-20');
insert into schemat.raportyobserwacji values (49,168,'2012-12-20');
insert into schemat.raportyobserwacji values (50,169,'2012-12-20');
insert into schemat.raportyobserwacji values (51,170,'2012-12-20');
insert into schemat.raportyobserwacji values (52,171,'2012-12-20');
insert into schemat.raportyobserwacji values (53,172,'2012-12-20');
insert into schemat.raportyobserwacji values (54,173,'2012-12-20');
insert into schemat.raportyobserwacji values (55,174,'2012-12-20');
insert into schemat.raportyobserwacji values (56,175,'2012-12-20');
insert into schemat.raportyobserwacji values (57,176,'2012-12-20');
insert into schemat.raportyobserwacji values (58,177,'2012-12-20');
--------------------END RAPORTY OBSERWACJI ------------

---------------LISTY OD DZIECI ----------------
insert into schemat.ListyOddzieci values (1,1);
insert into schemat.ListyOddzieci values (2,2);
insert into schemat.ListyOddzieci values (3,3);
insert into schemat.ListyOddzieci values (4,4);
insert into schemat.ListyOddzieci values (5,5);
insert into schemat.ListyOddzieci values (6,6);
insert into schemat.ListyOddzieci values (7,7);
insert into schemat.ListyOddzieci values (8,8);
insert into schemat.ListyOddzieci values (10,9);
insert into schemat.ListyOddzieci values (11,10);
insert into schemat.ListyOddzieci values (12,11);
insert into schemat.ListyOddzieci values (13,12);
insert into schemat.ListyOddzieci values (14,13);
insert into schemat.ListyOddzieci values (15,14);
insert into schemat.ListyOddzieci values (16,15);
insert into schemat.ListyOddzieci values (17,16);
insert into schemat.ListyOddzieci values (18,17);
insert into schemat.ListyOddzieci values (19,18);
insert into schemat.ListyOddzieci values (20,19);
insert into schemat.ListyOddzieci values (21,20);
insert into schemat.ListyOddzieci values (22,21);
insert into schemat.ListyOddzieci values (23,22);
insert into schemat.ListyOddzieci values (24,23);
insert into schemat.ListyOddzieci values (25,24);
insert into schemat.ListyOddzieci values (26,25);
insert into schemat.ListyOddzieci values (27,26);
insert into schemat.ListyOddzieci values (28,27);
insert into schemat.ListyOddzieci values (29,28);
----------------------------------------


------ OBWIESZCZENIE KONTROLI --------------------------
insert into schemat.obwieszczeniakontroli values (DEFAULT, 80,1,31,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 81,2,32,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 82,3,33,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 83,4,34, false,'Sprzataj pokoj','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 84,5,35,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 85,6,36,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 86,7,37,true,' ','2012-12-22');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 87,8,38,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 88,10,39,false,'Nie bij brata ','2012-12-21');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 89,11,40 ,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 90,12 ,58,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 91,13 ,57,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 92,14,56,false,'Nie obrazaj siostry','2012-12-22');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 93, 15,55,true,' ' ,'2012-12-23');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 80,16,54,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 81,17,53,true,' ' ,'2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 82,18,52,true,' ','2012-12-18');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 83,19, 51,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 84,20,50,true,'ok','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 85,21,49,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 86,22,48,true,' ','2012-12-21');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 87,23,47,false,'Przepros rodzicow','2012-12-22');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 88,24,46,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 89,25,45,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 90,26 ,44,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 91,27 ,43,true,' ','2012-12-20');
insert into schemat.obwieszczeniakontroli values (DEFAULT, 92,28,42,true,' ','2012-12-20');

------------------------------------------------------------
--AGENCI --------------
insert into schemat.agencje values (1, 'Kauczatka', 1, 0,0);
insert into schemat.agencje values (2, 'Sybir', 1, 0, 0);
insert into schemat.agencje values (3, 'Moscow', 2, 0,0);
insert into schemat.agencje values (4, 'Kijow', 3, 0,0);
insert into schemat.agencje values (5, 'Wladiwistiok', 4, 0,0);
insert into schemat.agencje values (6, 'Petersburg', 5, 0,0);
insert into schemat.agencje values (7, 'Warsaw', 6, 0,0);
insert into schemat.agencje values (8, 'London', 7, 0,0);
insert into schemat.agencje values (9, 'Praga', 8, 0,0);
insert into schemat.agencje values (10, 'Paris', 9, 0,0);
insert into schemat.agencje values (11, 'Berlin', 10, 0,0);
insert into schemat.agencje values (12, 'Munn', 11, 0,0);
insert into schemat.agencje values (13, 'Puerto Rico', 12, 0,0);
insert into schemat.agencje values (14, 'Tokio', 13, 0,0);
insert into schemat.agencje values (15, 'Omsbootgoo', 14, 0,0);
insert into schemat.agencje values (16, 'New York', 15, 0,0);
insert into schemat.agencje values (17, 'Washington', 15, 0,0);
insert into schemat.agencje values (18, 'Las Vegas', 15, 0,0);
insert into schemat.agencje values (19, 'Kansas', 15, 0,0);
insert into schemat.agencje values (20, 'Rome', 16,  0,0);

--- AGENCI -----
insert into schemat.agenci values ( 150, 4, 'a0');
insert into schemat.agenci values ( 151, 7, 'a1');
insert into schemat.agenci values ( 152, 18, 'a2');
insert into schemat.agenci values ( 153, 16, 'a3');
insert into schemat.agenci values ( 154, 14, 'a4');
insert into schemat.agenci values ( 155, 16, 'a5');
insert into schemat.agenci values ( 156, 7, 'a6');
insert into schemat.agenci values ( 157, 13, 'a7');
insert into schemat.agenci values ( 158, 10, 'a8');
insert into schemat.agenci values ( 159, 2, 'a9');
insert into schemat.agenci values ( 160, 3, 'a10');
insert into schemat.agenci values ( 161, 8, 'b0');
insert into schemat.agenci values ( 162, 11, 'b1');
insert into schemat.agenci values ( 163, 20, 'b2');
insert into schemat.agenci values ( 164, 4, 'b3');
insert into schemat.agenci values ( 165, 7, 'b4');
insert into schemat.agenci values ( 166, 1, 'b5');
insert into schemat.agenci values ( 167, 7, 'b6');
insert into schemat.agenci values ( 168, 13, 'b7');
insert into schemat.agenci values ( 169, 17, 'b8');
insert into schemat.agenci values ( 170, 12, 'b9');
insert into schemat.agenci values ( 171, 9, 'b10');
insert into schemat.agenci values ( 172, 8, 'c0');
insert into schemat.agenci values ( 173, 10, 'c1');
insert into schemat.agenci values ( 174, 3, 'c2');
insert into schemat.agenci values ( 175, 11, 'c3');
insert into schemat.agenci values ( 176, 3, 'c4');
insert into schemat.agenci values ( 177, 4, 'c5');
insert into schemat.agenci values ( 178, 8, 'c6');
insert into schemat.agenci values ( 179, 16, 'c7');
insert into schemat.agenci values ( 180, 10, 'c8');
insert into schemat.agenci values ( 181, 3, 'c9');
insert into schemat.agenci values ( 182, 3, 'c10');
insert into schemat.agenci values ( 183, 19, 'd0');
insert into schemat.agenci values ( 184, 10, 'd1');
insert into schemat.agenci values ( 185, 8, 'd2');
insert into schemat.agenci values ( 186, 14, 'd3');
insert into schemat.agenci values ( 187, 17, 'd4');
insert into schemat.agenci values ( 188, 12, 'd5');
insert into schemat.agenci values ( 189, 3, 'd6');
insert into schemat.agenci values ( 190, 10, 'd7');
insert into schemat.agenci values ( 191, 14, 'd8');
insert into schemat.agenci values ( 192, 2, 'd9');
insert into schemat.agenci values ( 193, 20, 'd10');
insert into schemat.agenci values ( 194, 5, 'e0');
insert into schemat.agenci values ( 195, 18, 'e1');
insert into schemat.agenci values ( 196, 19, 'e2');
insert into schemat.agenci values ( 197, 5, 'e3');
insert into schemat.agenci values ( 198, 16, 'e4');
insert into schemat.agenci values ( 199, 11, 'e5');
insert into schemat.agenci values ( 200, 14, 'e6');
insert into schemat.agenci values ( 201, 7, 'e7');
insert into schemat.agenci values ( 202, 12, 'e8');
insert into schemat.agenci values ( 203, 1, 'e9');
insert into schemat.agenci values ( 204, 17, 'e10');
insert into schemat.agenci values ( 205, 14, 'f0');
insert into schemat.agenci values ( 206, 3, 'f1');
insert into schemat.agenci values ( 207, 11, 'f2');
insert into schemat.agenci values ( 208, 17, 'f3');
insert into schemat.agenci values ( 209, 2, 'f4');
insert into schemat.agenci values ( 210, 6, 'f5');
insert into schemat.agenci values ( 211, 6, 'f6');
insert into schemat.agenci values ( 212, 5, 'f7');
insert into schemat.agenci values ( 213, 8, 'f8');
insert into schemat.agenci values ( 214, 17, 'f9');
insert into schemat.agenci values ( 215, 6, 'f10');
insert into schemat.agenci values ( 216, 7, 'g0');
insert into schemat.agenci values ( 217, 10, 'g1');
insert into schemat.agenci values ( 218, 14, 'g2');
insert into schemat.agenci values ( 219, 18, 'g3');

---- KONIEC AGENTOW --------

INSERT INTO schemat.maszynisci values (50,1,1,1,false);
INSERT INTO schemat.maszynisci values (51,2,1,1,false);
INSERT INTO schemat.maszynisci values (52,3,1,1,false);
INSERT INTO schemat.maszynisci values (53,4,1,1,false);
INSERT INTO schemat.maszynisci values (54,5,1,1,false);
INSERT INTO schemat.maszynisci values (55,6,1,1,true);
INSERT INTO schemat.maszynisci values (56,7,1,1,true);
INSERT INTO schemat.maszynisci values (57,8,1,1,true);
INSERT INTO schemat.maszynisci values (58,9,1,1,true);
INSERT INTO schemat.maszynisci values (59,10,1,1,true);
INSERT INTO schemat.maszynisci values (60,11,1,1,false);
INSERT INTO schemat.maszynisci values (61,12,1,1,false);
INSERT INTO schemat.maszynisci values (62,13,1,1,false);
INSERT INTO schemat.maszynisci values (63,14,1,1,false);
INSERT INTO schemat.maszynisci values (64,15,1,2,true);
INSERT INTO schemat.maszynisci values (65,15,2,2,true);
INSERT INTO schemat.maszynisci values (66,14,2,3,false);
INSERT INTO schemat.maszynisci values (67,13,2,4,false);




