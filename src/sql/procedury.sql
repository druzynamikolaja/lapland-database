-- http://www.postgresql.org/docs/9.2/static/sql-createfunction.html
-- http://www.postgresql.org/docs/9.2/static/plpgsql-control-structures.html


/*
	Funkcja, do klienta gnoma, dla podanego id_listu wystawia opinie i komentarz
	Dane przyjmowane ID LISTU, OPINIA DLA DANEGO LISTU('TEXT'), CZY ZAAKCEPTOWANO GO (BOOLEAN)
	@author Piotr Kruk
*/



CREATE OR REPLACE FUNCTION Wystaw_opinie_dziecku(parm1 int, parm2 text,parm3 boolean)
	returns void
	AS
	$BODY$
	 UPDATE schemat.obwieszczeniakontroli set zaakceptowano = $3, komentarz = $2
	 WHERE idlistudziecka = $1
	$BODY$
language sql SECURITY DEFINER; -- udzielenie praw do czynnosci wykonywanych przez funkcję dla wykonujacego (gnoma) @Konrad.


--- Przyklad uzycia 
--- select * from wystaw_opinie_dziecku(1,'Ucz sie!',false);
--- Usuniecie
-- drop function wystaw_opinie_dziecku(parm1 int, parm2 text,parm3 boolean);



/*
@author Piotr Kruk
-- Funkcja przydzielajaca urlop w razie wypadku bhp
--- Poprawiona funkcja Michała 
*/



CREATE OR REPLACE FUNCTION Chorobowe(parm1 int)
	RETURNS VOID
	AS
	$BODY$
	UPDATE schemat.pracownicy SET urlopzdrowotny = TRUE,koniecurlopu = CURRENT_DATE +b.wartoscurlopu
	FROM schemat.PracownicySali p,schemat.BHP b,schemat.SaleProdukcji s
	WHERE p.idkartyzakladowej = $1
	AND s.numersali = p.numersali
	AND s.idbhp = b.id;
	-- Maszynisci, pomysle nad ifem! ale to nie takie proste
	UPDATE schemat.pracownicy SET urlopzdrowotny = TRUE,koniecurlopu = CURRENT_DATE +b.wartoscurlopu
	FROM schemat.Maszynisci m,schemat.BHP b,schemat.SaleProdukcji s
	WHERE m.idkartyzakladowej = $1
	AND s.numersali = m.numersali
	AND s.idbhp = b.id;
	$BODY$
LANGUAGE SQL;



/*

--- Przyklad uzycia i testowania : 
	select * from chorobowe(11); 
	select * from schemat.pracownicy where id = 11
	update pracownicy set urlopzdrowotny = false,koniecurlopu=null where id = 11
	select * from pracownicy where urlopzdrowotny = true and koniecurlopu is not null;

*/



/*
@author Piotr Kruk
 funkcja zwalniajaca pracownikow z danego dzialu w SALACH PRODUKCJI
*/



CREATE OR REPLACE FUNCTION Zwolnij_dzial(parm1 int)
	RETURNS VOID
	AS
	$BODY$
	DELETE FROM schemat.pracownicysali WHERE numersali = $1;
	DELETE FROM schemat.maszynisci WHERE numersali = $1;
	$BODY$
LANGUAGE SQL;



/*
Przyklad uzycia: 
select * from zwolnij_dzial(1);
select * from schemat.pracownicysali;
*/



/*
@author Piotr Kruk
	Funkcja wysylajaca zamowienie do Fabryki, przyjmuje id dzialu i id iIe dany dzial ma wydrukowac jeszcze,
		gdy dodamy z minusikiem, to odejmiemy od dzialu :) 

*/



CREATE OR REPLACE FUNCTION Zamowienie(parm1 int, parm2 int)
	RETURNS VOID
	AS
	$BODY$
	UPDATE schemat.Fabryka SET ilosczamowien = ilosczamowien + $2
	WHERE numerwydzialu = $1
	$BODY$
LANGUAGE SQL;


--- Przyklad uzycia 
-- select * from Zamowienie(1,10);
--  select * from Zamowienie(1,-5);
-- select * from schemat.fabryka;



/**
 * @author	Konrad Talik
 *			Funkcja wyświetlająca ilosc nowych zamówień,
 *			oraz funkcja do przyjmowania zamówień do realizacji.
 */



CREATE OR REPLACE FUNCTION Ilosc_nowych_zamowien(wydzial int)
	RETURNS int
	AS
	$$
	WITH Wiersz AS 
		(SELECT IloscZamowien, Zrealizowano
			FROM schemat.Fabryka WHERE NumerWydzialu = wydzial)
		SELECT IloscZamowien - Zrealizowano AS Zamowienia FROM Wiersz;
	$$
LANGUAGE SQL;



CREATE OR REPLACE FUNCTION Przyjeto(wydzial int, ileprzyjeto int)
	RETURNS void
	AS
	$$
		UPDATE schemat.Fabryka SET Zrealizowano = Zrealizowano + ileprzyjeto;
	$$
LANGUAGE SQL;



/*
  Funkcja uruchamiajaca nasze swieta !
  @author : Piotr Kruk 
 */



/*
-- W PLANACH I W PROJEKCIE @Piotr Kruk
--------------------------START FUNKCJI ------------------------------------
CREATE OR REPLACE FUNCTION zaczynamy_swieta ()
	returns void
	AS
	$BODY$
		UPDATE schemat.renifery SET zakupiony = TRUE 
		WHERE zakupiony = FALSE 
		--- KUPUJEMY RENIFERY! SANIE GOTOWE DO PRACY !
		--- I TUTAJ BYM CHCIAL JAKOS TO ZAANIMOWAC ZE MIKOLAJ ROZDAJE PREZENTY
		-- Moze cron i funkcja odpalana o odpowiedniej porze?
		UPDATE schemat.regiony SET prezentyrozdane = TRUE 
		-- prezenty rozdane 
		UPDATE schemat.renifery SET  zakupiony = FALSE
		-- renifery do stajni! HOJ HOJ HOJ 
	$BODY$
language sql; */
--------------------------START FUNKCJI ------------------------------------
--- Przyklad uzycia
-- select * from zaczynamy_swieta();
--- USUNIECIE
--drop function zaczynamy_swieta;




/**
 * @author	Konrad Talik
 *			Procedura umożliwiająca dodanie listu do tabeli ListyOdDzieci,
 *			bez wiedzy czy dane dziecko istnieje w bazie czy nie
 *			(przy czym najpierw dodawany jest wiersz w tabeli Listy).
 */



CREATE OR REPLACE FUNCTION List_od_dziecka
	(DanyKraj text, DaneImie text, DaneNazwisko text, DanyWiek int = 0,
	DanaPlec text = NULL, DataOtrzymaniaListu date = CURRENT_DATE, DanyNumerKartoteki int = NULL)
	RETURNS VOID AS
$$

DECLARE
id_dziecka int;
id_regionu int;
id_listu int;

BEGIN
	id_regionu = (SELECT DISTINCT NumerRegionu FROM schemat.Regiony WHERE Panstwo = DanyKraj);

	-- Ustalanie tożsamości dziecka.

	IF DanyNumerKartoteki IS NOT NULL THEN
		id_dziecka = (SELECT Id FROM schemat.Dzieci
			WHERE NumerKartoteki = DanyNumerKartoteki);
	ELSE
		id_dziecka = (SELECT Id FROM schemat.Dzieci
			WHERE RegionPochodzenia = id_regionu
			AND Imie = DaneImie AND Nazwisko = DaneNazwisko AND Wiek = DanyWiek);
	END IF;
		
	IF id_dziecka IS NULL THEN
		INSERT INTO schemat.Dzieci VALUES (
			DEFAULT, (SELECT DISTINCT NumerRegionu FROM schemat.Regiony WHERE Panstwo = DanyKraj), 
			DaneImie, DaneNazwisko, DanyNumerKartoteki, DanyWiek, DanaPlec);

		id_dziecka = (SELECT Id FROM schemat.Dzieci
			WHERE RegionPochodzenia = id_regionu
			AND Imie = DaneImie AND Nazwisko = DaneNazwisko AND Wiek = DanyWiek);
	END IF;

	-- Tworzenie listu

	WITH NoweId AS (
	INSERT INTO schemat.Listy (Id, DataOtrzymania, OdDziecka) VALUES (DEFAULT, DataOtrzymaniaListu, True)
		RETURNING Id)
	INSERT INTO schemat.ListyOdDzieci VALUES ((SELECT Id FROM NoweId), id_dziecka);


END;
$$
LANGUAGE PLPGSQL;


